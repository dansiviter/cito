/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito;

import static cito.MimeTypes.TEXT_PLAIN;
import static java.util.Collections.emptyMap;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.event.Message;
import cito.internal.Connection;
import cito.internal.ConnectionRegistry;
import cito.internal.Log;
import cito.internal.MessageBuilderFactory;
import cito.internal.MessageBuilderFactory.MessageBuilder;
import cito.internal.SystemConnection;

/**
 * Unit test for {@link MessagingSupport}.
 *
 * @author Daniel Siviter
 * @since v1.0 [25 Jul 2016]
 */
@ExtendWith(MockitoExtension.class)
public class MessagingSupportTest {
	@Mock
	private Log log;
	@Mock
	private ConnectionRegistry registry;
	@Mock
	private SystemConnection systemConnection;
	@Mock
	private Connection connection;
	@Mock
	private MessageBuilderFactory factory;
	@Mock
	private MessageBuilder<Message> builder;

	@InjectMocks
	private MessagingSupport support;

	@Test
	public void broadcast_destination_payload(@Mock Message msg) throws IOException {
		when(this.registry.systemConnection()).thenReturn(systemConnection);
		when(this.factory.builder()).thenAnswer(b -> builder);  // avoid generics errors
		when(this.builder.body(any(), any())).thenReturn(this.builder);
		when(this.builder.destination(any())).thenReturn(this.builder);
		when(this.builder.headers(any())).thenReturn(this.builder);
		when(this.builder.build()).thenReturn(msg);

		this.support.broadcast("destination", "payload", emptyMap());

		verify(this.systemConnection).on(msg);
		verify(this.builder).body("payload", null);
		verify(this.builder).destination("destination");
		verify(this.builder).headers(emptyMap());
		verify(this.log).debugf("Broadcasting... [destination=%s]", "destination");
		verify(this.registry).systemConnection();
	}

	@Test
	public void broadcast_destination_mediaType_payload(@Mock Message msg) throws IOException {
		when(this.registry.systemConnection()).thenReturn(systemConnection);
		when(this.factory.builder()).thenAnswer(b -> builder);  // avoid generics errors
		when(this.builder.body(any(), any())).thenReturn(this.builder);
		when(this.builder.destination(any())).thenReturn(this.builder);
		when(this.builder.headers(any())).thenReturn(this.builder);
		when(this.builder.build()).thenReturn(msg);

		this.support.broadcast("destination", "payload", TEXT_PLAIN, emptyMap());

		verify(this.builder).body("payload", TEXT_PLAIN);
		verify(this.builder).destination("destination");
		verify(this.builder).headers(emptyMap());
		verify(this.systemConnection).on(msg);
		verify(this.log).debugf("Broadcasting... [destination=%s]", "destination");
		verify(this.registry).systemConnection();
	}

	@Test
	public void sendTo_destination_payload(@Mock Message msg) throws IOException {
		when(this.registry.get(any())).thenAnswer(c -> Optional.of(this.connection));
		when(this.factory.builder()).thenAnswer(b -> builder);  // avoid generics errors
		when(this.builder.sessionId(any())).thenReturn(this.builder);
		when(this.builder.destination(any())).thenReturn(this.builder);
		when(this.builder.headers(any())).thenReturn(this.builder);
		when(this.builder.build()).thenReturn(msg);

		this.support.sendTo("sessionId", "destination", "payload", emptyMap());

		final ArgumentCaptor<Message> eventCaptor = ArgumentCaptor.forClass(Message.class);
		verify(this.connection).on(eventCaptor.capture());

		verify(this.builder).sessionId("sessionId");
		verify(this.builder).destination("destination");
		verify(this.builder).headers(emptyMap());
		verify(this.log).debugf("Sending... [sessionId=%s,destination=%s]", "sessionId", "destination");
		verify(this.registry).get("sessionId");
	}

	@Test
	public void sendTo_destination_mediaType_payload(@Mock Message msg) throws IOException {
		when(this.registry.get(any())).thenAnswer(c -> Optional.of(this.connection));
		when(this.factory.builder()).thenAnswer(b -> builder);  // avoid generics errors
		when(this.builder.sessionId(any())).thenReturn(this.builder);
		when(this.builder.destination(any())).thenReturn(this.builder);
		when(this.builder.headers(any())).thenReturn(this.builder);
		when(this.builder.build()).thenReturn(msg);

		this.support.sendTo("sessionId", "destination", "payload", TEXT_PLAIN, emptyMap());

		final ArgumentCaptor<Message> eventCaptor = ArgumentCaptor.forClass(Message.class);
		verify(this.connection).on(eventCaptor.capture());

		verify(this.builder).sessionId("sessionId");
		verify(this.builder).destination("destination");
		verify(this.builder).headers(emptyMap());
		verify(this.log).debugf("Sending... [sessionId=%s,destination=%s]", "sessionId", "destination");
		verify(this.registry).get("sessionId");
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.log, this.registry, this.systemConnection, this.connection);
	}
}
