package cito.internal;

import static java.lang.Long.BYTES;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import javax.websocket.PongMessage;
import javax.websocket.RemoteEndpoint.Basic;
import javax.websocket.Session;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

/**
 * Unit tests for {@link RttService}.
 *
 * @author Daniel Siviter
 * @since v1.0 [30 Jan 2018]
 */
@ExtendWith(MockitoExtension.class)
public class RttServiceTest {
	@Mock
	private Log log;
	@Mock
	private ScheduledExecutorService executor;

	@InjectMocks
	private RttService rttService;

	@Test
	public void start(@Mock Session session, @Mock Basic basic) throws IllegalArgumentException, IOException {
		doAnswer(new Answer<Future<?>>() {
			@Override
			public Future<?> answer(InvocationOnMock invocation) throws Throwable {
				((Runnable) invocation.getArguments()[0]).run();
				return null;
			}
		}).when(executor).submit(any(Runnable.class));
		when(session.isOpen()).thenReturn(true);
		when(session.getId()).thenReturn("sessionId");
		when(session.getBasicRemote()).thenReturn(basic);

		this.rttService.start(session);

		verify(this.executor).submit(any(Runnable.class));
		verify(session).isOpen();
		verify(session).getId();
		verify(this.log).pingSent("sessionId");
		verify(session).getBasicRemote();
		verify(basic).sendPing(any());

		verifyNoMoreInteractions(session, basic);
	}

	@Test
	public void pong(@Mock Session session, @Mock PongMessage pongMessage) {
		when(pongMessage.getApplicationData()).thenReturn(
				(ByteBuffer) ByteBuffer.allocate(BYTES).putLong(123).flip());
		when(session.getId()).thenReturn("sessionId");
		when(session.getUserProperties()).thenReturn(new HashMap<>());

		this.rttService.pong(session, pongMessage);

		verify(pongMessage).getApplicationData();
		verify(session).getId();
		verify(this.log).pongReceived(Mockito.anyLong(), eq("sessionId"));
		verify(session).getUserProperties();
		verify(this.executor).schedule(
				any(Runnable.class),
				eq(60L),
				eq(SECONDS));
		verifyNoMoreInteractions(session, pongMessage);
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.log, this.executor);
	}
}
