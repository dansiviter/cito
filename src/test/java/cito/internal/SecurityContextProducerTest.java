package cito.internal;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.websocket.Session;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.security.SecurityContext;

/**
 * Unit test for {@link SecurityContextProducer}.
 *
 * @author Daniel Siviter
 * @since v1.0 [31 Dec 2017]
 */
@ExtendWith(MockitoExtension.class)
public class SecurityContextProducerTest {
	@Mock
	private Session session;
	@Mock
	private SecurityContext securityCtx;

	@Test
	public void securityCtx() {
		when(this.session.getUserProperties()).thenReturn(singletonMap(SecurityContext.class.getName(), this.securityCtx));

		SecurityContext actual = SecurityContextProducer.securityCtx(this.session);

		assertEquals(this.securityCtx, actual);

		verify(this.session).getUserProperties();
	}

	@Test
	public void securityCtx_noop() {
		when(this.session.getUserProperties()).thenReturn(emptyMap());

		SecurityContext actual = SecurityContextProducer.securityCtx(this.session);

		assertNull(actual);

		verify(this.session).getUserProperties();
	}

	@Test
	public void set() {
		Map<String, Object> properties = new HashMap<>();
		when(this.session.getUserProperties()).thenReturn(properties);

		SecurityContextProducer.set(this.session, this.securityCtx);

		assertEquals(this.securityCtx, properties.get(SecurityContext.class.getName()));

		verify(this.session).getId();
		verify(this.session, times(2)).getUserProperties();
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.session, this.securityCtx);
	}
}
