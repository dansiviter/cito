/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.internal.SystemConnection.SESSION_ID;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.ReflectionUtil;
import cito.event.Message;
import cito.internal.stomp.Factory;
import cito.internal.stomp.Session;

/**
 * Unit tests for {@link SystemConnection}.
 *
 * @author Daniel Siviter
 * @since v1.0 [14 Apr 2017]
 */
@ExtendWith(MockitoExtension.class)
public class SystemConnectionTest {
	@Mock
	private Log log;
	@Mock
	private ConnectionFactory connectionFactory;
	@Mock
	private Factory factory;

	@InjectMocks
	private SystemConnection connection;

	@Test
	public void scope() {
		assertNotNull(ReflectionUtil.getAnnotation(SystemConnection.class, ApplicationScoped.class));
	}

	@Test
	public void init(@Mock JMSContext ctx) throws JMSException {
		when(this.connectionFactory.createContext()).thenReturn(ctx);

		this.connection.init();

		verify(this.connectionFactory).createContext();
		verify(this.log).infof("Starting JMS connection... [%s]", SESSION_ID);
		verify(ctx).setClientID(SESSION_ID);
		verify(ctx).start();
		verifyNoMoreInteractions(ctx);
	}

	@Test
	public void on_message(@Mock Message msg, @Mock Session session) throws JMSException {
		when(this.factory.toSession(this.connection, JMSContext.AUTO_ACKNOWLEDGE)).thenReturn(session);
		when(msg.sessionId()).thenReturn(Optional.of(SESSION_ID));

		this.connection.on(msg);

		verify(this.factory).toSession(this.connection, JMSContext.AUTO_ACKNOWLEDGE);
		verify(msg).sessionId();
		verify(this.log).debugf("Message event. [%s]", SESSION_ID);
		verify(session).sendToBroker(msg);
		verifyNoMoreInteractions(msg, session);
	}

	@Test
	public void on_message_incorrectSessionId(@Mock Message msg, @Mock Session session) throws JMSException {
		when(msg.sessionId()).thenReturn(Optional.of("another"));

		assertThrows(IllegalArgumentException.class, () -> {
			this.connection.on(msg);
		}, "Session identifier mismatch! [expected=" + SESSION_ID + " OR null,actual=another]");

		verify(msg, times(2)).sessionId();
		verifyNoMoreInteractions(msg, session);
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.log, this.connectionFactory, this.factory);
	}
}
