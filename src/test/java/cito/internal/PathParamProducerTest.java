/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Optional;

import javax.enterprise.inject.spi.Annotated;
import javax.enterprise.inject.spi.InjectionPoint;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.annotation.PathParam;
import cito.annotation.PathParam.Literal;
import cito.event.DestinationChanged;
import cito.event.Message;
import cito.internal.Context.Scope;


/**
 * Unit tests for {@link PathParamProducer}.
 *
 * @author Daniel Siviter
 * @since v1.0 [14 Apr 2017]
 */
@ExtendWith(MockitoExtension.class)
public class PathParamProducerTest {
	@Mock
	private Context context;

	private Scope scope;

	@BeforeEach
	public void beforeEach() {
		this.scope = Context.scope(context);
	}

	@Test
	public void set_path(@Mock PathParser parser) {
		try (Scope scope = PathParamProducer.set(parser)) {
			final ArgumentCaptor<PathParser> captor = ArgumentCaptor.forClass(PathParser.class);
			verify(context).putScoped(eq(PathParser.class.getName()), captor.capture());
			assertSame(parser, captor.getValue());
		}

		verifyNoMoreInteractions(parser);
	}

	@Test
	public void set_pathParser() {
		try (Scope scope = PathParamProducer.set("/")) {
			verify(context).putScoped(eq(PathParser.class.getName()), any());
		}
	}

	@Test
	public void pathParam(
			@Mock InjectionPoint ip,
			@Mock DestinationChanged dc,
			@Mock Message msg,
			@Mock Annotated annotated)
	{
		final PathParser parser = new PathParser("/{there}");
		when(msg.destination()).thenReturn(Optional.of("/here"));
		when(ip.getAnnotated()).thenReturn(annotated);
		when(annotated.getAnnotation(PathParam.class)).thenReturn(Literal.pathParam("there"));

		final String param = PathParamProducer.getValue(ip, parser, msg, dc);
		assertEquals("here", param);

		verify(msg).destination();
		verify(ip).getAnnotated();
		verify(annotated).getAnnotation(PathParam.class);
		verifyNoMoreInteractions(ip, msg, dc, annotated);
	}

	@Test
	public void pathParam_message(
			@Mock InjectionPoint ip,
			@Mock Message msg,
			@Mock Annotated annotated)
	{
		final PathParser parser = new PathParser("/{there}");
		when(msg.destination()).thenReturn(Optional.of("/here"));
		when(ip.getAnnotated()).thenReturn(annotated);
		when(annotated.getAnnotation(PathParam.class)).thenReturn(Literal.pathParam("there"));

		final String param = PathParamProducer.getValue(ip, parser, msg, null);
		assertEquals("here", param);

		verify(msg).destination();
		verify(ip).getAnnotated();
		verify(annotated).getAnnotation(PathParam.class);
		verifyNoMoreInteractions(ip, msg, annotated);
	}

	@Test
	public void pathParam_destination(
			@Mock InjectionPoint ip,
			@Mock DestinationChanged dc,
			@Mock Message msg,
			@Mock Annotated annotated)
	{
		final PathParser parser = new PathParser("/{there}");
		when(dc.getDestination()).thenReturn("/here");
		when(ip.getAnnotated()).thenReturn(annotated);
		when(annotated.getAnnotation(PathParam.class)).thenReturn(Literal.pathParam("there"));

		final String param = PathParamProducer.getValue(ip, parser, null, dc);
		assertEquals("here", param);

		verify(dc).getDestination();
		verify(ip).getAnnotated();
		verify(annotated).getAnnotation(PathParam.class);
		verifyNoMoreInteractions(ip, dc, annotated);
	}

	@AfterEach
	public void afterEach() {
		this.scope.close();
	}
}
