/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.ReflectionUtil.getAnnotation;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

import javax.enterprise.context.ApplicationScoped;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.event.DestinationChanged;
import cito.event.DestinationChanged.Type;
import cito.internal.Context.Scope;

/**
 * Unit tests {@link DestinationChangedProducer}.
 *
 * @author Daniel Siviter
 * @since v1.0 [16 Apr 2017]
 */
@ExtendWith(MockitoExtension.class)
public class DestinationChangedProducerTest {
	@Mock
	private Context context;

	private Scope scope;

	@BeforeEach
	public void beforeEach() {
		this.scope = Context.scope(context);
	}

	@Test
	public void scope() {
		assertNotNull(getAnnotation(DestinationChangedProducer.class, ApplicationScoped.class));
	}

	@Test
	public void test() {
		assertNull(DestinationChangedProducer.get(this.context));

		final DestinationChanged e = new DestinationChanged(Type.ADDED, "/");

		try (Scope scope = DestinationChangedProducer.set(e)) {
			verify(context).putScoped(eq(DestinationChanged.class.getName()), any());
		}

		assertNull(DestinationChangedProducer.get(this.context));
	}

	@AfterEach
	public void afterEach() {
		this.scope.close();
	}
}
