/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cito.internal.PathParser.Result;

/**
 * JUnit test for {@link PathParser}.
 *
 * @author Daniel Siviter
 * @since v1.0 [17 Jan 2017]
 */
public class PathParserTest {
	@Test
	public void pathParser_path() {
		final PathParser parser = PathParser.pathParser("/");
		final PathParser parser0 = PathParser.pathParser("/");
		assertSame(parser, parser0);
	}

	@Test
	public void parse_forwardSlash() {
		final PathParser parser = new PathParser("/queue/{param1}/segment/{param2}");
		final Result result = parser.parse("/queue/first/segment/second");
		assertTrue(result.isSuccess());
		assertEquals("first", result.get("param1"));
		assertEquals("second", result.get("param2"));
	}

	@Test
	public void parse_period() {
		final PathParser parser = new PathParser("/queue/{param1}.segment.{param2}");
		final Result result = parser.parse("/queue/first.segment.second");
		assertTrue(result.isSuccess());
		assertEquals("first", result.get("param1"));
		assertEquals("second", result.get("param2"));
	}

	@Test
	public void parse_hyphen() {
		final PathParser parser = new PathParser("/queue/{param1}");
		final Result result = parser.parse("/queue/first-segment");
		assertTrue(result.isSuccess());
		assertEquals("first-segment", result.get("param1"));
	}

	@Test
	public void parse_noPrefix() {
		final PathParser parser = new PathParser("{param1}/segment/{param2}");
		final Result result = parser.parse("first/segment/second");
		assertTrue(result.isSuccess());
		assertEquals("first", result.get("param1"));
		assertEquals("second", result.get("param2"));
	}

	@Test
	public void parse_invalidSegments() {
		final PathParser parser = new PathParser("{param1}/segment/{param2}");
		assertFalse(parser.parse("/queue/first/segment/second").isSuccess());
	}

	@Test
	public void parse_nonMatchingSegments() {
		final PathParser parser = new PathParser("/queue/{param1}/segment");
		assertFalse(parser.parse("/queue/first/another").isSuccess());
	}

	@Test
	public void parse_wildcard() {
		final PathParser parser = new PathParser("{param1}/segment/{param2}/*");
		final Result result = parser.parse("first/segment/second/anything");
		assertTrue(result.isSuccess());
		assertEquals("first", result.get("param1"));
		assertEquals("second", result.get("param2"));
	}

	@Test
	public void parse_static() {
		final Result result = PathParser.parse("/queue/{param1}", "/queue/first");
		assertTrue(result.isSuccess());
		assertEquals("first", result.get("param1"));
	}

	@Test
	public void equality() {
		final PathParser parser0 = new PathParser("/{param}");
		assertTrue(parser0.equals(parser0));

		final PathParser parser1 = new PathParser("/{param}");
		assertTrue(parser0.equals(parser1));
		assertEquals(parser0.hashCode(), parser1.hashCode());

		final PathParser parser2 = new PathParser("/{anotherParam}");
		assertFalse(parser0.equals(parser2));
		assertThat(parser0.hashCode(), not(is(parser2.hashCode())));
	}

	@Test
	public void toString_() {
		final PathParser parser = new PathParser("path/*");
		assertEquals("cito.internal.PathParser@" +  Integer.toHexString(parser.hashCode()) + "[path/*]", parser.toString());
	}
}
