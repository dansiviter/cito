/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.annotation.Body.Literal.body;
import static java.util.Collections.singleton;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.lang.annotation.Annotation;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;

import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.AnnotatedMember;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.enterprise.inject.spi.ProcessInjectionPoint;
import javax.enterprise.inject.spi.configurator.BeanConfigurator;
import javax.enterprise.util.TypeLiteral;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.ReflectionUtil;
import cito.annotation.Body;


/**
 * Unit test for {@link BodyProducerExtension}.
 *
 * @author Daniel Siviter
 * @since v1.0 [21 May 2017]
 */
@ExtendWith(MockitoExtension.class)
public class BodyProducerExtensionTest {
	private static final TypeLiteral<Set<Annotation>> SET_ANNOTATIONS = new TypeLiteral<Set<Annotation>>() {
		private static final long serialVersionUID = 1L;
	};

	@SuppressWarnings("unused")
	private static List<TestBean> TYPE = null; // don't delete, used for creating reflected type

	private Type type;

	@Mock
	private BeanManager beanManager;

	private BodyProducerExtension extension;

	@BeforeEach
	public void before() throws NoSuchFieldException, SecurityException {
		this.type = getClass().getDeclaredField("TYPE").getGenericType();
		this.extension = new BodyProducerExtension();
	}

	@Test
	public void findBody(
			@Mock ProcessInjectionPoint<?, ?> pip,
			@Mock InjectionPoint ip,
			@Mock AnnotatedMember<?> annotatedMember,
			@Mock Member member)
	{
		when(pip.getInjectionPoint()).thenReturn(ip);
		when(ip.getAnnotated()).thenReturn(annotatedMember);
		when(annotatedMember.isAnnotationPresent(any())).thenReturn(true);

		final List<ProcessInjectionPoint<?, ?>> found = ReflectionUtil.get(this.extension, "found");
		assertTrue(found.isEmpty());

		this.extension.findBody(pip);

		assertEquals(1, found.size());

		final ProcessInjectionPoint<?, ?> actualPip = found.get(0);
		assertEquals(pip, actualPip);

		verify(pip).getInjectionPoint();
		verify(ip).getAnnotated();
		verify(annotatedMember).isAnnotationPresent(Body.class);
		verifyNoMoreInteractions(pip, ip, annotatedMember, member);
	}

	@Test
	public void addBeans(
			@Mock AfterBeanDiscovery abd,
			@Mock ProcessInjectionPoint<?, ?> pip,
			@Mock InjectionPoint ip,
			@Mock BeanConfigurator<Object> configurator,
			@Mock Member member)
	{
		when(pip.getInjectionPoint()).thenReturn(ip);
		when(ip.getType()).thenReturn(this.type);
		when(ip.getMember()).thenReturn(member);
		when(member.getName()).thenReturn("test");
		when(ip.getQualifiers()).thenReturn(singleton((Annotation) body()));
		when(abd.addBean()).thenReturn(configurator);
		when(configurator.name(any())).thenReturn(configurator);
		when(configurator.beanClass(any())).thenReturn(configurator);
		when(configurator.types(this.type, Object.class)).thenReturn(configurator);
		when(configurator.qualifiers(any(SET_ANNOTATIONS.getRawType()))).thenReturn(configurator);

		final List<ProcessInjectionPoint<?, ?>> found = ReflectionUtil.get(this.extension, "found");
		found.add(pip);

		this.extension.addBeans(abd, this.beanManager);

		assertTrue(found.isEmpty());

		verify(pip).getInjectionPoint();
		verify(ip).getType();
		verify(ip).getMember();
		verify(member).getName();
		verify(ip).getQualifiers();
		verify(abd).addBean();
		verify(configurator).name("test");
		verify(configurator).beanClass(List.class);
		verify(configurator).types(this.type, Object.class);
		verify(configurator).qualifiers(any(SET_ANNOTATIONS.getRawType()));
		verify(configurator).createWith(any());
		verifyNoMoreInteractions(abd, pip, ip, configurator, member);
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.beanManager);
	}

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [9 Jul 2018]
	 *
	 */
	public static class TestBean { }
}
