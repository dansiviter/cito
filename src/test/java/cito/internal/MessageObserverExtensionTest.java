/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.annotation.OnDisconnect.Literal.onDisconnect;
import static cito.annotation.OnSend.Literal.onSend;
import static cito.annotation.OnSubscribe.Literal.onSubscribe;
import static cito.annotation.OnUnsubscribe.Literal.onUnsubscribe;
import static java.util.Collections.singleton;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Set;

import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.BeforeBeanDiscovery;
import javax.enterprise.inject.spi.ObserverMethod;
import javax.enterprise.inject.spi.ProcessObserverMethod;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.tomitribe.microscoped.core.ScopeContext;

import cito.ReflectionUtil;
import cito.annotation.OnConnected;
import cito.annotation.OnDisconnect;
import cito.annotation.OnSend;
import cito.annotation.OnSubscribe;
import cito.annotation.OnUnsubscribe;
import cito.annotation.WebSocketScope;
import cito.event.Message;

/**
 * Unit test for {@link MessageObserverExtension}.
 *
 * @author Daniel Siviter
 * @since v1.0 [25 Jul 2016]
 */
@ExtendWith(MockitoExtension.class)
public class MessageObserverExtensionTest {
	@Mock
	private BeanManager beanManager;

	private MessageObserverExtension extension;

	@BeforeEach
	public void before() {
		this.extension = new MessageObserverExtension();
	}

	@Test
	public void addScope(@Mock BeforeBeanDiscovery beforeBeanDiscovery) {
		this.extension.addScope(beforeBeanDiscovery);

		verify(beforeBeanDiscovery).addScope(WebSocketScope.class, true, false);
		verifyNoMoreInteractions(beforeBeanDiscovery);
	}

	@Test
	public void register_onConnected(
			@Mock ProcessObserverMethod<Message, ?> processObserverMethod,
			@Mock ObserverMethod<Message> observerMethod)
	{
		when(processObserverMethod.getObserverMethod()).thenReturn(observerMethod);
		when(observerMethod.getObservedQualifiers()).thenReturn(singleton(OnConnected.Literal.onConnected()));

		this.extension.registerMessageEvent(processObserverMethod);

		assertEquals(observerMethod, getMessageObservers(this.extension).get(OnConnected.class).iterator().next());

		verify(processObserverMethod).getObserverMethod();
		verify(observerMethod).getObservedQualifiers();
		verify(processObserverMethod).veto();
		verifyNoMoreInteractions(processObserverMethod, observerMethod);
	}

	@Test
	public void register_onSend(
			@Mock ProcessObserverMethod<Message, ?> processObserverMethod,
			@Mock ObserverMethod<Message> observerMethod)
	{
		when(processObserverMethod.getObserverMethod()).thenReturn(observerMethod);
		when(observerMethod.getObservedQualifiers()).thenReturn(singleton(onSend("")));

		this.extension.registerMessageEvent(processObserverMethod);

		assertEquals(observerMethod, getMessageObservers(this.extension).get(OnSend.class).iterator().next());

		verify(processObserverMethod).getObserverMethod();
		verify(observerMethod).getObservedQualifiers();
		verify(processObserverMethod).veto();
		verifyNoMoreInteractions(processObserverMethod, observerMethod);
	}

	@Test
	public void register_onSubscribe(
			@Mock ProcessObserverMethod<Message, ?> processObserverMethod,
			@Mock ObserverMethod<Message> observerMethod)
	{
		when(processObserverMethod.getObserverMethod()).thenReturn(observerMethod);
		when(observerMethod.getObservedQualifiers()).thenReturn(singleton(onSubscribe("")));

		this.extension.registerMessageEvent(processObserverMethod);

		assertEquals(observerMethod, getMessageObservers(this.extension).get(OnSubscribe.class).iterator().next());

		verify(processObserverMethod).getObserverMethod();
		verify(observerMethod).getObservedQualifiers();
		verify(processObserverMethod).veto();
		verifyNoMoreInteractions(processObserverMethod, observerMethod);
	}

	@Test
	public void register_onUnsubscribe(
			@Mock ProcessObserverMethod<Message, ?> processObserverMethod,
			@Mock ObserverMethod<Message> observerMethod)
	{
		when(processObserverMethod.getObserverMethod()).thenReturn(observerMethod);
		when(observerMethod.getObservedQualifiers()).thenReturn(singleton(onUnsubscribe("")));

		this.extension.registerMessageEvent(processObserverMethod);

		assertEquals(observerMethod, getMessageObservers(this.extension).get(OnUnsubscribe.class).iterator().next());

		verify(processObserverMethod).getObserverMethod();
		verify(observerMethod).getObservedQualifiers();
		verify(processObserverMethod).veto();
		verifyNoMoreInteractions(processObserverMethod, observerMethod);
	}

	@Test
	public void register_onDisconnect(
			@Mock ProcessObserverMethod<Message, ?> processObserverMethod,
			@Mock ObserverMethod<Message> observerMethod)
	{
		when(processObserverMethod.getObserverMethod()).thenReturn(observerMethod);
		when(observerMethod.getObservedQualifiers()).thenReturn(singleton(onDisconnect()));

		this.extension.registerMessageEvent(processObserverMethod);

		assertEquals(observerMethod, getMessageObservers(this.extension).get(OnDisconnect.class).iterator().next());

		verify(processObserverMethod).getObserverMethod();
		verify(observerMethod).getObservedQualifiers();
		verify(processObserverMethod).veto();
		verifyNoMoreInteractions(processObserverMethod, observerMethod);
	}

	@Test
	public void getObservers(
			@Mock ObserverMethod<Message> observerMethod)
	{
		getMessageObservers(this.extension).put(OnSubscribe.class, singleton(observerMethod));

		final Set<ObserverMethod<Message>> results = this.extension.getMessageObservers(OnSubscribe.class);

		assertEquals(singleton(observerMethod), results);

		verifyNoMoreInteractions(observerMethod);
	}

	@Test
	public void registerContexts(
			@Mock AfterBeanDiscovery afterBeanDiscovery)
	{
		this.extension.registerContexts(afterBeanDiscovery, this.beanManager);

		verify(afterBeanDiscovery).addContext(any(ScopeContext.class));
		verifyNoMoreInteractions(afterBeanDiscovery);
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.beanManager);
	}


	// --- Static Method ---

	/**
	 *
	 * @param e
	 * @return
	 */
	private static Map<Class<? extends Annotation>, Set<ObserverMethod<Message>>> getMessageObservers(MessageObserverExtension e) {
		return ReflectionUtil.get(e, "messageObservers");
	}
}
