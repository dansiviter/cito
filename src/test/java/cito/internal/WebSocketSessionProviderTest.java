/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.ReflectionUtil.getAnnotation;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.BeanManager;
import javax.websocket.Session;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.tomitribe.microscoped.core.ScopeContext;

import cito.annotation.WebSocketScope;
import cito.internal.Context;
import cito.internal.Log;
import cito.internal.QuietClosable;
import cito.internal.Context.Scope;

/**
 * Unit test for {@link WebSocketSessionProvider}.
 *
 * @author Daniel Siviter
 * @since v1.0 [17 Apr 2017]
 */

@ExtendWith(MockitoExtension.class)
public class WebSocketSessionProviderTest {
	@Mock
	public Log log;
	@Mock
	public BeanManager beanManager;
	@Mock
	private Context context;

	@InjectMocks
	public WebSocketSessionProvider provider;

	private Scope scope;

	@BeforeEach
	public void beforeAll() {
		this.scope = Context.scope(this.context);
	}

	@Test
	public void scope() {
		assertNotNull(getAnnotation(WebSocketSessionProvider.class, ApplicationScoped.class));
	}

	@Test
	public void set(@Mock Session session, @Mock ScopeContext<Session> context) {
		when(session.getId()).thenReturn("sessionId");
		when(this.beanManager.getContext(WebSocketScope.class)).thenReturn(context);

		try (QuietClosable closable = this.provider.set(session)) {
			verify(this.context).put(Session.class, session);
		}
		verify(this.context).remove(Session.class);

		verify(session).getId();
		verify(this.log).debugf("Setting session. [%s]", "sessionId");
		verify(this.beanManager).getContext(WebSocketScope.class);
		verify(context).enter(session);
		verify(context).exit(null);
		verifyNoMoreInteractions(session, context);
	}

	@AfterEach
	public void after() {
		this.scope.close();
		verifyNoMoreInteractions(this.log, this.beanManager);
	}
}
