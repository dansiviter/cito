/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.annotation.OnSend.Literal.onSend;
import static cito.annotation.OnSubscribe.Literal.onSubscribe;
import static cito.annotation.OnUnsubscribe.Literal.onUnsubscribe;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.ObserverMethod;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.ReflectionUtil;
import cito.annotation.OnConnected;
import cito.annotation.OnDisconnect;
import cito.annotation.OnSend;
import cito.annotation.OnSubscribe;
import cito.annotation.OnUnsubscribe;
import cito.event.Message;
import cito.event.Message.Type;
import cito.internal.Context.Scope;

/**
 * Unit test for {@link EventProducer}.
 *
 * @author Daniel Siviter
 * @since v1.0 [25 Jul 2016]
 */
@ExtendWith(MockitoExtension.class)
public class EventProducerTest {

	@Mock
	private BeanManager beanManager;
	@Mock
	private MessageObserverExtension extension;
	@Mock
	private ObserverMethod<Message> observerMethod;

	@InjectMocks
	private EventProducer eventProducer;

	private Scope scope;

	@BeforeEach
	public void before() {
		this.scope = Context.scope(Context.create());
		when(this.beanManager.getExtension(MessageObserverExtension.class)).thenReturn(this.extension);
	}

	@Test
	public void message_CONNECTED(@Mock Message message) {
		when(this.extension.getMessageObservers(OnConnected.class)).thenReturn(Collections.singleton(this.observerMethod));
		when(message.type()).thenReturn(Optional.of(Type.CONNECTED));

		this.eventProducer.message(message);

		verify(this.beanManager).getExtension(MessageObserverExtension.class);
		verify(this.extension).getMessageObservers(OnConnected.class);
		verify(this.observerMethod).notify(message);
	}

	@Test
	public void message_SEND(@Mock Message message) {
		when(this.extension.getMessageObservers(OnSend.class)).thenReturn(Collections.singleton(this.observerMethod));
		when(observerMethod.getObservedQualifiers()).thenReturn(Collections.singleton(onSend("topic/*")));
		when(message.type()).thenReturn(Optional.of(Type.SEND));
		when(message.destination()).thenReturn(Optional.of("topic/*"));

		this.eventProducer.message(message);

		verify(this.beanManager).getExtension(MessageObserverExtension.class);
		verify(this.extension).getMessageObservers(OnSend.class);
		verify(this.observerMethod).getObservedQualifiers();
		verify(this.observerMethod).notify(message);
	}

	@Test
	public void message_SUBSCRIBE(@Mock Message message) {
		when(this.extension.getMessageObservers(OnSubscribe.class)).thenReturn(Collections.singleton(this.observerMethod));
		when(observerMethod.getObservedQualifiers()).thenReturn(Collections.singleton(onSubscribe("topic/*")));
		when(message.type()).thenReturn(Optional.of(Type.SUBSCRIBE));
		when(message.id()).thenReturn(Optional.of("ABC123"));
		when(message.destination()).thenReturn(Optional.of("topic/*"));

		this.eventProducer.message(message);

		verify(this.beanManager).getExtension(MessageObserverExtension.class);
		verify(this.extension).getMessageObservers(OnSubscribe.class);
		verify(this.observerMethod).getObservedQualifiers();
		verify(this.observerMethod).notify(message);
		verifyNoMoreInteractions(this.observerMethod);
	}

	@Test
	public void message_UNSUBSCRIBE(@Mock Message message) {
		when(this.extension.getMessageObservers(OnUnsubscribe.class)).thenReturn(Collections.singleton(observerMethod));
		when(this.observerMethod.getObservedQualifiers()).thenReturn(Collections.singleton(onUnsubscribe("topic/*")));
		ReflectionUtil.<Map<String,String>>get(this.eventProducer, "idDestinationMap").put("id", "topic/foo");
		when(message.type()).thenReturn(Optional.of(Type.UNSUBSCRIBE));
		when(message.id()).thenReturn(Optional.of("id"));

		this.eventProducer.message(message);

		verify(this.beanManager).getExtension(MessageObserverExtension.class);
		verify(this.extension).getMessageObservers(OnUnsubscribe.class);
		verify(this.observerMethod).getObservedQualifiers();
		verify(this.observerMethod).notify(message);
	}

	@Test
	public void message_DISCONNECT(@Mock Message message) {
		when(this.extension.getMessageObservers(OnDisconnect.class)).thenReturn(Collections.singleton(observerMethod));
		when(message.type()).thenReturn(Optional.of(Type.DISCONNECT));

		this.eventProducer.message(message);

		verify(this.beanManager).getExtension(MessageObserverExtension.class);
		verify(this.extension).getMessageObservers(OnDisconnect.class);
		verify(this.observerMethod).notify(message);
		verifyNoMoreInteractions(this.observerMethod);
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.beanManager, this.extension, this.observerMethod);
		this.scope.close();
	}
}
