/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [27 Dec 2019]
 */
@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
public class ConnectionRegistryTest {
	@Mock
	private Log log;

	@InjectMocks
	private ConnectionRegistry registry;

	@Test
	@Order(1)
	public void register(@Mock Connection connection) {
		when(connection.getSessionId()).thenReturn("ABC123");

		this.registry.register(connection);

		verify(connection).getSessionId();
		verify(this.log).debugf("Registering connection. [%s]", "ABC123");
	}

	@Test
	@Order(2)
	public void register_existing(@Mock Connection connection) {
		when(connection.getSessionId()).thenReturn("ABC123");

		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			this.registry.register(connection);
		});
		assertEquals("Session already registered! [ABC123]", ex.getMessage());

		verify(connection).getSessionId();
		verify(this.log).debugf("Registering connection. [%s]", "ABC123");
	}

	@Test
	@Order(3)
	public void get() {
		Optional<? extends Connection> connection = this.registry.get("ABC123");

		assertTrue(connection.isPresent());
	}

	@Test
	@Order(4)
	public void unregister(@Mock Connection connection) {
		when(connection.getSessionId()).thenReturn("ABC123");

		this.registry.unregister(connection);

		verify(connection).getSessionId();
		verify(this.log).debugf("Un-registering session. [%s]", "ABC123");
	}

	@Test
	@Order(5)
	public void unregister_missing(@Mock Connection connection) {
		when(connection.getSessionId()).thenReturn("ABC123");

		IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> {
			this.registry.unregister(connection);
		});
		assertEquals("Session not registered! [ABC123]", ex.getMessage());

		verify(connection).getSessionId();
		verify(this.log).debugf("Un-registering session. [%s]", "ABC123");
	}

	@Test
	@Order(6)
	public void systemConnection(@Mock SystemConnection connection) {
		when(connection.getSessionId()).thenReturn("$Y$T3M");

		this.registry.register(connection);

		SystemConnection actual = this.registry.systemConnection();
		assertEquals(actual, connection);
	}
}
