/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Optional;
import java.util.OptionalInt;

import javax.jms.BytesMessage;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.TextMessage;
import javax.jms.Topic;

import org.glassfish.tyrus.core.uri.internal.MultivaluedHashMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.ReflectionUtil;
import cito.event.Message;
import cito.internal.AbstractConnection;
import cito.internal.MessageBuilderFactory;
import cito.internal.MessageBuilderFactory.MessageBuilder;
import cito.internal.stomp.Header.Standard;

/**
 * Unit tests for {@link Factory}.
 *
 * @author Daniel Siviter
 * @since v1.0 [29 Oct 2016]
 */
@ExtendWith(MockitoExtension.class)
public class FactoryTest {
	@Mock
	private MessageBuilderFactory messageBuilderFactory;

	@InjectMocks
	private Factory factory;

	@Test
	public void toSession() throws JMSException {
		final AbstractConnection conn = mock(AbstractConnection.class);
		final JMSContext jmsConn = mock(JMSContext.class);
		when(conn.getDelegate()).thenReturn(jmsConn);
		final JMSContext jmsSession = mock(JMSContext.class);
		when(jmsConn.createContext(JMSContext.AUTO_ACKNOWLEDGE)).thenReturn(jmsSession);

		final Session session = this.factory.toSession(conn, JMSContext.AUTO_ACKNOWLEDGE);
		assertEquals(jmsSession, ReflectionUtil.get(session, "delegate"));

		verify(conn).getDelegate();
		verify(jmsConn).createContext(JMSContext.AUTO_ACKNOWLEDGE);
		verifyNoMoreInteractions(conn, jmsConn, jmsSession);
	}

	@Test
	public void toDestination_queue() throws JMSException {
		final JMSContext session = mock(JMSContext.class);

		this.factory.toDestination(session, "queue/foo");

		verify(session).createQueue("foo");
		verifyNoMoreInteractions(session);
	}

	@Test
	public void toDestination_topic() throws JMSException {
		final JMSContext session = mock(JMSContext.class);

		this.factory.toDestination(session, "topic/foo");

		verify(session).createTopic("foo");
		verifyNoMoreInteractions(session);
	}

	@Test
	public void fromDestination_queue() throws JMSException {
		final Queue q = mock(Queue.class);
		when(q.getQueueName()).thenReturn("foo");

		final String output = this.factory.fromDestination(q);
		assertEquals("queue/foo", output);

		verify(q).getQueueName();
		verifyNoMoreInteractions(q);
	}

	@Test
	public void fromDestination_topic() throws JMSException {
		final Topic t = mock(Topic.class);
		when(t.getTopicName()).thenReturn("foo");

		final String output = this.factory.fromDestination(t);
		assertEquals("topic/foo", output);

		verify(t).getTopicName();
		verifyNoMoreInteractions(t);
	}

	@Test
	public void toMessage_bytes(
		@Mock JMSContext ctx,
		@Mock Message msg,
		@Mock BytesMessage bytesMsg)
	throws JMSException
	{
		final ByteBuffer buffer = ByteBuffer.wrap(new byte[1]).asReadOnlyBuffer();
		when(msg.body()).thenReturn(Optional.of(buffer));
		when(msg.headers()).thenReturn(new MultivaluedHashMap<>());
		when(msg.contentLength()).thenReturn(OptionalInt.of(1));
		when(ctx.createBytesMessage()).thenReturn(bytesMsg);

		this.factory.toMessage(ctx, msg);

		verify(msg).body();
		verify(msg, times(2)).headers();
		verify(msg).contentLength();
		verify(ctx).createBytesMessage();
		verify(bytesMsg).setJMSCorrelationID(null);
		verify(bytesMsg).writeBytes(any(), eq(0), eq(1));
		verifyNoMoreInteractions(ctx, msg, bytesMsg);
	}

	@Test
	public void toMessage_text(
		@Mock JMSContext ctx,
		@Mock Message msg,
		@Mock TextMessage textMsg)
	throws JMSException
	{
		final ByteBuffer buffer = ByteBuffer.wrap(new byte[0]).asReadOnlyBuffer();
		when(msg.body()).thenReturn(Optional.of(buffer));
		when(msg.headers()).thenReturn(new MultivaluedHashMap<>());
		when(msg.contentLength()).thenReturn(OptionalInt.empty());
		when(ctx.createTextMessage("")).thenReturn(textMsg);

		this.factory.toMessage(ctx, msg);

		verify(msg).body();
		verify(msg, times(2)).headers();
		verify(msg).contentLength();
		verify(ctx).createTextMessage("");
		verify(textMsg).setJMSCorrelationID(null);
		verifyNoMoreInteractions(ctx, msg, textMsg);
	}


	@Test

	public void toMessage_textMessage(
		@Mock MessageBuilder<?> builder,
		@Mock TextMessage textMsg)
	throws JMSException, IOException
	{
		when(this.messageBuilderFactory.builder()).thenAnswer(b -> builder);  // avoid generics errors
		when(textMsg.getPropertyNames()).thenReturn(Collections.enumeration(Collections.singleton("hello")));
		when(textMsg.getText()).thenReturn("");

		this.factory.toMessage(textMsg, "subscriptionId");

		verify(textMsg).getPropertyNames();
		verify(textMsg).getText();
		verify(textMsg).getJMSMessageID();
		verify(textMsg).getJMSDestination();
		verify(textMsg).getJMSCorrelationID();
		verify(textMsg).getJMSExpiration();
		verify(textMsg).getJMSRedelivered();
		verify(textMsg).getJMSPriority();
		verify(textMsg).getJMSReplyTo();
		verify(textMsg).getJMSTimestamp();
		verify(textMsg).getJMSType();
		verify(textMsg).getStringProperty("hello");
		verify(textMsg).getStringProperty(Standard.CONTENT_TYPE.value());
		verify(textMsg).getText();
		verifyNoMoreInteractions(textMsg);
	}

	@Test
	public void toFrame_bytesMessage(@Mock MessageBuilder<?> builder, @Mock BytesMessage message) throws IOException, JMSException {
		when(this.messageBuilderFactory.builder()).thenAnswer(b -> builder);  // avoid generics errors
		when(message.getPropertyNames()).thenReturn(Collections.enumeration(Collections.singleton("hello")));

		this.factory.toMessage(message, "subscriptionId");

		verify(message).getPropertyNames();
		verify(message).getJMSMessageID();
		verify(message).getJMSDestination();
		verify(message).getJMSCorrelationID();
		verify(message).getJMSExpiration();
		verify(message).getJMSRedelivered();
		verify(message).getJMSPriority();
		verify(message).getJMSReplyTo();
		verify(message).getJMSTimestamp();
		verify(message).getJMSType();
		verify(message).getStringProperty("hello");
		verify(message).getStringProperty(Standard.CONTENT_TYPE.value());
		verify(message).getBodyLength();
		verify(message).readBytes(new byte[0]);
		verifyNoMoreInteractions(message);
	}

	@Test
	public void toJmsKey() {
		assertEquals("my_HYPHEN_COMPLEX_DOT_key", Factory.toJmsKey(Header.valueOf("my-COMPLEX.key")));
	}

	@Test
	public void toStompKey() {
		assertEquals("my-COMPLEX.key", Factory.toStompKey("my_HYPHEN_COMPLEX_DOT_key").value());
	}
}
