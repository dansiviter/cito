/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static cito.internal.stomp.Header.Standard.LOGIN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Provider;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.security.auth.login.LoginException;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.RemoteEndpoint.Basic;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.MimeTypes;
import cito.ReflectionUtil;
import cito.event.Message;
import cito.internal.Log;
import cito.internal.stomp.Header.Standard;
import cito.security.SecurityContext;

/**
 * Unit tests for {@link StompConnection}.
 *
 * @author Daniel Siviter
 * @since v1.0 [25 Jul 2016]
 */
@ExtendWith(MockitoExtension.class)
public class StompConnectionTest {
	@Mock
	private Log log;
	@Mock
	private BeanManager beanManager;
	@Mock
	private ConnectionFactory connectionFactory;
	@Mock
	private Factory factory;
	@Mock
	private ScheduledExecutorService scheduler;
	@Mock
	private Provider<javax.websocket.Session> wsSessionProvider;
	@Mock
	private javax.websocket.Session wsSession;
	@Mock
	private Provider<SecurityContext> securityCtx;
	@Mock
	private HeartBeatMonitor heartBeatMonitor;

	@InjectMocks
	private StompConnection connection;

	@BeforeEach
	public void before() {
		ReflectionUtil.set(this.connection, "sessionId", "ABC123");
	}

	@Test
	public void send_frame(@Mock StompMessage msg, @Mock Basic basic) throws IOException {
		ReflectionUtil.set(this.connection, "wsSession", this.wsSession);
		final Frame frame = Frame.message("destination", "subscriptionId", "messageId", MimeTypes.TEXT_PLAIN, "body").build();
		when(msg.frame()).thenReturn(frame);
		when(this.wsSession.getBasicRemote()).thenReturn(basic);

		this.connection.sendToClient(msg);

		verify(heartBeatMonitor).resetSend();
		verify(this.log).infof("Sending message to client. [sessionId=%s,command=%s]", "ABC123", Command.MESSAGE);
		verify(this.wsSession).getBasicRemote();
		verify(basic).sendBinary(any());
		verifyNoMoreInteractions(basic);
	}

	@Test
	public void send_frame_HEARTBEAT(@Mock StompMessage msg, @Mock Basic basic) throws IOException {
		ReflectionUtil.set(this.connection, "wsSession", this.wsSession);
		when(this.wsSession.getBasicRemote()).thenReturn(basic);
		final Frame frame = Frame.HEART_BEAT;
		when(msg.frame()).thenReturn(frame);

		this.connection.sendToClient(msg);

		verify(heartBeatMonitor).resetSend();
		verify(this.log).debugf("Sending message to client. [sessionId=%s,command=HEARTBEAT]", "ABC123");
		verify(this.wsSession).getBasicRemote();
		verify(basic).sendBinary(any());
		verifyNoMoreInteractions(basic);
	}

	@Test
	public void connect(@Mock StompMessage msg, @Mock JMSContext ctx, @Mock Basic basic)
	throws JMSException, LoginException, IOException
	{
		ReflectionUtil.set(this.connection, "wsSession", this.wsSession);
		ReflectionUtil.set(this.connection, "sessionId", null); // every other test needs it set!
		Frame frame = Frame.connect("myhost.com", StompVersion.V1_O).header(LOGIN, "joe").build();
		when(msg.frame()).thenReturn(frame);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(this.connectionFactory.createContext("joe", null)).thenReturn(ctx);
		when(this.wsSession.getNegotiatedSubprotocol()).thenReturn(StompVersion.V1_0_SUBPROTOCOL);
		when(this.wsSession.getBasicRemote()).thenReturn(basic);

		this.connection.connect(msg);

		verify(this.log).infof("Connecting... [%s]", "ABC123");
		verify(this.connectionFactory).createContext("joe", null);
		verify(heartBeatMonitor).resetSend();
		verify(this.log).infof("Starting JMS connection... [%s]", "ABC123");
		verify(ctx).setClientID("ABC123");
		verify(ctx).start();
		verify(this.log).infof("Sending message to client. [sessionId=%s,command=%s]", "ABC123", Command.CONNECTED);
		verify(this.wsSession).getBasicRemote();
		verify(basic).sendBinary(any());
		verifyNoMoreInteractions(msg, wsSession, ctx, basic);
	}

	@Test
	public void connect_guest(@Mock StompMessage msg, @Mock JMSContext ctx, @Mock Basic basic)
	throws JMSException, LoginException, IOException
	 {
		ReflectionUtil.set(this.connection, "wsSession", this.wsSession);
		ReflectionUtil.set(this.connection, "sessionId", null); // every other test needs it set!
		final Frame frame = Frame.connect("myhost.com", StompVersion.V1_O).build();
		when(msg.frame()).thenReturn(frame);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(this.connectionFactory.createContext()).thenReturn(ctx);
		when(this.wsSession.getNegotiatedSubprotocol()).thenReturn(StompVersion.V1_0_SUBPROTOCOL);
		when(this.wsSession.getBasicRemote()).thenReturn(basic);

		this.connection.connect(msg);

		verify(this.log).infof("Connecting... [%s]", "ABC123");
		verify(this.securityCtx).get();
		verify(this.connectionFactory).createContext();
		verify(heartBeatMonitor).resetSend();
		verify(this.log).infof("Starting JMS connection... [%s]", "ABC123");
		verify(ctx).setClientID("ABC123");
		verify(ctx).start();
		verify(this.log).infof("Sending message to client. [sessionId=%s,command=%s]", "ABC123", Command.CONNECTED);
		verify(this.wsSession).getBasicRemote();
		verify(basic).sendBinary(any());
		verifyNoMoreInteractions(ctx, basic);
	}


	@Test
	public void connect_connected(@Mock StompMessage msg)
	throws JMSException, LoginException, IOException
	 {
		IllegalStateException actual = assertThrows(IllegalStateException.class, () -> {
			this.connection.connect(msg);
		});
		assertEquals("Already connected!", actual.getMessage());
	}

	@Test
	public void connect_noSessionId(@Mock StompMessage msg)
	throws JMSException, LoginException, IOException
	{
		ReflectionUtil.set(this.connection, "sessionId", null); // every other test needs it set!

		IllegalArgumentException actual = assertThrows(IllegalArgumentException.class, () -> {
			this.connection.connect(msg);
		});
		assertEquals("Session ID cannot be null!", actual.getMessage());
	}

	@Test
	public void connect_versionMismatch(@Mock StompMessage msg, @Mock JMSContext ctx, @Mock Basic basic)
	throws JMSException, LoginException, IOException
	{
		ReflectionUtil.set(this.connection, "wsSession", this.wsSession);
		ReflectionUtil.set(this.connection, "sessionId", null); // every other test needs it set!
		final Frame frame = Frame.connect("myhost.com", StompVersion.V1_O).header(LOGIN, "joe").build();
		when(msg.frame()).thenReturn(frame);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(this.wsSession.getNegotiatedSubprotocol()).thenReturn(StompVersion.V1_1_SUBPROTOCOL);
		when(this.wsSession.getBasicRemote()).thenReturn(basic);

		IllegalStateException actual = assertThrows(IllegalStateException.class, () -> {
			this.connection.connect(msg);
		});
		assertEquals("Incompatible subprotocol and 'accept-version'! [subprotocol=v11.stomp,accept-version=[1.0]]", actual.getMessage());

		verify(this.log).infof("Connecting... [%s]", "ABC123");
		verify(this.log).infof("Sending message to client. [sessionId=%s,command=%s]", "ABC123", Command.ERROR);
		verify(basic).sendBinary(any());
	}

	@Test
	public void on_wrongSession(@Mock StompMessage msg) {
		final Frame frame = Frame.disconnect().build();
		when(msg.frame()).thenReturn(frame);
		when(msg.sessionId()).thenReturn(Optional.of("Another"));

		assertThrows(IllegalStateException.class, () -> {
			this.connection.on(msg);
		}, "Session identifier mismatch! [expected=ABC123,actual=Another]");
	}

	@Test
	public void on_CONNECT(@Mock StompMessage msg, @Mock JMSContext ctx, @Mock Basic basic)
	throws IOException {
		ReflectionUtil.set(this.connection, "sessionId", null);
		ReflectionUtil.set(this.connection, "wsSession", this.wsSession);
		when(this.connectionFactory.createContext()).thenReturn(ctx);
		final Frame frame = Frame.connect("myhost.com", StompVersion.V1_O).build();
		when(msg.frame()).thenReturn(frame);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(this.connectionFactory.createContext()).thenReturn(ctx);
		when(this.wsSession.getNegotiatedSubprotocol()).thenReturn(StompVersion.V1_0_SUBPROTOCOL);
		when(this.wsSession.getBasicRemote()).thenReturn(basic);

		this.connection.on(msg);

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.CONNECT);
		verify(this.log).infof("CONNECT/STOMP recieved. Opening connection to broker. [%s]", Optional.of("ABC123"));
		verify(this.log).infof("Connecting... [%s]","ABC123");
		verify(this.securityCtx).get();
		verify(this.connectionFactory).createContext();
		verify(heartBeatMonitor).resetRead();
		verify(heartBeatMonitor).resetSend();
		verify(this.log).infof("Starting JMS connection... [%s]", "ABC123");
		verify(ctx).setClientID("ABC123");
		verify(ctx).start();
		verify(this.log).infof("Sending message to client. [sessionId=%s,command=%s]", "ABC123", Command.CONNECTED);
		verify(this.wsSession).getBasicRemote();
		verify(basic).sendBinary(any());
		verifyNoMoreInteractions(msg, ctx, basic);
	}

	@Test
	public void on_DISCONNECT(@Mock StompMessage msg) throws IOException {
		when(this.wsSession.isOpen()).thenReturn(true);
		when(msg.frame()).thenReturn(Frame.disconnect().build());
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));

		this.connection.on(msg);

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.DISCONNECT);
		verify(this.log).infof("DISCONNECT recieved. Closing connection to broker. [%s]", "ABC123");
		verify(this.log).infof("Closing connection. [sessionId=%s,code=%s,reason=%s]",
				"ABC123", CloseCodes.NORMAL_CLOSURE.getCode(), "Disconnect");
		verify(this.wsSession).isOpen();
		verify(this.wsSession).close(any());
		verifyNoMoreInteractions(msg);
	}

	@Test
	public void on_SEND(@Mock StompMessage msg, @Mock Session session) throws JMSException {
		ReflectionUtil.set(this.connection, "session", session);
		final Frame frame = Frame.send("/there", null, "%s").build();
		when(msg.frame()).thenReturn(frame);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));

		this.connection.on(msg);

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.SEND);
		verifyNoMoreInteractions(msg, session);
	}

	@Test
	public void on_SEND_jmsDestination(@Mock StompMessage msg, @Mock Session session) throws JMSException {
		ReflectionUtil.set(this.connection, "session", session);
		final Frame frame = Frame.send("topic/there", null, "%s").build();
		when(msg.frame()).thenReturn(frame);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));

		this.connection.on(msg);

		verify(session).sendToBroker(msg);
		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.SEND);
		verifyNoMoreInteractions(msg, session);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void on_ACK(@Mock StompMessage msg, @Mock javax.jms.Message jmsMsg) throws JMSException {
		when(msg.frame()).thenReturn(Frame.builder(Command.ACK).header(Standard.ID, "1").build());
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		ReflectionUtil.get(this.connection, "ackMessages", Map.class).put("1", jmsMsg);

		this.connection.on(msg);

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.ACK);
		verify(jmsMsg).acknowledge();
		verifyNoMoreInteractions(jmsMsg);
	}

	@Test
	public void on_ACK_noExist(@Mock StompMessage msg) {
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.ACK).header(Standard.ID, "1").build());

		assertThrows(IllegalStateException.class, () -> {
			this.connection.on(msg);
		}, "No such message to ACK! [1]");

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.ACK);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void on_NACK(@Mock StompMessage msg, @Mock javax.jms.Message jmsMsg) {
		ReflectionUtil.get(this.connection, "ackMessages", Map.class).put("1", jmsMsg);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.NACK).header(Standard.ID, "1").build());

		this.connection.on(msg);

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.NACK);
		verify(this.log).warnf("NACK recieved, but no JMS equivalent! [%s]", "1");
		verifyNoMoreInteractions(jmsMsg);
	}

	@Test
	public void on_NACK_noExist(@Mock StompMessage msg) {
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.NACK).header(Standard.ID, "1").build());

		assertThrows(IllegalStateException.class, () -> {
			this.connection.on(msg);
		}, "No such message to NACK! [1]");

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.NACK);
	}

	@Test
	public void on_BEGIN(@Mock StompMessage msg, @Mock Session txSession) throws JMSException {
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.BEGIN).header(Standard.TRANSACTION, "1").build());
		when(this.factory.toSession(this.connection, JMSContext.SESSION_TRANSACTED)).thenReturn(txSession);

		this.connection.on(msg);

		assertEquals(txSession, ReflectionUtil.get(this.connection, "txSessions", Map.class).get("1"));

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.BEGIN);
		verify(this.factory).toSession(this.connection, JMSContext.SESSION_TRANSACTED);
		verifyNoMoreInteractions(txSession);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void on_BEGIN_alreadyExists(@Mock Session txSession, @Mock StompMessage msg) {
		ReflectionUtil.get(this.connection, "txSessions", Map.class).put("1", txSession);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.BEGIN).header(Standard.TRANSACTION, "1").build());

		assertThrows(IllegalStateException.class, () -> {
			this.connection.on(msg);
		}, "Transaction already started! [1]");

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.BEGIN);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void on_COMMIT(@Mock Session txSession, @Mock StompMessage msg) throws JMSException {
		ReflectionUtil.get(this.connection, "txSessions", Map.class).put("1", txSession);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.COMMIT).header(Standard.TRANSACTION, "1").build());

		this.connection.on(msg);

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.COMMIT);
		verify(txSession).commit();
		verifyNoMoreInteractions(txSession);
	}

	@Test
	public void on_COMMIT_notExists(@Mock StompMessage msg) {
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.COMMIT).header(Standard.TRANSACTION, "1").build());

		assertThrows(IllegalStateException.class, () -> {
			this.connection.on(msg);
		}, "Transaction session does not exists! [1]");

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.COMMIT);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void on_ABORT(@Mock Session txSession, @Mock StompMessage msg) throws JMSException {
		ReflectionUtil.get(this.connection, "txSessions", Map.class).put("1", txSession);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.ABORT).header(Standard.TRANSACTION, "1").build());

		this.connection.on(msg);

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.ABORT);
		verify(txSession).rollback();
		verifyNoMoreInteractions(txSession);

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.ABORT);
	}

	@Test
	public void on_ABORT_notExists(@Mock StompMessage msg) {
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.ABORT).header(Standard.TRANSACTION, "1").build());

		assertThrows(IllegalStateException.class, () -> {
			this.connection.on(msg);
		}, "Transaction session does not exists! [1]");

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.ABORT);
	}

	@Test
	public void on_SUBSCRIBE(
		@Mock Session session, @Mock Destination destination, @Mock JMSConsumer consumer, @Mock StompMessage msg)
	throws JMSException
	{
		ReflectionUtil.set(this.connection, "session", session);
		when(session.toDestination("/dest")).thenReturn(destination);
		when(session.getConnection()).thenReturn(this.connection);
		when(session.createConsumer(eq(destination), any(String.class))).thenReturn(consumer);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.subscribe("1", "/dest").build());

		this.connection.on(msg);

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.SUBSCRIBE);
		verify(session).toDestination("/dest");
		verify(session).getConnection();
		verify(session).createConsumer(destination, "session IS NULL OR session = 'ABC123'");
		verify(consumer).setMessageListener(any(MessageListener.class));
		verifyNoMoreInteractions(session, destination, consumer);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void on_SUBSCRIBE_alreadyExists(@Mock Subscription subscription, @Mock StompMessage msg) {
		ReflectionUtil.get(this.connection, "subscriptions", Map.class).put("1", subscription);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.subscribe("1", "/dest").build());

		assertThrows(IllegalStateException.class, () -> {
			this.connection.on(msg);
		}, "Subscription already exists! [1]");

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.SUBSCRIBE);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void on_UNSUBSCRIBE(@Mock Subscription subscription, @Mock StompMessage msg) {
		ReflectionUtil.get(this.connection, "subscriptions", Map.class).put("1", subscription);
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.UNSUBSCRIBE).subscription("1").build());

		this.connection.on(msg);

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.UNSUBSCRIBE);
	}

	@Test
	public void on_UNSUBSCRIBE_noExist(@Mock StompMessage msg) {
		when(msg.sessionId()).thenReturn(Optional.of("ABC123"));
		when(msg.frame()).thenReturn(Frame.builder(Command.UNSUBSCRIBE).subscription("1").build());

		assertThrows(IllegalStateException.class, () -> {
			this.connection.on(msg);
		}, "Subscription does not exist! [1]");

		verify(this.log).infof("Message received from client. [sessionId=%s,command=%s]", Optional.of("ABC123"), Command.UNSUBSCRIBE);
	}

	@Test
	public void addAckMessage() throws JMSException {
		final javax.jms.Message msg = mock(javax.jms.Message.class);
		when(msg.getJMSMessageID()).thenReturn("foo");

		this.connection.addAckMessage(msg);

		assertEquals(msg, ReflectionUtil.<Map<String, Message>>get(this.connection, "ackMessages").get("foo"));

		verify(msg).getJMSMessageID();
		verifyNoMoreInteractions(msg);
	}

	@Test
	public void close_closeReason() throws IOException, JMSException {
		final HeartBeatMonitor heartBeatMonitor = mock(HeartBeatMonitor.class);
		ReflectionUtil.set(this.connection, "heartBeatMonitor", heartBeatMonitor);
		final JMSContext jmsConnection = mock(JMSContext.class);
		ReflectionUtil.set(this.connection, "delegate", jmsConnection);
		final CloseReason reason = new CloseReason(CloseCodes.CANNOT_ACCEPT, "Aggghhh!");
		when(this.wsSession.isOpen()).thenReturn(true);

		this.connection.close(reason);

		verify(this.log).infof("Closing connection. [sessionId=%s,code=%s,reason=%s]", "ABC123", CloseCodes.CANNOT_ACCEPT.getCode(), "Aggghhh!");
		verify(jmsConnection).close();
		verify(heartBeatMonitor).close();
		verify(this.wsSession).isOpen();
		verify(this.wsSession).close(any());
		verifyNoMoreInteractions(heartBeatMonitor, jmsConnection);
	}

	@Test
	public void toString_() {
		assertThat(this.connection.toString(), startsWith(StompConnection.class.getName() + "@"));
		assertThat(this.connection.toString(), endsWith("{sessionId=ABC123}"));
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.log,
				this.beanManager,
				this.connectionFactory,
				this.factory,
				this.scheduler,
				this.wsSessionProvider,
				this.wsSession,
				this.securityCtx);
	}
}
