/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp.ws;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.websocket.DecodeException;
import javax.websocket.EndpointConfig;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.internal.stomp.Command;
import cito.internal.stomp.Frame;

/**
 * Unit test for {@link FrameDecoder}.
 *
 * @author Daniel Siviter
 * @since v1.0 [24 Nov 2017]
 */
@ExtendWith(MockitoExtension.class)
public class FrameDecoderTest {
	@Mock
	private EndpointConfig config;

	private FrameDecoder binary;

	@BeforeEach
	public void before() {
		this.binary = new FrameDecoder();
	}

	@Test
	public void decode_byteBuffer() throws DecodeException, IOException {
		final String input = "MESSAGE\ndestination:wonderland\nsubscription:a\ncontent-length:4\n\nbody\u0000";
		final Frame frame;
		try (InputStream is = new ByteArrayInputStream(input.getBytes()))  {
			frame = binary.decode(null, ByteBuffer.wrap(input.getBytes(UTF_8)));
		}
		assertEquals(Command.MESSAGE, frame.command());
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.config);
	}
}
