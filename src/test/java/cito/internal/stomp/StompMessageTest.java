/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static cito.MimeTypes.TEXT_PLAIN;
import static cito.event.Message.Type.SEND;
import static cito.internal.stomp.StompVersion.V1_0_SUBPROTOCOL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.internal.MessageBuilderFactory;

/**
 * Unit tests for {@link StompMessage}.
 *
 * @author Daniel Siviter
 * @since v1.0 [28 Dec 2019]
 */
@ExtendWith(MockitoExtension.class)
public class StompMessageTest {
	@Test
	public void builder_nullFactory() {
		NullPointerException ex = assertThrows(NullPointerException.class, () -> {
			StompMessage.builder(null, V1_0_SUBPROTOCOL);
		});
		assertEquals("No factory!", ex.getMessage());
	}

	@Test
	public void builder_nullSubprotocol(@Mock MessageBuilderFactory factory) {
		NullPointerException ex = assertThrows(NullPointerException.class, () -> {
			StompMessage.builder(factory, null);
		});
		assertEquals("No subprotocol!", ex.getMessage());
	}

	@Test
	public void build_unknownType(@Mock MessageBuilderFactory factory) {
		IllegalStateException ex = assertThrows(IllegalStateException.class, () -> {
			StompMessage.builder(factory, V1_0_SUBPROTOCOL).build();
		});
		assertEquals("Type must be set!", ex.getMessage());
	}

	@Test
	public void build_SEND(@Mock MessageBuilderFactory factory) throws IOException {
		ByteBuffer body = ByteBuffer.allocate(0);
		StompMessage msg = StompMessage.builder(factory, V1_0_SUBPROTOCOL).type(SEND).destination("foo")
				.body(body, TEXT_PLAIN).build();
		assertEquals("foo", msg.destination().get());
		assertEquals(body, msg.body().get());
		assertEquals(0, msg.contentLength().getAsInt());
		assertEquals(TEXT_PLAIN, msg.contentType().get());
	}

	@Test
	public void build_SEND_noDestination(@Mock MessageBuilderFactory factory) {
		AssertionError ex = assertThrows(AssertionError.class, () -> {
			StompMessage.builder(factory, V1_0_SUBPROTOCOL).type(SEND).build();
		});
		assertEquals("Required header 'destination' not set on 'SEND' frame!", ex.getMessage());
	}
}
