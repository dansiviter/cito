/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.inject.Provider;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.event.Message;
import cito.internal.Connection;
import cito.internal.Log;
import cito.internal.MessageBuilderFactory;
import cito.internal.MessageBuilderFactory.MessageBuilder;

/**
 * Unit test for {@link ErrorHandler}.
 *
 * @author Daniel Siviter
 * @since v1.0 [28 Mar 2017]
 */
@ExtendWith(MockitoExtension.class)
public class ErrorHandlerTest {
	@Mock
	private Log log;
	@Mock
	private Provider<MessageBuilderFactory> factoryProvider;

	@InjectMocks
	private ErrorHandler errorHandler;

	@Test
	public void onError(
			@Mock Connection connection,
			@Mock Message cause,
			@Mock MessageBuilderFactory factory,
			@Mock MessageBuilder<Message> builder)
	throws IOException
	{
		when(this.factoryProvider.get()).thenReturn(factory);
		when(factory.builder()).thenAnswer(b -> builder);  // avoid generics errors
		when(builder.error(any())).thenReturn(builder);
		final Exception e = new Exception("Oh no!");

		this.errorHandler.onError(connection, "sessionId", cause, "oooh", e);

		verify(this.log).warnf(e, "Error while processing frame! [sessionId=%s]", "sessionId");
		verify(connection).on(any());
		verify(connection).close(any());
		verifyNoMoreInteractions(connection);
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.log);

	}
}
