/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.startsWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Optional;

import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.ReflectionUtil;
import cito.event.Message;

/**
 * Unit tests for {@link Session}.
 *
 * @author Daniel Siviter
 * @since v1.0 [25 Jul 2016]
 */
@ExtendWith(MockitoExtension.class)
public class SessionTest {
	@Mock
	private StompConnection conn;
	@Mock
	private JMSContext delegate;
	@Mock
	private JMSProducer producer;
	@Mock
	private Factory factory;

	private Session session;

	@BeforeEach
	public void before() {
		this.session = new Session(this.conn, this.delegate, factory);
	}

	@Test
	public void send_frame(
		@Mock Message msg, @Mock javax.jms.Message jmsMsg, @Mock Destination destination)
	throws JMSException
	 {
		ReflectionUtil.set(this.session, "producer", this.producer);
		when(msg.destination()).thenReturn(Optional.of("/here"));
		when(this.factory.toMessage(this.delegate, msg)).thenReturn(jmsMsg);
		when(this.factory.toDestination(this.delegate, "/here")).thenReturn(destination);

		this.session.sendToBroker(msg);

		verify(msg).destination();
		verify(this.factory).toMessage(this.delegate, msg);
		verify(this.factory).toDestination(this.delegate, "/here");
		verify(this.producer).send(destination, jmsMsg);
		verifyNoMoreInteractions(msg, jmsMsg, destination);
	}

	@Test
	public void send_message(
		@Mock Message msg, @Mock javax.jms.Message jmsMsg, @Mock Subscription subscription)
	throws JMSException, IOException
	 {
		ReflectionUtil.set(this.session, "producer", this.producer);
		when(this.factory.toMessage(jmsMsg, "subscriptionId")).thenReturn(msg);
		when(this.delegate.getSessionMode()).thenReturn(JMSContext.AUTO_ACKNOWLEDGE);
		when(subscription.getId()).thenReturn("subscriptionId");

		this.session.send(jmsMsg, subscription);

		verify(this.delegate).getSessionMode();
		verify(subscription).getId();
		verify(this.factory).toMessage(jmsMsg, "subscriptionId");
		verify(this.conn).on(any());
		verifyNoMoreInteractions(jmsMsg, subscription, jmsMsg);
	}

	@Test
	public void send_message_ack(
		@Mock Message msg, @Mock javax.jms.Message jmsMsg, @Mock Subscription subscription)
	throws JMSException, IOException
	{
		ReflectionUtil.set(this.session, "producer", this.producer);
		when(this.factory.toMessage(jmsMsg, "subscriptionId")).thenReturn(msg);
		when(this.delegate.getSessionMode()).thenReturn(JMSContext.CLIENT_ACKNOWLEDGE);
		when(subscription.getId()).thenReturn("subscriptionId");

		this.session.send(jmsMsg, subscription);

		verify(this.delegate).getSessionMode();
		verify(this.conn).addAckMessage(jmsMsg);
		verify(subscription).getId();
		verify(this.factory).toMessage(jmsMsg, "subscriptionId");
		verify(this.conn).on(any());
		verifyNoMoreInteractions(jmsMsg, subscription, jmsMsg);
	}

	@Test
	public void toString_() {
		assertThat(this.session.toString(), startsWith(Session.class.getName() + "@"));
		assertThat(this.session.toString(), endsWith("{connection=conn,session=delegate}"));
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.conn, this.delegate, this.factory, this.producer);
	}
}
