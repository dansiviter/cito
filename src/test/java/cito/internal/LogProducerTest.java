/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.ReflectionUtil.getAnnotation;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.lang.reflect.Member;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.InjectionPoint;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Unit tests for {@link LogProducer}.
 *
 * @author Daniel Siviter
 * @since v1.0 [14 Apr 2017]
 */
@ExtendWith(MockitoExtension.class)
public class LogProducerTest {
	@Test
	public void scope() {
		assertNotNull(getAnnotation(LogProducer.class, ApplicationScoped.class));
	}

	@Test
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void logger_injectionPoint(@Mock InjectionPoint ip, @Mock Bean bean) {
		when(ip.getBean()).thenReturn(bean);
		when(bean.getBeanClass()).thenReturn(LogProducerTest.class);

		final Log log = LogProducer.log(ip);
		assertNotNull(log);

		verify(ip).getBean();
		verify(bean).getBeanClass();
		verifyNoMoreInteractions(ip, bean);
	}

	@Test
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void logger_injectionPoint_noBean(@Mock InjectionPoint ip, @Mock Member member) {
		when(ip.getMember()).thenReturn(member);
		when(member.getDeclaringClass()).thenReturn((Class) LogProducerTest.class);

		final Log log = LogProducer.log(ip);
		assertNotNull(log);

		verify(ip).getBean();
		verify(ip).getMember();
		verify(member).getDeclaringClass();
		verifyNoMoreInteractions(ip, member);
	}

	@Test
	public void logger_class() {
		final Log log = LogProducer.log(LogProducerTest.class);
		assertNotNull(log);
	}
}
