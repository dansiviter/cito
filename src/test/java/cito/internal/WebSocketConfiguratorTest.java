package cito.internal;

import static java.util.Collections.emptyMap;
import static javax.websocket.server.HandshakeRequest.SEC_WEBSOCKET_PROTOCOL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

import org.glassfish.tyrus.core.uri.internal.MultivaluedHashMap;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * Unit tests for {@link WebSocketConfigurator}.
 *
 * @author Daniel Siviter
 * @since v1.0 [11 Feb 2018]
 */

@ExtendWith(MockitoExtension.class)
public class WebSocketConfiguratorTest {
	@Mock
	private ServerEndpointConfig sec;
	@Mock
	private HandshakeRequest request;
	@Mock
	private HandshakeResponse response;

	private WebSocketConfigurator configurator;

	@BeforeEach
	public void before() {
		this.configurator = new WebSocketConfigurator();
	}

	@Test
	public void modifyHandshake() {
		when(this.request.getHeaders()).thenReturn(emptyMap());

		this.configurator.modifyHandshake(this.sec, this.request, this.response);

		verify(this.request).getHeaders();
	}

	@Test
	public void modifyHandshake_setSubProtocol() {
		when(this.request.getHeaders()).thenReturn(Collections.singletonMap(SEC_WEBSOCKET_PROTOCOL, Arrays.asList("v11.stomp, v10.stomp")));
		final MultivaluedHashMap<String, String> responseHeaders = new MultivaluedHashMap<>();
		when(this.response.getHeaders()).thenReturn(responseHeaders);

		this.configurator.modifyHandshake(this.sec, this.request, this.response);
		assertEquals("v11.stomp", responseHeaders.getFirst(SEC_WEBSOCKET_PROTOCOL));

		verify(this.request).getHeaders();
		verify(this.response).getHeaders();
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.sec, this.request, this.response);
	}
}
