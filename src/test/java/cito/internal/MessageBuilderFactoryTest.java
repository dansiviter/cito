/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import javax.websocket.Session;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.ext.Serialiser;
import cito.internal.MessageBuilderFactory.MessageBuilder;
import cito.internal.stomp.StompMessage;
import cito.internal.stomp.StompVersion;

/**
 * Unit tests for {@link MessageBuilderFactory}.
 *
 * @author Daniel Siviter
 * @since v1.0 [28 Nov 2019]
 */
@ExtendWith(MockitoExtension.class)
public class MessageBuilderFactoryTest {
	@Mock
	private Session session;
	@Mock
	private Serialiser serialiser;

	@InjectMocks
	private MessageBuilderFactory factory;

	@Test
	public void builder_subprotocol_stomp10() {
		MessageBuilder<?> builder = factory.builder(StompVersion.V1_0_SUBPROTOCOL);
		assertThat(builder, isA(StompMessage.Builder.class));
	}

	@Test
	public void builder_stomp10() {
		when(session.getNegotiatedSubprotocol()).thenReturn(StompVersion.V1_0_SUBPROTOCOL);
		MessageBuilder<?> builder = factory.builder();
		assertThat(builder, isA(StompMessage.Builder.class));
	}

	@Test
	public void builder_subprotocol_stomp11() {
		MessageBuilder<?> builder = factory.builder(StompVersion.V1_1_SUBPROTOCOL);
		assertThat(builder, isA(StompMessage.Builder.class));
	}

	@Test
	public void builder_stomp11() {
		when(session.getNegotiatedSubprotocol()).thenReturn(StompVersion.V1_1_SUBPROTOCOL);
		MessageBuilder<?> builder = factory.builder();
		assertThat(builder, isA(StompMessage.Builder.class));
	}

	@Test
	public void builder_subprotocol_stomp12() {
		MessageBuilder<?> builder = factory.builder(StompVersion.V1_2_SUBPROTOCOL);
		assertThat(builder, isA(StompMessage.Builder.class));
	}

	@Test
	public void builder_stomp12() {
		when(session.getNegotiatedSubprotocol()).thenReturn(StompVersion.V1_2_SUBPROTOCOL);
		MessageBuilder<?> builder = factory.builder();
		assertThat(builder, isA(StompMessage.Builder.class));
	}

	@Test
	public void builder_subprotocol_unknown() {
		IllegalStateException ex = assertThrows(IllegalStateException.class, () -> {
			factory.builder("foo");
		});
		assertEquals("Unknown subprotocol! [foo]", ex.getMessage());
	}

	@Test
	public void builder_subprotocol_null() {
		NullPointerException ex = assertThrows(NullPointerException.class, () -> {
			factory.builder(null);
		});
		assertEquals("Subprotocol expected!", ex.getMessage());
	}
}
