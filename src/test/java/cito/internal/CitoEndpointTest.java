/*

 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.enterprise.event.Event;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.BeanManager;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler.Whole;
import javax.websocket.PongMessage;
import javax.websocket.RemoteEndpoint.Basic;
import javax.websocket.Session;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.tomitribe.microscoped.core.ScopeContext;

import cito.MimeTypes;
import cito.ReflectionUtil;
import cito.annotation.WebSocketScope;
import cito.event.Message;
import cito.internal.MessageBuilderFactory.MessageBuilder;
import cito.internal.annotation.Subprotocol;
import cito.internal.stomp.Encoding;
import cito.internal.stomp.Frame;

/**
 * Unit tests for {@link CitoEndpoint}.
 *
 * @author Daniel Siviter
 * @since v1.0 [15 Jul 2016]
 */
@ExtendWith(MockitoExtension.class)
public class CitoEndpointTest {
	private static final QuietClosable CLOSABLE = () -> { };

	@Mock
	protected Log log;
	@Mock
	private BeanManager beanManager;
	@Mock
	private MessageObserverExtension extension;
	@Mock
	private ScopeContext<Session> webSocketContext;
	@Mock
	private WebSocketSessionProvider sessionProvider;
	@Mock
	private Instance<Connection> connectionInstance;
	@Mock
	private Event<Message> messageEvent;
	@Mock
	private RttService rttService;
	@Mock
	private MessageBuilderFactory factory;

	@InjectMocks
	private CitoEndpoint endpoint = new CitoEndpoint();

	@Test
	@SuppressWarnings("unchecked")
	public void onOpen(@Mock Session session, @Mock EndpointConfig config, @Mock Connection connection) {
		when(session.getId()).thenReturn("sessionId");
		when(this.sessionProvider.set(any())).thenReturn(CLOSABLE);
		when(this.connectionInstance.get()).thenReturn(connection);
		when(this.connectionInstance.select(any(Subprotocol.class))).thenReturn(this.connectionInstance);
		when(session.getNegotiatedSubprotocol()).thenReturn("foo");

		this.endpoint.onOpen(session, config);

		verify(this.connectionInstance).get();
		verify(connection).open(session, config);
		verify(session, times(2)).addMessageHandler(any(Class.class), any(Whole.class));
		verify(session, times(2)).getId();
		verify(this.sessionProvider).set(session);
		verify(session).getUserPrincipal();
		verify(this.log).infof("WebSocket connection opened. [id=%s,principal=%s]",
				"sessionId",
				null);
		verify(session).getUserProperties();
		verifyNoMoreInteractions(session, config, connection);
	}

	@Test
	public void on_message(@Mock Connection connection, @Mock Session session, @Mock MessageBuilder<Message> builder, @Mock Message msg) throws IOException {
		ReflectionUtil.set(this.endpoint, "connection", connection);
		when(session.getId()).thenReturn("sessionId");
		when(this.sessionProvider.set(any())).thenReturn(CLOSABLE);
		when(this.factory.builder()).thenAnswer(b -> builder);  // avoid generics errors
		when(builder.raw(any())).thenReturn(builder);
		when(builder.sessionId(any())).thenReturn(builder);
		when(builder.build()).thenReturn(msg);
		final Frame frame = Frame.message("destination", "subscriptionId", "messageId", MimeTypes.TEXT_PLAIN, "body").build();

		this.endpoint.on(session, Encoding.from(frame));

		verify(session).getId();
		verify(this.sessionProvider).set(session);
		verify(session).getUserPrincipal();
		verify(this.log).debugf("Received message from client. [id=%s,principal=%s]", "sessionId", null);
		verify(connection).fromClient(any());
		verify(this.messageEvent).fire(any());
		verify(this.messageEvent).fireAsync(any(), any());
		verifyNoMoreInteractions(connection, session);
	}

	@Test
	public void pong(@Mock Connection connection, @Mock Session session, @Mock PongMessage pongMessage) {
		ReflectionUtil.set(this.endpoint, "connection", connection);
		when(session.getId()).thenReturn("sessionId");
		when(this.sessionProvider.set(any())).thenReturn(CLOSABLE);

		this.endpoint.pong(session, pongMessage);

		verify(session).getId();
		verify(this.log).debugf(eq("Received pong from client. [id=%s,principal=%s]"), eq("sessionId"), isNull());
		verify(session).getUserPrincipal();
		verify(this.sessionProvider).set(session);
		verifyNoMoreInteractions(connection, session, pongMessage);
	}

	@Test
	public void onError(@Mock Session session, @Mock Basic basic) throws IOException, EncodeException {
		when(session.getId()).thenReturn("sessionId");
		when(this.sessionProvider.set(any())).thenReturn(CLOSABLE);
		final Throwable cause = new Throwable();
		when(session.getBasicRemote()).thenReturn(basic);

		this.endpoint.onError(session, cause);

		verify(session).getId();
		verify(this.sessionProvider).set(session);
		verify(session).getUserPrincipal();
		verify(this.log).warnf(eq(cause), eq("WebSocket error. [id=%s,principal=%s,errorId=%s]"), eq("sessionId"), isNull(), anyString());
		verify(session).getBasicRemote();
		verify(basic).sendBinary(any());
		verify(session).close(any(CloseReason.class));
		verifyNoMoreInteractions(session, basic);
	}

	@Test
	public void onClose(@Mock Connection connection, @Mock Session session) throws IOException {
		ReflectionUtil.set(this.endpoint, "connection", connection);
		when(session.getId()).thenReturn("sessionId");
		when(this.sessionProvider.set(any())).thenReturn(CLOSABLE);
		final CloseReason reason = new CloseReason(CloseCodes.GOING_AWAY, "oooh");
		when(this.beanManager.getContext(WebSocketScope.class)).thenReturn(this.webSocketContext);

		this.endpoint.onClose(session, reason);

		verify(session).getId();
		verify(session).getUserPrincipal();
		verify(this.log).infof("WebSocket connection closed. [id=%s,principal=%s,code=%s,reason=%s]", "sessionId", null, reason.getCloseCode(), reason.getReasonPhrase());
		verify(this.sessionProvider).set(session);
		verify(this.beanManager).getContext(WebSocketScope.class);
		verify(connection).close(reason);
		verifyNoMoreInteractions(connection, session);
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(
				this.log,
				this.beanManager,
				this.extension,
				this.sessionProvider,
				this.connectionInstance,
				this.messageEvent);
	}
}
