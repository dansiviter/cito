/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.ext;

import static cito.MimeTypes.APPLICATION_JSON;
import static cito.MimeTypes.TEXT_PLAIN;
import static cito.ReflectionUtil.set;
import static java.nio.charset.StandardCharsets.UTF_16;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.lang.reflect.Type;

import javax.activation.MimeType;
import javax.enterprise.inject.Instance;
import javax.json.bind.Jsonb;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import cito.internal.Log;

/**
 * Unit tests for {@link JsonBSerialiser}.
 *
 * @author Daniel Siviter
 * @since v1.0 [11 Feb 2018]
 */
@ExtendWith(MockitoExtension.class)
public class JsonBSerialiserTest {
	private static final MimeType APPLICATION_JSON_UTF8 = APPLICATION_JSON.toBuilder().charset("UTF-8").build();

	@Mock
	private Log log;
	@Mock
	private Instance<Jsonb> instance;

	@InjectMocks
	private JsonBSerialiser serialiser;

	@Test
	public void isReadable() {
		assertTrue(this.serialiser.isReadable(String.class, APPLICATION_JSON));
		assertTrue(this.serialiser.isReadable(Reader.class, APPLICATION_JSON));

		assertTrue(this.serialiser.isReadable(String.class, APPLICATION_JSON_UTF8));
		assertTrue(this.serialiser.isReadable(Reader.class, APPLICATION_JSON_UTF8));

		assertFalse(this.serialiser.isReadable(String.class, TEXT_PLAIN));
	}

	@Test
	public void readFrom_notSatisfed() throws IOException {
		when(this.instance.isUnsatisfied()).thenReturn(true);

		final String actual = (String) this.serialiser.readFrom(
				String.class, APPLICATION_JSON, new ByteArrayInputStream("\"input\"".getBytes(UTF_8)));

		assertEquals("input", actual);

		verify(this.instance).isUnsatisfied();
	}

	@Test
	public void readFrom(@Mock Jsonb jsonb) throws IOException {
		when(this.instance.isUnsatisfied()).thenReturn(false);
		when(this.instance.get()).thenReturn(jsonb);
		when(jsonb.fromJson(any(InputStream.class), any(Type.class))).thenReturn("input");

		final String actual = (String) this.serialiser.readFrom(
				String.class, APPLICATION_JSON, new ByteArrayInputStream("\"input\"".getBytes(UTF_16)));

		assertEquals("input", actual);

		verify(this.instance).isUnsatisfied();
		verify(this.instance).get();
		verify(jsonb).fromJson(any(InputStream.class), any(Type.class));
		verifyNoMoreInteractions(jsonb);
	}

	@Test
	public void isWriteable() {
		assertTrue(this.serialiser.isWriteable(String.class, APPLICATION_JSON));
		assertTrue(this.serialiser.isWriteable(Reader.class, APPLICATION_JSON));

		assertTrue(this.serialiser.isWriteable(String.class, APPLICATION_JSON_UTF8));
		assertTrue(this.serialiser.isWriteable(Reader.class, APPLICATION_JSON_UTF8));

		assertFalse(this.serialiser.isWriteable(String.class, TEXT_PLAIN));
	}

	@Test
	public void writeTo(@Mock Jsonb jsonb) throws IOException {
		when(this.instance.isUnsatisfied()).thenReturn(false);
		when(this.instance.get()).thenReturn(jsonb);
		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				((OutputStream) invocation.getArguments()[2]).write("\"input\"".getBytes(UTF_8));
				return null;
			}
		}).when(jsonb).toJson(any(String.class), eq(String.class), any(OutputStream.class));

		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		this.serialiser.writeTo("input", String.class, APPLICATION_JSON_UTF8, out);
		assertEquals("\"input\"", new String(out.toByteArray(), UTF_8));

		verify(this.instance).isUnsatisfied();
		verify(this.instance).get();
		verify(jsonb).toJson(any(String.class), eq(String.class), any(OutputStream.class));
		verifyNoMoreInteractions(jsonb);
	}

	@Test
	public void destroy() throws Exception {
		final Jsonb jsonb = mock(Jsonb.class);
		set(this.serialiser, "jsonb", jsonb);

		this.serialiser.destroy();

		verify(jsonb).close();
		verifyNoMoreInteractions(jsonb);
	}

	@Test
	public void destroy_exception() throws Exception {
		final Jsonb jsonb = mock(Jsonb.class);
		set(this.serialiser, "jsonb", jsonb);
		doThrow(new IllegalArgumentException()).when(jsonb).close();

		this.serialiser.destroy();

		verify(jsonb).close();
		verify(this.log).warn(eq("Unable to close Jsonb!"), any(Throwable.class));
		verifyNoMoreInteractions(jsonb);
	}


	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.log, this.instance);
	}
}
