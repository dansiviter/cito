/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.ext;

import static cito.MimeTypes.APPLICATION_JSON;
import static cito.MimeTypes.TEXT_PLAIN;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;

import javax.activation.MimeType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for {@link TextPlainSerialiser}.
 *
 * @author Daniel Siviter
 * @since v1.0 [4 Dec 2017]
 */
public class TextPlainSerialiserTest {
	private static final MimeType TEXT_PLAIN_UTF8 = TEXT_PLAIN.toBuilder().charset("UTF-8").build();

	private TextPlainSerialiser serialiser;

	@BeforeEach
	public void before() {
		this.serialiser = new TextPlainSerialiser();
	}

	@Test
	public void isReadable() {
		assertTrue(this.serialiser.isReadable(String.class, TEXT_PLAIN));
		assertTrue(this.serialiser.isReadable(Reader.class, TEXT_PLAIN));

		assertTrue(this.serialiser.isReadable(String.class, TEXT_PLAIN_UTF8));
		assertTrue(this.serialiser.isReadable(Reader.class, TEXT_PLAIN_UTF8));

		assertFalse(this.serialiser.isReadable(String.class, APPLICATION_JSON));
	}

	@Test
	public void readFrom() throws IOException {
		String expected = "input";
		String actual = (String) this.serialiser.readFrom(
				String.class, TEXT_PLAIN_UTF8, new ByteArrayInputStream(expected.getBytes(UTF_8)));

		assertEquals(expected, actual);
	}

	@Test
	public void readFrom_reader() throws IOException {
		String expected = "input";
		Reader reader = (Reader) this.serialiser.readFrom(
				Reader.class, TEXT_PLAIN_UTF8, new ByteArrayInputStream(expected.getBytes(UTF_8)));

		try (BufferedReader bufReader = new BufferedReader(reader)) {
			assertEquals(expected, bufReader.readLine());
		}
	}

	@Test
	public void readFrom_unexpected() throws IOException {
		String expected = "input";

		assertThrows(IllegalArgumentException.class, () -> {
			this.serialiser.readFrom(
				OutputStream.class, TEXT_PLAIN_UTF8, new ByteArrayInputStream(expected.getBytes(UTF_8)));
		});
	}

	@Test
	public void isWriteable() {
		assertTrue(this.serialiser.isWriteable(String.class, TEXT_PLAIN));
		assertTrue(this.serialiser.isWriteable(Reader.class, TEXT_PLAIN));

		assertTrue(this.serialiser.isWriteable(String.class, TEXT_PLAIN_UTF8));
		assertTrue(this.serialiser.isWriteable(Reader.class, TEXT_PLAIN_UTF8));

		assertFalse(this.serialiser.isWriteable(String.class, APPLICATION_JSON));
	}

	@Test
	public void writeTo() throws IOException {
		String expected = "input";
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		this.serialiser.writeTo("input", String.class, TEXT_PLAIN_UTF8, out);

		assertEquals(expected, new String(out.toByteArray(), UTF_8));
	}

	@Test
	public void writeTo_reader() throws IOException {
		String expected = "input";
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StringReader reader = new StringReader(expected);
		this.serialiser.writeTo(reader, Reader.class, TEXT_PLAIN_UTF8, out);

		assertEquals(expected, new String(out.toByteArray(), UTF_8));
	}

	@Test
	public void writeTo_unexpected() throws IOException {
		String expected = "input";
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		StringReader reader = new StringReader(expected);

		assertThrows(IllegalArgumentException.class, () -> {
			this.serialiser.writeTo(reader, OutputStream.class, TEXT_PLAIN_UTF8, out);
		});
	}
}
