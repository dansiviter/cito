/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for {@link Util}.
 *
 * @author Daniel Siviter
 * @since v1.0 [16 Apr 2017]
 */
public class UtilTest {
	@Test
	public void getFirst() {
		final String[] array = { "first", "second" };
		assertEquals("first", Util.getFirst(array));
	}

	@Test
	public void getFirst_collection() {
		final List<String> collection = Arrays.asList(new String[] { "first", "second" });
		assertEquals("first", Util.getFirst(collection));
	}

	@Test
	public void isEmpty() {
		assertTrue(Util.isEmpty(""));
		assertFalse(Util.isEmpty(null));
		assertFalse(Util.isEmpty(" "));
		assertFalse(Util.isEmpty("-"));
	}

	@Test
	public void isNullOrEmpty() {
		assertTrue(Util.isNullOrEmpty(""));
		assertTrue(Util.isNullOrEmpty(null));
		assertFalse(Util.isNullOrEmpty(" "));
		assertFalse(Util.isNullOrEmpty("-"));
	}

	@Test
	public void requireNonEmpty() {
		assertNotNull(Util.requireNonEmpty(Collections.singleton("hello")));
	}

	@Test
	public void requireNonEmpty_empty() {
		assertThrows(IllegalArgumentException.class, () -> {
			Util.requireNonEmpty(Collections.emptyList());
		}, "Collection is empty!");
	}

	@Test
	public void requireNonEmpty_null() {
		assertThrows(NullPointerException.class, () -> {
			Util.requireNonEmpty(null);
		});
	}
}
