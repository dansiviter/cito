/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito;

import static cito.MimeTypes.TEXT_PLAIN;
import static cito.internal.stomp.StompVersion.stompVersion;
import static cito.internal.stomp.StompVersion.toWsSubprotocol;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.ObservesAsync;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.DeploymentException;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.server.Server;
import org.glassfish.tyrus.test.tools.TestContainer;
import org.jboss.weld.junit5.auto.AddBeanClasses;
import org.jboss.weld.junit5.auto.AddExtensions;
import org.jboss.weld.junit5.auto.EnableAutoWeld;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.annotation.OnSend;
import cito.annotation.PathParam;
import cito.event.Message;
import cito.internal.BodyProducerExtension;
import cito.internal.CitoEndpoint;
import cito.internal.ClientMessageProducer;
import cito.internal.ContextProducer;
import cito.internal.DestinationChangedProducer;
import cito.internal.EventProducer;
import cito.internal.ExecutorServiceProducer;
import cito.internal.LogProducer;
import cito.internal.MessageBuilderFactory;
import cito.internal.MessageObserverExtension;
import cito.internal.PathParamProducer;
import cito.internal.RttService;
import cito.internal.SecurityContextProducer;
import cito.internal.WebSocketSessionProvider;
import cito.internal.stomp.Command;
import cito.internal.stomp.Encoding;
import cito.internal.stomp.ErrorHandler;
import cito.internal.stomp.Factory;
import cito.internal.stomp.Frame;
import cito.internal.stomp.HeartBeatMonitor;
import cito.internal.stomp.StompConnection;
import cito.internal.stomp.StompVersion;
import cito.junit.ThreadDumpOnFailWatcher;
import cito.security.SecurityRegistry;
import java8.util.Objects;

/**
 * @author Daniel Siviter
 * @since v1.0 [23 Nov 2019]
 */
@EnableAutoWeld
@AddExtensions({ MessageObserverExtension.class, BodyProducerExtension.class })
@AddBeanClasses({
	CitoEndpoint.class,
	LogProducer.class,
	WebSocketSessionProvider.class,
	StompConnection.class,
	ErrorHandler.class,
	SecurityRegistry.class,
	Factory.class,
	SecurityContextProducer.class,
	HeartBeatMonitor.class,
	PathParamProducer.class,
	ClientMessageProducer.class,
	DestinationChangedProducer.class,
	EventProducer.class,
	ExecutorServiceProducer.class,
	ContextProducer.class,
	RttService.class,
	MessageBuilderFactory.class,

	StompKitchenSinkTest.Service.class
})
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
@ExtendWith({ MockitoExtension.class, ThreadDumpOnFailWatcher.class })
public class StompKitchenSinkTest extends TestContainer {
	@Mock
	private ConnectionFactory connectionFactory;
	@Mock
	private JMSContext jmsContext;

	private Server server;
	private Session session;

	@Inject
	private Service service;

	@BeforeAll
	public void before() throws DeploymentException {
		this.server = startServer(CitoApplicationConfig.class);
	}

	@Produces
    @ApplicationScoped
    public ConnectionFactory connectionFactory() {
      return this.connectionFactory;
	}

	@Test
	@Order(1)
    public void connect() throws InterruptedException, IOException, DeploymentException {
		when(this.connectionFactory.createContext()).thenReturn(this.jmsContext);
		final ClientManager client = createClient();

		final BlockingQueue<Frame> queue = new SynchronousQueue<>();

		this.session = client.connectToServer(new Endpoint() {
			@Override
			public void onOpen(Session session, EndpointConfig EndpointConfig) {
					session.addMessageHandler(new MessageHandler.Whole<ByteBuffer>() {
						@Override
						public void onMessage(ByteBuffer buf) {
							try {
								final Frame frame = Encoding.from(buf);
								queue.put(frame);
							} catch (IOException | InterruptedException e) {
								throw new IllegalStateException(e);
							}
						}
					});
			}
		}, config(StompVersion.V1_2), getURI("/cito"));

		send(Frame.connect("localhost", stompVersion(session.getNegotiatedSubprotocol())).build());

		final Frame connected = await().until(queue::poll, f -> {
			return f != null && f.command() == Command.CONNECTED;
		});
		assertThat("Subprotocol matches", connected.version().get(), is(StompVersion.V1_2));

		verify(this.connectionFactory).createContext();
		verify(this.jmsContext).setClientID(any());
		verify(this.jmsContext).start();
	}

	@Test
	@Order(2)
	public void send() throws IOException, InterruptedException {
		send(Frame.send("foo.bar", TEXT_PLAIN, "Hello").build());

		final String asyncParam = await().until(this.service::getAsyncParam, v -> !Objects.isNull(v));
		assertThat(asyncParam, is("foo"));

		send(Frame.send("hello.acme", TEXT_PLAIN, "Hello").build());

		final String param = await().until(this.service::getParam, v -> !Objects.isNull(v));
		assertThat(param, is("hello"));
	}

	@Test
	@Order(5)
	public void close() throws IOException {
		session.close();
	}

	@AfterAll
	public void after() {
		this.server.stop();
	}

	private void send(Frame frame) throws IOException {
		this.session.getBasicRemote().sendBinary(Encoding.from(frame));
	}

	private static ClientEndpointConfig config(StompVersion... subprotocols) {
		return ClientEndpointConfig.Builder.create()
				.preferredSubprotocols(toWsSubprotocol(subprotocols))
				.build();
	}


	// --- Inner Classes ---

	@ApplicationScoped
	public static class Service {
		private String param;
		private String asyncParam;

		public void on(@Observes @OnSend("{param}.acme") Message msg, @PathParam("param") String param) {
			this.param = param;
		}

		public void onAsync(@ObservesAsync @OnSend("{param}.bar") Message msg, @PathParam("param") String param) {
			this.asyncParam = param;
		}

		/**
		 * @return the param
		 */
		public String getParam() {
			return param;
		}

		/**
		 * @return the asyncParam
		 */
		public String getAsyncParam() {
			return asyncParam;
		}
	}
}
