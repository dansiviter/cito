/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.security;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.security.Principal;
import java.util.List;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.ReflectionUtil;
import cito.annotation.DenyAllLiteral;
import cito.annotation.PermitAllLiteral;
import cito.annotation.RolesAllowedLiteral;
import cito.security.Builder.NullDestinationMatcher;
import cito.security.Builder.PrincipalMatcher;
import cito.security.Builder.SecurityAnnotationMatcher;
import cito.internal.stomp.Command;

/**
 * Unit tests for {@link Builder}.
 *
 * @author Daniel Siviter
 * @since v1.0 [14 Apr 2017]
 */
@ExtendWith(MockitoExtension.class)
public class BuilderTest {
	@Mock
	private SecurityRegistry registry;
	@Mock
	private SecurityContext context;

	private Builder builder;

	@BeforeEach
	public void before() {
		this.builder = new Builder(this.registry);
	}

	@Test
	public void matches_frameMatcher(@Mock MessageMatcher frameMatcher) {
		this.builder.matches(frameMatcher);

		verifyNoMoreInteractions(frameMatcher);
	}

	@Test
	public void matches_commands() {
		this.builder.matches(Command.COMMIT, Command.MESSAGE);
	}

	@Test
	public void matches_destinations() {
		this.builder.matches("/here", "/there");
	}

	@Test
	public void matches_securityMatcher(@Mock SecurityMatcher securityMatcher) {
		this.builder.matches(securityMatcher);

		verifyNoMoreInteractions(securityMatcher);
	}

	@Test
	public void nullDestination() {
		this.builder.nullDestination();

		assertTrue(getSecurityMatchers().isEmpty());
		assertEquals(1, getFrameMatchers().size());
		assertEquals(NullDestinationMatcher.class, getFrameMatchers().get(0).getClass());
	}

	@Test
	public void roles() {
		this.builder.roles("this", "that");

		assertTrue(getFrameMatchers().isEmpty());
		assertEquals(1, getSecurityMatchers().size());
		SecurityAnnotationMatcher matcher = (SecurityAnnotationMatcher) getSecurityMatchers().get(0);
		assertTrue(RolesAllowed.class.isAssignableFrom(ReflectionUtil.get(matcher, "annotation").getClass()));
	}

	@Test
	public void permitAll() {
		this.builder.permitAll();

		assertTrue(getFrameMatchers().isEmpty());
		assertEquals(1, getSecurityMatchers().size());
		final SecurityAnnotationMatcher matcher = (SecurityAnnotationMatcher) getSecurityMatchers().get(0);
		assertTrue(PermitAll.class.isAssignableFrom(ReflectionUtil.get(matcher, "annotation").getClass()));
	}

	@Test
	public void denyAll() {
		this.builder.denyAll();

		assertTrue(getFrameMatchers().isEmpty());
		assertEquals(1, getSecurityMatchers().size());
		SecurityAnnotationMatcher matcher = (SecurityAnnotationMatcher) getSecurityMatchers().get(0);
		assertTrue(DenyAll.class.isAssignableFrom(ReflectionUtil.get(matcher, "annotation").getClass()));
	}

	@Test
	public void principleExists() {
		this.builder.principleExists();

		assertTrue(getFrameMatchers().isEmpty());
		assertEquals(1, getSecurityMatchers().size());
		assertEquals(PrincipalMatcher.class, getSecurityMatchers().get(0).getClass());
	}

	@Test
	public void build(@Mock MessageMatcher frameMatcher, @Mock SecurityMatcher securityMatcher) {
		getFrameMatchers().add(frameMatcher);
		getSecurityMatchers().add(securityMatcher);

		this.builder.build();

		verify(this.registry).register(any());
		verifyNoMoreInteractions(frameMatcher, securityMatcher);
	}

	@Test
	public void principalMatcher(@Mock Principal principal) {
		when(this.context.getUserPrincipal()).thenReturn(principal);
		final PrincipalMatcher matcher = new PrincipalMatcher();

		assertTrue(matcher.permitted(this.context));

		verify(this.context).getUserPrincipal();
		verifyNoMoreInteractions(principal);
	}

	@Test
	public void principalMatcher_false() {
		final PrincipalMatcher matcher = new PrincipalMatcher();

		assertFalse(matcher.permitted(this.context));

		verify(this.context).getUserPrincipal();
	}

	@Test
	public void securityAnnotationMatcher_rolesAllowed() {
		when(this.context.isUserInRole("this")).thenReturn(false);
		when(this.context.isUserInRole("that")).thenReturn(true);
		final SecurityAnnotationMatcher matcher = new SecurityAnnotationMatcher(new RolesAllowedLiteral("this", "that"));

		assertTrue(matcher.permitted(this.context));

		verify(this.context).isUserInRole("this");
		verify(this.context).isUserInRole("that");
	}

	@Test
	public void securityAnnotationMatcher_rolesAllowed_false() {
		final SecurityAnnotationMatcher matcher = new SecurityAnnotationMatcher(new RolesAllowedLiteral("this", "that"));

		assertFalse(matcher.permitted(this.context));

		verify(this.context).isUserInRole("this");
		verify(this.context).isUserInRole("that");
	}

	@Test
	public void securityAnnotationMatcher_permitAll() {
		final SecurityAnnotationMatcher matcher = new SecurityAnnotationMatcher(PermitAllLiteral.permitAll());

		assertTrue(matcher.permitted(this.context));
	}

	@Test
	public void securityAnnotationMatcher_denyAll() {
		final SecurityAnnotationMatcher matcher = new SecurityAnnotationMatcher(DenyAllLiteral.denyAll());

		assertFalse(matcher.permitted(this.context));
	}

	@Test
	public void nullDestinationMatcher() {
		final NullDestinationMatcher matcher = new NullDestinationMatcher();

		assertTrue(matcher.matches((String) null));
	}

	@Test
	public void nullDestinationMatcher_false() {
		final NullDestinationMatcher matcher = new NullDestinationMatcher();

		assertFalse(matcher.matches("/"));
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.registry, this.context);
	}

	/**
	 *
	 * @return
	 */
	private List<MessageMatcher> getFrameMatchers() {
		return ReflectionUtil.get(this.builder, "frameMatchers");
	}

	/**
	 *
	 * @return
	 */
	private List<SecurityMatcher> getSecurityMatchers() {
		return ReflectionUtil.get(this.builder, "securityMatchers");
	}
}
