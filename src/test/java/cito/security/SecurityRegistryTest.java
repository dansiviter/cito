/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.security;

import static cito.ReflectionUtil.getAnnotation;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import cito.ReflectionUtil;
import cito.event.Message;
import cito.security.Builder.Limitation;

/**
 * Unit tests for {@link Builder}.
 *
 * @author Daniel Siviter
 * @since v1.0 [14 Apr 2017]
 */
@ExtendWith(MockitoExtension.class)
public class SecurityRegistryTest {
	@Mock
	private Instance<SecurityCustomiser> customisers;
	@Mock
	private SecurityCustomiser customiser;
	@InjectMocks
	private SecurityRegistry registry;

	@Test
	public void scope() {
		assertNotNull(getAnnotation(SecurityRegistry.class, ApplicationScoped.class));
	}

	@Test
	public void init() {
		this.registry.init();

		verify(this.customisers).forEach(any());
	}

	@Test
	public void register(@Mock Limitation limitation) {
		this.registry.register(limitation);

		assertEquals(1, getLimitations().size());
		assertTrue(getLimitations().contains(limitation));

		verifyNoMoreInteractions(limitation);
	}

	@Test
	public void getMatching(@Mock Message msg, @Mock Limitation limitation, @Mock Limitation limitation0) {
		getLimitations().add(limitation);
		when(limitation.test(msg)).thenReturn(true);
		getLimitations().add(limitation0);

		this.registry.getMatching(msg);

		verify(limitation).test(msg);
		verify(limitation0).test(msg);
		verifyNoMoreInteractions(limitation, limitation0, msg);
	}

	@Test
	public void isPermitted(@Mock Message msg, @Mock SecurityContext context, @Mock Limitation limitation) {
		getLimitations().add(limitation);
		when(limitation.test(msg)).thenReturn(true);
		when(limitation.permitted(context)).thenReturn(true);

		assertTrue(this.registry.isPermitted(msg, context));

		verify(limitation).test(msg);
		verify(limitation).permitted(context);
		verifyNoMoreInteractions(msg, context, limitation);
	}

	@Test
	public void isPermitted_false(@Mock Message msg, @Mock SecurityContext context, @Mock Limitation limitation) {
		getLimitations().add(limitation);
		when(limitation.test(msg)).thenReturn(true);

		assertFalse(this.registry.isPermitted(msg, context));

		verify(limitation).test(msg);
		verify(limitation).permitted(context);
		verifyNoMoreInteractions(msg, context, limitation);
	}

	@Test
	public void builder() {
		final Builder builder = this.registry.builder();
		final Builder builder0 = this.registry.builder();

		assertThat(builder, not(sameInstance(builder0)));
	}

	@Test
	public void configure() {
		this.registry.customise(customiser);

		verify(customiser).customise(this.registry);
	}

	@AfterEach
	public void after() {
		verifyNoMoreInteractions(this.customisers, this.customiser);
	}

	/**
	 *
	 * @return
	 */
	private Set<Limitation> getLimitations() {
		return ReflectionUtil.get(this.registry, "limitations");
	}
}
