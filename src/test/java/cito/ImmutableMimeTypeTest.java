/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.activation.MimeType;

import org.junit.jupiter.api.Test;

/**
 * Unit tests for {@link ImmutableMimeType}.
 *
 * @author Daniel Siviter
 * @since v1.0 [29 Dec 2019]
 */
public class ImmutableMimeTypeTest {
	@Test
	public void hashcode() {
		MimeType mimeType0 = ImmutableMimeType.mimeType("application/json");
		MimeType mimeType1 = ImmutableMimeType.mimeType("application/json");
		assertEquals(mimeType0.hashCode(), mimeType1.hashCode());

		MimeType mimeType2 = ImmutableMimeType.mimeType("application/json;charset=iso-8859-x");
		assertEquals(mimeType0.hashCode(), mimeType2.hashCode());

		MimeType mimeType3 = ImmutableMimeType.mimeType("application/text");
		assertNotEquals(mimeType0.hashCode(), mimeType3.hashCode());

		MimeType mimeType4 = ImmutableMimeType.mimeType("text/json");
		assertNotEquals(mimeType0.hashCode(), mimeType4.hashCode());
	}

	@Test
	public void equals() {
		MimeType mimeType0 = ImmutableMimeType.mimeType("application/json");
		MimeType mimeType1 = ImmutableMimeType.mimeType("application/json");
		assertTrue(mimeType0.equals(mimeType1));

		MimeType mimeType2 = ImmutableMimeType.mimeType("application/json;charset=iso-8859-x");
		assertTrue(mimeType0.equals(mimeType2));

		MimeType mimeType3 = ImmutableMimeType.mimeType("application/text");
		assertFalse(mimeType0.equals(mimeType3));

		MimeType mimeType4 = ImmutableMimeType.mimeType("text/json");
		assertFalse(mimeType0.equals(mimeType4));
	}

	@Test
	public void setPrimaryType() {
		assertThrows(UnsupportedOperationException.class, () -> {
			ImmutableMimeType.mimeType("application/json").setPrimaryType("foo");
		});
	}

	@Test
	public void setSubType() {
		assertThrows(UnsupportedOperationException.class, () -> {
			ImmutableMimeType.mimeType("application/json").setSubType("foo");
		});
	}

	@Test
	public void setParameter() {
		assertThrows(UnsupportedOperationException.class, () -> {
			ImmutableMimeType.mimeType("application/json").setParameter("foo", "bar");
		});
	}

	@Test
	public void getParameters_remove() {
		assertThrows(UnsupportedOperationException.class, () -> {
			ImmutableMimeType.mimeType("application/json").getParameters().remove("foo");
		});
	}


	@Test
	public void getParameters_set() {
		assertThrows(UnsupportedOperationException.class, () -> {
			ImmutableMimeType.mimeType("application/json").getParameters().set("foo", "bar");
		});
	}
}
