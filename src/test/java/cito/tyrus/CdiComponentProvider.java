/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.tyrus;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.CDI;

import org.glassfish.tyrus.core.ComponentProvider;

/**
 * @author Daniel Siviter
 * @since v1.0 [13 Nov 2019]
 */
public class CdiComponentProvider extends ComponentProvider {
	private final CDI<Object> cdi = CDI.current();

	private final Map<Object, Instance<?>> cdiBeanToContext = new ConcurrentHashMap<>();

	@Override
	public boolean isApplicable(Class<?> c) {
		return this.cdi.select(c).isResolvable();
	}

	@Override
	public <T> Object create(Class<T> c) {
		synchronized (cdi) {
			final Instance<T> instance = cdi.select(c);
			final T value = instance.get();
			cdiBeanToContext.put(value, instance);
			return value;
		}
	}

	@Override
	public boolean destroy(Object o) {
		if (cdiBeanToContext.containsKey(o)) {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			final Instance<Object> instance = (Instance) cdiBeanToContext.remove(o);
			instance.destroy(o);
			return true;
		}
		return false;
	}
}
