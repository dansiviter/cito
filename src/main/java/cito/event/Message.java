/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.event;

import static cito.internal.stomp.Header.Standard.CONTENT_LENGTH;
import static cito.internal.stomp.Header.Standard.CONTENT_TYPE;
import static java.lang.Integer.parseInt;
import static java.util.Collections.singletonMap;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;

import javax.activation.MimeType;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.validation.constraints.NotNull;

import cito.ImmutableMimeType;
import cito.internal.stomp.Header;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [19 Jul 2016]
 */
@Immutable
public abstract class Message {
	private final Optional<String> sessionId;

	public Message(Optional<String> sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the originating session identifier. If this is a internally generated
	 *         message (i.e. application code) then this will be {@code null}.
	 */
	public Optional<String> sessionId() {
		return sessionId;
	}

	/**
	 *
	 * @return the type of message.
	 */
	public abstract Optional<Type> type();

	/**
	 * @return the headers. These are unmodifiable.
	 */
	public abstract Map<Header, List<String>> headers();

	/**
	 * Checks if the header is set on the frame.
	 *
	 * @param header the header to check.
	 * @return {@code true} if the frame contains the header.
	 */
	public boolean contains(@NotNull Header header) {
		return get(header).isPresent();
	}

	/**
	 *
	 * @param header the header type wanted.
	 * @return the header values.
	 */
	public Optional<List<String>> get(@NotNull Header header) {
		final List<String> values = headers().get(header);
		return Optional.ofNullable(values);
	}

	/**
	 *
	 * @param header the header type wanted.
	 * @return the header values.
	 */
	public Optional<List<String>> getHeader(@NotNull String header) {
		return get(Header.valueOf(header));
	}

	/**
	 *
	 * @param header the header type wanted.
	 * @return the first header value.
	 */
	public Optional<String> getFirst(@Nonnull Header header) {
		final Optional<List<String>> values = get(header);
		if (values.isPresent() && !values.get().isEmpty()) {
			return Optional.of(values.get().get(0));
		}
		return Optional.empty();
	}

	/**
	 *
	 * @param header the header type wanted.
	 * @return the first header value.
	 */
	public Optional<String> getFirstHeader(@Nonnull String header) {
		return getFirst(Header.valueOf(header));
	}

	/**
	 *
	 * @param header the header type wanted.
	 * @return the first header value.
	 */
	public OptionalInt getFirstInt(@Nonnull Header header) {
		final Optional<String> value = getFirst(header);
		return value.isPresent() ? OptionalInt.of(parseInt(value.get())) : OptionalInt.empty();
	}

	/**
	 * @return the identity, usually the websocket session ID.
	 */
	public abstract Optional<String> id();

	/**
	 * @return the body of the message.
	 */
	public abstract Optional<ByteBuffer> body();

	/**
	 * @return the {@code content-type} header.
	 */
	public Optional<MimeType> contentType() {
		return getFirst(CONTENT_TYPE).map(ImmutableMimeType::mimeType);
	}

	/**
	 * @return the {@code content-length} header.
	 * @see Header.Standard#CONTENT_LENGTH
	 */
	public OptionalInt contentLength() {
		return getFirstInt(CONTENT_LENGTH);
	}

	/**
	 * @return the message destination.
	 */
	public abstract Optional<String> destination();

	@Override
	public String toString() {
		return super.toString() + singletonMap("sessionId", this.sessionId);
	}

	/**
	 *
	 */
	public enum Type {
		CONNECTED, SEND, SUBSCRIBE, UNSUBSCRIBE, DISCONNECT, HEARTBEAT
	}
}
