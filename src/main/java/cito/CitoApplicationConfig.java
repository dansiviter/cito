/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito;

import static cito.internal.stomp.StompVersion.toWsSubprotocol;
import static java.lang.System.getProperty;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;

import java.util.Set;

import javax.websocket.Endpoint;
import javax.websocket.server.ServerApplicationConfig;
import javax.websocket.server.ServerEndpointConfig;
import javax.websocket.server.ServerEndpointConfig.Builder;

import cito.internal.CitoEndpoint;
import cito.internal.WebSocketConfigurator;
import cito.internal.stomp.StompVersion;

/**
 * Entrypoint into Cito. The only configurable parameter is the path via {@code cito.path} which defaults to
 * {@code /cito}.
 *
 * @author Daniel Siviter
 * @since v1.0 [23 Nov 2019]
 */
public class CitoApplicationConfig implements ServerApplicationConfig {
	@Override
	public Set<ServerEndpointConfig> getEndpointConfigs(Set<Class<? extends Endpoint>> endpointClasses) {
		final String path = getProperty("cito.path", "/cito");
		return singleton(config(path));
	}

	@Override
	public Set<Class<?>> getAnnotatedEndpointClasses(Set<Class<?>> scanned) {
		return emptySet();
	}

	// --- Static Methods ---

	/**
	 * Convenience method to create config for Cito.
	 *
	 * @param path the path for the Cito endpoint.
	 * @param versions the supported stomp versions.
	 * @return the config.
	 */
	public static ServerEndpointConfig config(String path, StompVersion... versions) {
		return Builder.create(CitoEndpoint.class, path)
				.subprotocols(toWsSubprotocol(versions.length > 0 ? versions : StompVersion.values()))
				.configurator(new WebSocketConfigurator())
				.build();
	}
}
