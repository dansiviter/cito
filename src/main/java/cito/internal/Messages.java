/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import java.io.IOException;
import java.lang.reflect.Type;

import javax.activation.MimeType;

import org.jboss.logging.annotations.Message;
import org.jboss.logging.annotations.MessageBundle;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [13 Oct 2019]
 *
 */
@MessageBundle(projectCode = "CW")
public interface Messages {
	Messages MESSAGES = org.jboss.logging.Messages.getBundle(Messages.class);

	@Message("Session already set! [expected=%s,current=%s]")
	IllegalStateException sessionAlreadySet(String expected, String current);

	@Message("Already connected!")
	IllegalStateException alreadyConnected();

	@Message("No compatible reader found! Unable to read. [type=%s,mimeType=%s]")
	IOException noReaderFound(Type type, MimeType mimeType);

	@Message("No compatible writer found! Unable to write. [type=%s,mimeType=%s]")
	IOException noWriterFound(Type type, MimeType mimeType);

	@Message("Value already in context! [%s]")
	IllegalStateException contextValueAlreadySet(String key);

	/**
	 * @return messages instance.
	 */
	public static Messages messages() {
		return MESSAGES;
	}
}
