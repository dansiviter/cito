/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static java.util.Objects.requireNonNull;

import java.util.Map;
import java.util.Objects;
import java.util.WeakHashMap;
import java.util.regex.Matcher;

import javax.annotation.Nonnull;

import cito.annotation.PathParam;

/**
 * Utility for parsing paths and performing parameters expansion.
 *
 * @author Daniel Siviter
 * @since v1.0 [17 Jan 2017]
 * @see Glob
 * @see PathParam
 */
public class PathParser {
	private static final Map<String, PathParser> PARSERS = new WeakHashMap<>();

	private final String pattern;
	private final Glob glob;

	/**
	 * Create a parser.
	 *
	 * @param pattern the pattern to parse.
	 */
	public PathParser(@Nonnull String pattern) {
		this.glob = Glob.from(requireNonNull(this.pattern = pattern));
	}

	/**
	 *
	 * @param path the path to check.
	 * @return the parse result.
	 */
	public Result parse(@Nonnull CharSequence path) {
		final Matcher matcher = this.glob.compiled().matcher(path);
		return new Result(matcher);
	}

	@Override
	public String toString() {
		return super.toString() + "[" + this.pattern + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.pattern);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null ||getClass() != obj.getClass())
			return false;
		PathParser other = (PathParser) obj;
		return Objects.equals(this.pattern, other.pattern);
	}


	// --- Static Methods ---

	/**
	 * Returns a {@link PathParser} instance from a weak cache of instances. If one doesn't exist then it will be
	 * created.
	 *
	 * @param path the path parser pattern.
	 * @return the parser instance.
	 */
	public static PathParser pathParser(@Nonnull String path) {
		return PARSERS.computeIfAbsent(path, k -> new PathParser(path));
	}

	/**
	 * Performs creation and parsing in one call.
	 *
	 * @param pathPattern the path parser pattern.
	 * @param path the path to parse.
	 * @return the result.
	 */
	public static Result parse(@Nonnull String pathPattern, @Nonnull String path) {
		return pathParser(pathPattern).parse(path);
	}


	// --- Inner Classes ---

	/**
	 * The result of the parser. This effectively wraps the regex parser and provides access to the underlying parameter
	 * values.
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [17 Jan 2017]
	 */
	public static class Result {
		private final Matcher matcher;

		private Result(@Nonnull Matcher matcher) {
			this.matcher = requireNonNull(matcher);
		}

		/**
		 * @return {@code true} if it was a success.
		 */
		public boolean isSuccess() {
			return matcher.matches();
		}

		/**
		 * @param name the parameter name.
		 * @return the value or {@code null} if not found.
		 */
		public String get(@Nonnull String name) {
			return this.matcher.group(name);
		}
	}
}
