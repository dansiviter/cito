/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.annotation.Nonnull;

/**
 * @author Daniel Siviter
 * @since v1.0 [23 Nov 2019]
 */
public enum StompVersion {
	V1_2("1.2", StompVersion.V1_2_SUBPROTOCOL),
	V1_1("1.1", StompVersion.V1_1_SUBPROTOCOL),
	V1_O("1.0", StompVersion.V1_0_SUBPROTOCOL);

	public static final String V1_0_SUBPROTOCOL = "v10.stomp";
	public static final String V1_1_SUBPROTOCOL = "v11.stomp";
	public static final String V1_2_SUBPROTOCOL = "v12.stomp";

	private final String version;
	private final String wsSubprotocol;

	StompVersion(String version, String wsSubprotocol) {
		this.version = version;
		this.wsSubprotocol = wsSubprotocol;
	}

	/**
	 * @return the version
	 */
	public String version() {
		return version;
	}

	/**
	 * @return the websocket subprotocol version
	 */
	public String wsSubprotocol() {
		return this.wsSubprotocol;
	}


	// --- Static Methods ---

	/**
	 *
	 * @param in the string representation.
	 * @return the found version.
	 * @throws IllegalArgumentException if no enum for this type is found.
	 */
	public static StompVersion stompVersion(@Nonnull String in) {
		return fromString(in);
	}

	/**
	 *
	 * @param in the string representation.
	 * @return the found version.
	 * @throws IllegalArgumentException if no enum for this type is found.
	 */
	public static StompVersion fromString(@Nonnull String in) {
		for (StompVersion version : values()) {
			if (version.version.equals(in) || version.wsSubprotocol.equals(in)) {
				return version;
			}
		}
		return valueOf(in);
	}

	/**
	 *
	 * @param versions the versions to convert to websocket subprotocols.
	 * @return the string list.
	 */
	public static List<String> toWsSubprotocol(StompVersion... versions) {
		return stream(versions).map(StompVersion::wsSubprotocol).collect(toList());
	}
}
