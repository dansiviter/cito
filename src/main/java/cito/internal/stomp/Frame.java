/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static cito.internal.stomp.Header.Standard.ACCEPT_VERSION;
import static cito.internal.stomp.Header.Standard.CONTENT_LENGTH;
import static cito.internal.stomp.Header.Standard.CONTENT_TYPE;
import static cito.internal.stomp.Header.Standard.DESTINATION;
import static cito.internal.stomp.Header.Standard.HOST;
import static cito.internal.stomp.Header.Standard.ID;
import static cito.internal.stomp.Header.Standard.MESSAGE_ID;
import static cito.internal.stomp.Header.Standard.RECEIPT;
import static cito.internal.stomp.Header.Standard.RECEIPT_ID;
import static cito.internal.stomp.Header.Standard.SERVER;
import static cito.internal.stomp.Header.Standard.SESSION;
import static cito.internal.stomp.Header.Standard.SUBSCRIPTION;
import static cito.internal.stomp.Header.Standard.TRANSACTION;
import static cito.internal.stomp.Header.Standard.VERSION;
import static java.lang.Integer.parseInt;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.stream;
import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicLong;

import javax.activation.MimeType;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import javax.annotation.concurrent.ThreadSafe;
import javax.validation.constraints.NotNull;

import cito.ImmutableMimeType;
import cito.internal.stomp.Header.Standard;

/**
 * Defines a STOMP frame
 *
 * @author Daniel Siviter
 * @since v1.0 [12 Jul 2016]
 */
@Immutable @ThreadSafe
public class Frame {
	private static final AtomicLong MESSAGE_ID_COUNTER = new AtomicLong();
	public static final Frame HEART_BEAT = new Frame(Command.HEARTBEAT, new HashMap<>(0));

	private final Command command;
	private final Map<Header, List<String>> headers;
	private final Optional<ByteBuffer> body;

	/**
	 * @param command the STOMP command.
	 * @param headers the headers.
	 */
	Frame(@Nonnull Command command, @Nonnull Map<Header, List<String>> headers) {
		this(command, headers, Optional.empty());
	}

	/**
	 * @param command the STOMP command.
	 * @param headers the headers.
	 * @param body the frame body.
	 */
	Frame(@Nonnull Command command, @Nonnull Map<Header, List<String>> headers, @Nonnull Optional<ByteBuffer> body) {
		this.command = requireNonNull(command);
		final Map<Header, List<String>> tmpHeaders = new LinkedHashMap<>(headers);
		tmpHeaders.entrySet().forEach(e -> e.setValue(unmodifiableList(e.getValue())));
		this.headers = unmodifiableMap(tmpHeaders);
		this.body = body.map(ByteBuffer::asReadOnlyBuffer);
	}

	/**
	 * @return {@code true} if this is a heart beat.
	 */
	public boolean isHeartBeat() {
		return this.command == Command.HEARTBEAT;
	}

	/**
	 * @return the STOMP command.
	 */
	public Command command() {
		return command;
	}

	/**
	 * @return the headers. These are unmodifiable.
	 */
	public Map<Header, List<String>> headers() {
		return headers;
	}

	/**
	 * @return the frame body.
	 */
	public Optional<ByteBuffer> body() {
		return body;
	}

	/**
	 * Checks if the header is set on the frame.
	 *
	 * @param header the header to check.
	 * @return {@code true} if the frame contains the header.
	 */
	public boolean contains(@NotNull Header header) {
		return !isNull(get(header));
	}

	/**
	 *
	 * @param header the header type wanted.
	 * @return the header values.
	 */
	public Optional<List<String>> get(@NotNull Header header) {
		final List<String> values = headers().get(header);
		return Optional.ofNullable(values);
	}

	/**
	 *
	 * @param header the header type wanted.
	 * @return the header values.
	 */
	public Optional<List<String>> getHeader(@NotNull String header) {
		return get(Header.valueOf(header));
	}

	/**
	 *
	 * @param header the header type wanted.
	 * @return the first header value.
	 */
	public Optional<String> getFirst(@Nonnull Header header) {
		final Optional<List<String>> values = get(header);
		if (values.isPresent() && !values.get().isEmpty()) {
			return Optional.of(values.get().get(0));
		}
		return Optional.empty();
	}

	/**
	 *
	 * @param header the header type wanted.
	 * @return the first header value.
	 */
	public Optional<String> getFirstHeader(@Nonnull String header) {
		return getFirst(Header.valueOf(header));
	}

	/**
	 *
	 * @param header the header type wanted.
	 * @return the first header value.
	 */
	public OptionalInt getFirstInt(@Nonnull Header header) {
		final Optional<String> value = getFirst(header);
		return value.isPresent() ? OptionalInt.of(parseInt(value.get().toString())) : OptionalInt.empty();
	}

	/**
	 * @return the {@code destination}.
	 * @see Header.Standard#DESTINATION
	 */
	public Optional<String> destination() {
		return getFirst(DESTINATION);
	}

	/**
	 * @return the {@code content-length}.
	 * @see Header.Standard#CONTENT_LENGTH
	 */
	public OptionalInt contentLength() {
		return getFirstInt(CONTENT_LENGTH);
	}

	/**
	 * @return the {@code content-type}.
	 * @see Header.Standard#CONTENT_TYPE
	 */
	public Optional<MimeType> contentType() {
		return getFirst(CONTENT_TYPE).map(ImmutableMimeType::mimeType);
	}

	/**
	 * @return the {@code receipt}.
	 * @see Header.Standard#RECEIPT
	 */
	public OptionalInt receipt() {
		return getFirstInt(RECEIPT);
	}

	/**
	 * @return the {@code receipt-id}.
	 * @see Header.Standard#RECEIPT_ID
	 */
	public OptionalInt receiptId() {
		return getFirstInt(RECEIPT_ID);
	}

	/**
	 * @return the {@code id}. If this is a {@link Command#MESSAGE} then this returns the {@code subscription} value.
	 * @see Header.Standard#ID
	 */
	public Optional<String> id() {
		if (this.command == Command.MESSAGE) { // why is MESSAGE so special?!
			return getFirst(SUBSCRIPTION);
		}
		return getFirst(ID);
	}

	/**
	 * @return the {@code heart-beat}.
	 * @see Header.Standard#HEART_BEAT
	 */
	public Optional<HeartBeat> heartBeat() {
		return getFirst(Standard.HEART_BEAT).map(HeartBeat::new);
	}

	/**
	 * @return the {@code transaction}.
	 * @see Header.Standard#TRANSACTION
	 */
	public Optional<String> transaction() {
		return getFirst(TRANSACTION);
	}

	/**
	 * @return the {@code session}.
	 * @see Header.Standard#SESSION
	 */
	public Optional<String> session() {
		return getFirst(SESSION);
	}

	/**
	 * @return the {@code version}.
	 * @see Header.Standard#VERSION
	 */
	public Optional<StompVersion> version() {
		return getFirst(Standard.VERSION).map(StompVersion::stompVersion);
	}

	/**
	 * Only use this for debugging purposes. An especially large frame could lead to {@link BufferOverflowException}.
	 */
	@Override
	public String toString() {
		// 64k is generally much larger than the buffer used by WebSocket implementation, so this should suffice
		return UTF_8.decode(Encoding.from(this, false, 64 * 1024)).toString();
	}


	// --- Static Methods ---

	/**
	 * Creates a new builder for a {@link Command#CONNECT} message.
	 *
	 * @param host the host.
	 * @param acceptVersions the accept version values.
	 * @return the new builder.
	 */
	public static Builder connect(@Nonnull String host, @Nonnull StompVersion... acceptVersions) {
		final List<String> versions = stream(acceptVersions).map(StompVersion::version).collect(toList());
		return builder(Command.CONNECT).header(HOST, host).header(ACCEPT_VERSION, versions);
	}

	/**
	 * Creates a new builder for a {@link Command#DISCONNECT} message.
	 *
	 * @return the new builder.
	 */
	public static Builder disconnect() {
		return builder(Command.DISCONNECT);
	}

	/**
	 * Creates a new builder for a {@link Command#ERROR} message.
	 *
	 * @return the new builder.
	 */
	public static Builder error() {
		return builder(Command.ERROR);
	}

	/**
	 * Creates a new builder for a {@link Command#MESSAGE} message.
	 *
	 * @param destination the destination.
	 * @param subscriptionId the subscription identifier.
	 * @param messageId the message identifier.
	 * @param contentType the content type. This can be {@code null}.
	 * @param body the message body. This will be encoded to bytes using {@link StandardCharsets#UTF_8}.
	 * @return the new builder.
	 */
	public static Builder message(
			@Nonnull String destination,
			@Nonnull String subscriptionId,
			@Nonnull String messageId,
			MimeType contentType,
			@Nonnull String body)
	{
		return builder(Command.MESSAGE)
				.destination(destination)
				.subscription(subscriptionId)
				.messageId(messageId)
				.body(contentType, body);
	}

	/**
	 * Creates a new builder for a {@link Command#MESSAGE} message.
	 *
	 * @param destination the destination.
	 * @param subscriptionId the subscription identifier.
	 * @param messageId the message identifier.
	 * @param contentType the content type. This can be {@code null}.
	 * @param body the message body.
	 * @return the new builder.
	 */
	public static Builder message(
			@Nonnull String destination,
			@Nonnull String subscriptionId,
			@Nonnull String messageId,
			MimeType contentType,
			@Nonnull ByteBuffer body)
	{
		return builder(Command.MESSAGE)
				.destination(destination)
				.subscription(subscriptionId)
				.messageId(messageId)
				.body(contentType, body);
	}

	/**
	 * Creates a new builder for a {@link Command#SEND} message.
	 *
	 * @param destination the destination.
	 * @param contentType the content type. This can be {@code null}.
	 * @param body the message body.
	 * @return the new builder.
	 */
	public static Builder send(@Nonnull String destination, MimeType contentType, @Nonnull ByteBuffer body) {
		return builder(Command.SEND).destination(destination).body(contentType, body);
	}

	/**
	 * Creates a new builder for a {@link Command#SEND} message.
	 *
	 * @param destination the destination.
	 * @param contentType the content type. This can be {@code null}.
	 * @param body the message body. This will be encoded to bytes using {@link StandardCharsets#UTF_8}.
	 * @return the new builder.
	 */
	public static Builder send(@Nonnull String destination, MimeType contentType, @Nonnull String body) {
		return builder(Command.SEND).destination(destination).body(contentType, body);
	}

	/**
	 * Creates a new builder for a {@link Command#CONNECTED} message.
	 *
	 * @param version the version.
	 * @param session the session identifier.
	 * @param server the server name.
	 * @return the new builder.
	 */
	public static Builder connnected(@Nonnull StompVersion version, @Nonnull String session, @Nonnull String server) {
		final Builder builder = builder(Command.CONNECTED).header(VERSION, version.version());
		builder.header(SESSION, requireNonNull(session));
		builder.header(SERVER, requireNonNull(server));
		return builder;
	}

	/**
	 * Creates a new builder for a {@link Command#SUBSCRIBE} message.
	 *
	 * @param id the subscription identifier value.
	 * @param destination the destination.
	 * @return the new builder.
	 */
	public static Builder subscribe(@Nonnull String id, @Nonnull String destination) {
		return builder(Command.SUBSCRIBE).subscription(id).destination(destination);
	}

	/**
	 * Creates a new builder for a {@link Command#RECEIPT} message.
	 *
	 * @param receiptId the receipt identifier value.
	 * @return the new builder.
	 */
	public static Builder receipt(@Nonnull String receiptId) {
		return builder(Command.RECEIPT).header(RECEIPT_ID, receiptId);
	}

	/**
	 * Creates a new builder.
	 *
	 * @param command the command to use.
	 * @return the new builder.
	 */
	public static Builder builder(@Nonnull Command command) {
		return new Builder(command);
	}

	/**
	 * Creates a new builder from the given builder.
	 *
	 * @param builder the builder to copy from.
	 * @return the new builder.
	 */
	public static Builder builder(@Nonnull Builder builder) {
		return new Builder(builder);
	}

	/**
	 * Creates a new builder from the given frame.
	 *
	 * @param frame the frame to copy from.
	 * @return the new builder.
	 */
	public static Builder builder(@Nonnull Frame frame) {
		return new Builder(frame);
	}


	// --- Inner Classes ---

	/**
	 * A {@link Frame} builder.
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [15 Jul 2016]
	 */
	public static class Builder {
		private final Command command;
		private final Map<Header, List<String>> headers = new LinkedHashMap<>();
		private Optional<ByteBuffer> body = Optional.empty();

		/**
		 * Create a {@link Frame} builder from the given {@link Builder}.
		 *
		 * @param builder
		 */
		private Builder(@Nonnull Builder builder) {
			this(builder.command);

			for (Entry<Header, List<String>> e : builder.headers.entrySet()) {
				headers.put(e.getKey(),  new ArrayList<>(e.getValue()));
			}
			this.body = builder.body;
		}

		/**
		 * Create a {@link Frame} builder from the given {@link Frame}.
		 *
		 * @param frame
		 */
		private Builder(@Nonnull Frame frame) {
			this(frame.command());

			for (Entry<Header, List<String>> e : frame.headers().entrySet()) {
				headers.put(e.getKey(),  new ArrayList<>(e.getValue()));
			}
			this.body = frame.body();
		}

		/**
		 * Create a {@link Frame} for the {@link Command}.
		 *
		 * @param command the frame command.
		 */
		private Builder(@Nonnull Command command) {
			this.command = command;
		}

		/**
		 * @param header the header to add.
		 * @param values the values for the header.
		 * @return this builder instance.
		 */
		public Builder header(@Nonnull Header header, @Nonnull String... values) {
			return header(header, Arrays.asList(values));
		}

		/**
		 * @param header the header to add.
		 * @param values the values for the header.
		 * @return this builder instance.
		 */
		public Builder header(@Nonnull Header header, @Nonnull List<String> values) {
			if (values.isEmpty()) {
				throw new IllegalArgumentException("'values' cannot be null or empty!");
			}

			final StringJoiner joiner = new StringJoiner(",");
			for (String v : values) {
				joiner.add(v);
			}

			final List<String> valueList = this.headers.compute(header, (k, v) -> {
				if (v == null) {
					return new ArrayList<>();
				}
				return v;
			});
			valueList.add(joiner.toString());
			return this;
		}

		/**
		 * @param headers the headers to copy.
		 * @return this builder instance
		 */
		public Builder headers(@Nonnull Map<Header, List<String>> headers) {
			headers.forEach(this::header);
			return this;
		}

		/**
		 * @param destination the destination.
		 * @return this builder instance
		 */
		public Builder destination(@Nonnull String destination) {
			if (!this.command.destination()) {
				throw new IllegalArgumentException(this.command + " does not accept a destination!");
			}
			header(DESTINATION, destination);
			return this;
		}

		/**
		 * @param messageId the message identifier.
		 * @return this builder instance
		 */
		public Builder messageId(@Nonnull String messageId) {
			header(MESSAGE_ID, messageId);
			return this;
		}

		/**
		 * @param session the session identifier.
		 * @return this builder instance
		 */
		public Builder session(@Nonnull String session) {
			header(SESSION, session);
			return this;
		}

		/**
		 * @param contentType the body content type.
		 * @param body the frame body. This will be encoded using {@link StandardCharsets#UTF_8}.
		 * @return this builder instance
		 * @throws IllegalArgumentException if the command type does not accept a body or {@code body} is {@code null}.
		 */
		public Builder body(MimeType contentType, @Nonnull String body) {
			return body(contentType, UTF_8.encode(requireNonNull(body)));
		}

		/**
		 * @param contentType the body content type.
		 * @param body the frame body.
		 * @return this builder instance
		 * @throws IllegalArgumentException if the command type does not accept a body or {@code body} is {@code null}.
		 */
		public Builder body(MimeType contentType, @Nonnull ByteBuffer body) {

			try {
				return body(body);
			} finally {
				if (contentType != null) {
					header(CONTENT_TYPE, contentType.toString());
				}
			}
		}

		/**
		 * @param body the frame body.
		 * @return this builder instance
		 * @throws IllegalArgumentException if the command type does not accept a body or {@code body} is {@code null}.
		 */
		public Builder body(ByteBuffer body) {
			if (!this.command.body()) {
				throw new IllegalArgumentException(this.command + " does not accept a body!");
			}
			this.body = Optional.of(body);
			header(CONTENT_LENGTH, Integer.toString(body.limit()));
			return this;
		}

		/**
		 * @param id the subscription identifier.
		 * @return this builder instance
		 */
		public Builder subscription(@Nonnull String id) {
			if (!this.command.subscriptionId()) {
				throw new IllegalArgumentException(this.command + " does not accept a subscription identifier!");
			}

			if (this.command == Command.MESSAGE) { // why is MESSAGE so special?!
				header(SUBSCRIPTION, requireNonNull(id));
			} else {
				header(ID, requireNonNull(id));
			}

			return this;
		}

		/**
		 * @param outgoing the outgoing heart beat interval (in milliseconds).
		 * @param incoming the incomming heart beat interval (in milliseconds).
		 * @return this builder instance
		 */
		public Builder heartBeat(@Nonnull int outgoing, @Nonnull int incoming) {
			return header(Standard.HEART_BEAT, Integer.toString(outgoing), Integer.toString(incoming));
		}

		/**
		 * @param versions the versions.
		 * @return this builder instance
		 */
		public Builder version(String... versions) {
			return header(VERSION, versions);
		}

		/**
		 * @param receiptId the receipt identifier.
		 * @return this builder instance
		 */
		public Builder receipt(int receiptId) {
			return header(RECEIPT, Integer.toString(receiptId));
		}

		/**
		 * @param receiptId the receipt identifier.
		 * @return this builder instance
		 */
		public Builder receiptId(int receiptId) {
			return header(RECEIPT_ID, Integer.toString(receiptId));
		}

		/**
		 * Derives values from other headers if needed.
		 */
		private void derive() {
			if (!this.headers.containsKey(MESSAGE_ID) && this.command == Command.MESSAGE) {
				String messageId = Long.toString(MESSAGE_ID_COUNTER.getAndIncrement());
				if (this.headers.containsKey(SESSION))
					messageId = this.headers.get(SESSION).get(0).concat("-").concat(messageId);
				messageId(messageId);
			}
		}

		/**
		 * Verifies the minimum headers are present.
		 */
		private void validate() {
			if (this.body.isPresent() && !this.command.body()) {
				throw new IllegalStateException("Body not expected! [" + this.command + "]");
			}
			if (this.headers.containsKey(DESTINATION) && !this.command.destination()) {
				throw new IllegalStateException("Destination not expected! [" + this.command + "]");
			}
			switch (this.command) {
			case ACK:
			case NACK:
				assertExists(ID);
				break;
			case BEGIN:
			case COMMIT:
			case ABORT:
				assertExists(TRANSACTION);
				break;
			case CONNECT:
			case STOMP:
				assertExists(ACCEPT_VERSION);
				assertExists(HOST);
				break;
			case CONNECTED:
				assertExists(VERSION);
				break;
			case DISCONNECT:
			case ERROR:
			case HEARTBEAT:
				break;
			case MESSAGE:
				assertExists(DESTINATION);
				assertExists(MESSAGE_ID);
				assertExists(SUBSCRIPTION);
				break;
			case RECEIPT:
				assertExists(RECEIPT_ID);
				break;
			case SEND:
				assertExists(DESTINATION);
				break;
			case SUBSCRIBE:
				assertExists(DESTINATION);
				assertExists(ID);
				break;
			case UNSUBSCRIBE:
				assertExists(ID);
				break;
			default:
				return;
			}
		}

		/**
		 *
		 * @param header
		 */
		private void assertExists(Header header) {
			if (!this.headers.containsKey(header)) {
				throw new AssertionError(String.format(
						"Required header '%s' not set on '%s' frame!",
						header.value(), command));
			}
		}

		/**
		 * @return a newly created {@link Frame}.
		 */
		public Frame build() {
			derive();
			validate();

			final Map<Header, List<String>> headers = new LinkedHashMap<>();
			for (Entry<Header, List<String>> e : this.headers.entrySet()) {
				headers.put(e.getKey(),  new ArrayList<>(e.getValue()));
			}
			return new Frame(this.command, headers, this.body);
		}
	}

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [25 Jul 2016]
	 */
	public static class HeartBeat {
		public final long x, y;

		private HeartBeat(String heartBeat) {
			if (heartBeat == null) {
				this.x = 0;
				this.y = 0;
				return;
			}
			final String[] tokens = heartBeat.split(",");
			if (tokens.length != 2)
				throw new IllegalStateException("Invalid number of heart beat elements!");
			this.x = Long.parseLong(tokens[0]);
			this.y = Long.parseLong(tokens[1]);
		}
	}
}
