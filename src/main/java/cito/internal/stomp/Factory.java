/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static cito.internal.stomp.Header.Standard.CONTENT_LENGTH;
import static cito.internal.stomp.Header.Standard.CONTENT_TYPE;
import static cito.internal.stomp.Header.Standard.CORRELATION_ID;
import static cito.internal.stomp.Header.Standard.DESTINATION;
import static cito.internal.stomp.Header.Standard.EXPIRATION_TIME;
import static cito.internal.stomp.Header.Standard.MESSAGE_ID;
import static cito.internal.stomp.Header.Standard.PRORITY;
import static cito.internal.stomp.Header.Standard.REDELIVERED;
import static cito.internal.stomp.Header.Standard.REPLY_TO;
import static cito.internal.stomp.Header.Standard.SESSION;
import static cito.internal.stomp.Header.Standard.TIMESTAMP;
import static cito.internal.stomp.Header.Standard.TYPE;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Nonnull;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;

import cito.DestinationType;
import cito.internal.AbstractConnection;
import cito.internal.MessageBuilderFactory;
import cito.internal.MessageBuilderFactory.MessageBuilder;

/**
 * Factory for creating both JMS and STOMP artifacts.
 *
 * @author Daniel Siviter
 * @since v1.0 [29 Oct 2016]
 */
@ApplicationScoped
public class Factory {
	private static final Set<Header> IGNORE_HEADERS;

	static {
		final Set<Header> ignore = new HashSet<>();
		ignore.add(DESTINATION);
		ignore.add(CONTENT_LENGTH);
		ignore.add(CONTENT_TYPE);
		IGNORE_HEADERS = Collections.unmodifiableSet(ignore);
	}

	@Inject
	private MessageBuilderFactory messageBuilderFactory;

	/**
	 *
	 * @param conn the connection.
	 * @param sessionMode the session mode.
	 * @return the new session instances.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	public cito.internal.stomp.Session toSession(@Nonnull AbstractConnection conn, int sessionMode)
			throws JMSException
	{
		return new cito.internal.stomp.Session(
				conn,
				conn.getDelegate().createContext(sessionMode),
				this);
	}

	/**
	 * @param ctx the session.
	 * @param destination the destination.
	 * @return the new destination instance.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	public Destination toDestination(@Nonnull JMSContext ctx, @Nonnull String destination) throws JMSException {
		final String subDestination = destination.substring(destination.indexOf('/', 1) + 1, destination.length());
		switch (DestinationType.from(destination)) {
		case QUEUE:
			return ctx.createQueue(subDestination);
		case TOPIC:
			return ctx.createTopic(subDestination);
		default:
			throw new IllegalArgumentException("Unknown destination! ["  + destination + "]");
		}
	}

	/**
	 *
	 * @param d the destination.
	 * @return the destination string.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	public String fromDestination(@Nonnull Destination d) throws JMSException {
		if (d == null)  return null;

		if (d instanceof TemporaryTopic || d instanceof TemporaryQueue) {
			throw new IllegalArgumentException("Temporary destinations are not supported! [" + d + "]");
		}

		if (d instanceof Topic) {
			final Topic topic = (Topic) d;
			return new StringBuilder("topic/").append(topic.getTopicName()).toString();
		}
		final Queue queue = (Queue) d;
		return new StringBuilder("queue/").append(queue.getQueueName()).toString();
	}

	/**
	 * @param ctx the session.
	 * @param msg the frame.
	 * @return the created message.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	public Message toMessage(@Nonnull JMSContext ctx, @Nonnull cito.event.Message msg) throws JMSException {
		// FIXME buffer pool
		final OptionalInt contentLength = msg.contentLength();
		final Message jmsMsg;
		if (contentLength.isPresent()) {
			final BytesMessage bm = ctx.createBytesMessage();
			final ByteBuffer body = msg.body().get();
			final byte[] buf = new byte[1024 * 8];

			while (body.hasRemaining()) {
				final int count = Math.min(buf.length, body.remaining());
				body.get(buf, 0, count);
				bm.writeBytes(buf, 0, count);
			}

			jmsMsg = bm;
		} else {
			jmsMsg = ctx.createTextMessage(UTF_8.decode(msg.body().get()).toString());
		}
		copyHeaders(ctx, msg, jmsMsg);
		return jmsMsg;
	}

	/**
	 * @param message the message.
	 * @param subscriptionId the subscription identifier.
	 * @return the created frame.
	 * @throws IOException thrown if unable to deserialise.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	public cito.event.Message toMessage(@Nonnull Message message, @Nonnull String subscriptionId) throws IOException, JMSException {
		MessageBuilder<?> builder = this.messageBuilderFactory.builder();
		copyHeaders(message, builder);

		// FIXME buffer pool

		final ByteBuffer buf;
		if (message instanceof TextMessage) {
			final TextMessage msg = (TextMessage) message;
			buf = ByteBuffer.wrap(msg.getText().getBytes(UTF_8));
		} else if (message instanceof BytesMessage) {
			final BytesMessage msg = (BytesMessage) message;
			byte[] data = new byte[(int) msg.getBodyLength()];
			msg.readBytes(data);
			buf = ByteBuffer.wrap(data);
		} else {
			throw new IllegalArgumentException("Unexpected type! [" + message.getClass() + "]");
		}
		builder.body(buf);
		final String contentType = message.getStringProperty(CONTENT_TYPE.value());
		if (contentType != null) {
			builder.header(CONTENT_TYPE, contentType);
		}
		return builder.build();
	}

	/**
	 * @param source the source message.
	 * @param target the target frame.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	private void copyHeaders(@Nonnull Message source, @Nonnull MessageBuilder<?> target) throws JMSException {
		target.header(DESTINATION, fromDestination(source.getJMSDestination()));
		target.header(MESSAGE_ID, source.getJMSMessageID());

		if (source.getJMSCorrelationID() != null) {
			target.header(CORRELATION_ID, source.getJMSCorrelationID());
		}
		target.header(EXPIRATION_TIME, Long.toString(source.getJMSExpiration()));

		if (source.getJMSRedelivered()) {
			target.header(REDELIVERED, "true");
		}
		target.header(PRORITY, Integer.toString(source.getJMSPriority()));

		if (source.getJMSReplyTo() != null) {
			target.header(REPLY_TO, fromDestination(source.getJMSReplyTo()));
		}
		target.header(TIMESTAMP, Long.toString(source.getJMSTimestamp()));

		if (source.getJMSType() != null) {
			target.header(TYPE, source.getJMSType());
		}

		@SuppressWarnings("unchecked")
		final Enumeration<String> names = source.getPropertyNames();
		while (names.hasMoreElements()) {
			final String name = names.nextElement();
			target.header(toStompKey(name), source.getStringProperty(name));
		}
	}

	/**
	 * @param msg the source frame.
	 * @param jmsMsg the target message.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	private void copyHeaders(@Nonnull JMSContext ctx, @Nonnull cito.event.Message msg, @Nonnull Message jmsMsg) throws JMSException {
		final Map<Header, List<String>> headers = new TreeMap<Header, List<String>>(Header.HEADER_COMPARATOR);
		headers.putAll(msg.headers());

		jmsMsg.setJMSCorrelationID(removeAndGetFirst(headers, CORRELATION_ID));

		final String type = removeAndGetFirst(headers, TYPE);
		if (type != null) {
			jmsMsg.setJMSType(type);
		}

		String replyTo = removeAndGetFirst(headers, REPLY_TO);
		if (replyTo != null) {
			jmsMsg.setJMSReplyTo(toDestination(ctx, replyTo));
		}

		final String sessionId = removeAndGetFirst(headers, SESSION);
		if (sessionId != null)
			jmsMsg.setStringProperty("session", sessionId);

		// now the general headers
		for (Entry<Header, List<String>> e : msg.headers().entrySet()) {
			if (IGNORE_HEADERS.contains(e.getKey()))
				continue;
			jmsMsg.setStringProperty(toJmsKey(e.getKey()), e.getValue().get(0));
		}
	}


	// --- Static Methods ---

	/**
	 * @param map the source map.
	 * @param header the header to remove.
	 * @return the first value in the list if header is present.
	 */
	private static String removeAndGetFirst(@Nonnull Map<Header, List<String>> map, @Nonnull Header header) {
		final List<String> values = map.remove(header);
		return values != null && !values.isEmpty() ? values.get(0) : null;
	}

	/**
	 * @param header the header to convert.
	 * @return the JMS string representation.
	 */
	public static String toJmsKey(@Nonnull Header header) {
		return header.value().replace("-", "_HYPHEN_").replace(".", "_DOT_");
	}

	/**
	 * @param key the key to convert.
	 * @return the header value.
	 */
	public static Header toStompKey(@Nonnull String key) {
		return Header.valueOf(key.replace("_HYPHEN_", "-").replace("_DOT_", "."));
	}
}
