/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;

import cito.annotation.WebSocketScope;
import cito.internal.Log;

/**
 *
 * TODO: Configurable latency/flutter multiplier
 *
 * @author Daniel Siviter
 * @since v1.0 [22 Jul 2016]
 */
@WebSocketScope
public class HeartBeatMonitor {
	private static final float LATENCY_MULTIPLIER = 1.5f;

	@Inject
	private Log log;
	@Inject @Any
	private StompConnection conn;
	@Inject
	private ScheduledExecutorService scheduler;

	private Long sendDelay, readDelay;
	private ScheduledFuture<?> send, read;

	/**
	 *
	 * @param read the read delay in milliseconds.
	 * @param write the write delay in milliseconds.
	 */
	public void start(long read, long write) {
		this.log.infof("Starting heart beats. [sessionId=%s,read=%s,write=%s,latencyMultiplier=%s]",
				this.conn.getSessionId(), read, write, LATENCY_MULTIPLIER);
		if (read != 0) {
			this.readDelay = (long) (read * LATENCY_MULTIPLIER);
			resetRead();
		}
		if (write != 0) {
			this.sendDelay = write;
			resetSend();
		}
	}

	/**
	 * Reset the read timeout.
	 */
	public void resetRead() {
		// don't do anything if heartbeats are not expected
		if (this.readDelay == null) return;

		if (this.read != null)
			this.read.cancel(false);
		this.read = this.scheduler.schedule(new ReadHeartBeatTimeOut(), this.readDelay, TimeUnit.MILLISECONDS);
	}

	/**
	 * Reset the write timeout.
	 */
	public void resetSend() {
		// don't do anything if heartbeats are not expected
		if (this.sendDelay == null) return;

		if (this.send != null)
			this.send.cancel(false);
		this.send = this.scheduler.schedule(new SendHeartBeat(), this.sendDelay, TimeUnit.MILLISECONDS);
	}

	/**
	 *
	 */
	public void close() {
		if (this.send != null)
			this.send.cancel(false);
		if (this.read != null)
			this.read.cancel(false);
	}


	// --- Inner Classes ---

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [22 Jul 2016]
	 */
	private class SendHeartBeat implements Runnable {
		@Override
		public void run() {
			try {
				log.debugf("Sending heartbeat... [sessionId=%s]", conn.getSessionId());
				conn.sendToClient(Frame.HEART_BEAT);
			} catch (RuntimeException e) {
				log.warn("Unable to send heartbeat!", e);
			}
		}
	}

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [22 Jul 2016]
	 */
	private class ReadHeartBeatTimeOut implements Runnable {
		@Override
		public void run() {
			try {
				log.warnf("No read heartbeat! Closing... [sessionId=%s]", conn.getSessionId());
				conn.close(new CloseReason(CloseCodes.VIOLATED_POLICY, "Heartbeat not recieved in time."));
			} catch (RuntimeException e) {
				log.warn("Unable to close!", e);
			}
		}
	}
}
