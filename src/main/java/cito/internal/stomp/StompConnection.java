/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static cito.MimeTypes.TEXT_PLAIN;
import static cito.internal.stomp.Header.Standard.ACCEPT_VERSION;
import static cito.internal.stomp.Header.Standard.ACK;
import static cito.internal.stomp.Header.Standard.LOGIN;
import static cito.internal.stomp.Header.Standard.PASSCODE;
import static cito.internal.stomp.Header.Standard.RECEIPT;
import static cito.internal.stomp.StompVersion.V1_0_SUBPROTOCOL;
import static cito.internal.stomp.StompVersion.V1_1_SUBPROTOCOL;
import static cito.internal.stomp.StompVersion.V1_2_SUBPROTOCOL;
import static cito.internal.stomp.StompVersion.V1_O;
import static javax.jms.Session.AUTO_ACKNOWLEDGE;
import static javax.jms.Session.CLIENT_ACKNOWLEDGE;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.EndpointConfig;

import cito.DestinationType;
import cito.annotation.WebSocketScope;
import cito.event.Message;
import cito.internal.AbstractConnection;
import cito.internal.annotation.Subprotocol;
import cito.internal.stomp.Frame.HeartBeat;
import cito.security.SecurityContext;
import cito.security.SecurityRegistry;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [21 Jul 2016]
 */
@WebSocketScope
@Subprotocol(V1_0_SUBPROTOCOL)
@Subprotocol(V1_1_SUBPROTOCOL)
@Subprotocol(V1_2_SUBPROTOCOL)
public class StompConnection extends AbstractConnection {
	private static final int HEARTBEAT_READ_DEFAULT = 10_000, HEARTBEAT_WRITE_DEFAULT = 10_000;

	private final Map<String, Subscription> subscriptions = new ConcurrentHashMap<>();
	private final Map<String, Session> txSessions = new ConcurrentHashMap<>();
	private final Map<String, javax.jms.Message> ackMessages = new ConcurrentHashMap<>();

	@Inject
	private Provider<SecurityContext> securityCtx;
	@Inject
	private SecurityRegistry securityRegistry;
	@Inject
	private HeartBeatMonitor heartBeatMonitor;

	private String sessionId;
	private Session session, ackSession;
	private javax.websocket.Session wsSession;

	@Override
	public void open(javax.websocket.Session session, EndpointConfig config) {
		this.wsSession = session;
	}

	@Override
	public String getSessionId() {
		return sessionId;
	}

	void sendToClient(@Nonnull Message msg) {
		sendToClient(((StompMessage) msg).frame());
	}

	void sendToClient(@Nonnull Frame frame) {
		this.heartBeatMonitor.resetSend();
		final Command command = frame.command();

		if (frame.isHeartBeat()) {
			this.log.debugf("Sending message to client. [sessionId=%s,command=HEARTBEAT]", this.sessionId);
		} else {
			this.log.infof("Sending message to client. [sessionId=%s,command=%s]", this.sessionId, command);
		}

		try {
			this.wsSession.getBasicRemote().sendBinary(Encoding.from(frame, false, 64 * 1024));
		} catch (IOException e) {
			this.log.errorf(e, "Unable to send message! [sessionid=%s,command=%s]", sessionId, frame.command());
		}
	}

	/**
	 * @param ack if {@code true} then the client acknowledge session is returned.
	 * @return the session instances.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	private Session getSession(boolean ack) throws JMSException {
		if (ack) {
			if (this.ackSession == null) {
				this.ackSession = this.factory.toSession(this, CLIENT_ACKNOWLEDGE);
			}
			return this.ackSession;
		}
		if (this.session == null) {
			this.session = this.factory.toSession(this, AUTO_ACKNOWLEDGE);
		}
		return this.session;
	}

	/**
	 * @param frame the frame to test.
	 * @return the session that matches the frame.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	private Session getSession(@Nonnull Frame frame) throws JMSException {
		final Optional<String> tx = frame.transaction();
		if (tx.isPresent())
			return this.txSessions.get(tx.get());

		final String ackMode = frame.getFirst(ACK).orElse(null);
		return getSession("client".equalsIgnoreCase(ackMode));
	}

	/**
	 * @param msg the connect message.
	 * @throws JMSException if there is a JMS issue.
	 */
	public void connect(@Nonnull StompMessage msg) throws JMSException {
		if (this.sessionId != null) {
			throw new IllegalStateException("Already connected!");
		}

		if (!msg.sessionId().isPresent()) {
			throw new IllegalArgumentException("Session ID cannot be null!");
		}
		this.sessionId = msg.sessionId().get();

		this.log.infof("Connecting... [%s]", sessionId);

		final StompVersion version = acceptVersion(msg);
		final Frame.Builder connected = Frame.connnected(version, this.sessionId, "localhost");

		final Optional<HeartBeat> heartBeat = msg.frame().heartBeat();
		if (!V1_O.equals(version) && heartBeat.isPresent()) {
			connected.heartBeat(HEARTBEAT_READ_DEFAULT, HEARTBEAT_WRITE_DEFAULT);
		}

		final String login = msg.frame().getFirst(LOGIN).orElseGet(() -> {
			final SecurityContext securityCtx = this.securityCtx.get();
			return securityCtx != null && securityCtx.getUserPrincipal() != null ?
					securityCtx.getUserPrincipal().getName() : null;
		});
		final String passcode = msg.frame().getFirst(PASSCODE).orElse(null);

		createDelegate(login, passcode);

		sendToClient(connected.build());

		if (!V1_O.equals(version) && heartBeat.isPresent()) {
			final long readDelay = Math.max(heartBeat.get().x, HEARTBEAT_READ_DEFAULT);
			final long writeDelay = Math.max(HEARTBEAT_WRITE_DEFAULT, heartBeat.get().y);
			this.heartBeatMonitor.start(readDelay, writeDelay);
		}
	}

	@Override
	public void fromClient(Message msg) {
		final StompMessage stompMsg = (StompMessage) msg;
		final String sessionId = msg.sessionId().get();
		this.log.debugf("Message from client. [sessionId=%s,command=%s]", sessionId, stompMsg.frame().command());

		final boolean permitted = this.securityRegistry.isPermitted(stompMsg, this.securityCtx.get());
		if (!permitted) {
			this.errorHandler.onError(this, sessionId, msg, "Not permitted!", null);
			return;
		}
		on(msg);
	}

	@Override
	public void on(Message msg) {
		final StompMessage stompMsg = (StompMessage) msg;
		if (stompMsg.frame().command() != Command.CONNECT && !getSessionId().equals(msg.sessionId().get())) {
			throw new IllegalStateException(
					"Session identifier mismatch! [expected=" + this.sessionId + ",actual=" + msg.sessionId() + "]");
		}

		this.heartBeatMonitor.resetRead();

		if (stompMsg.frame().isHeartBeat()) {
			this.log.debugf("Heartbeat recieved from client. [%s]", msg.sessionId());
			return;
		}

		this.log.infof("Message received from client. [sessionId=%s,command=%s]",
				msg.sessionId(), stompMsg.frame().command());

		try {
			switch (stompMsg.frame().command()) {
			case CONNECT:
			case STOMP:
				this.log.infof("CONNECT/STOMP recieved. Opening connection to broker. [%s]", msg.sessionId());
				connect(stompMsg);
				return;
			case DISCONNECT:
				this.log.infof("DISCONNECT recieved. Closing connection to broker. [%s]", sessionId);
				disconnect(stompMsg);
				return;
			case SEND:
				if (DestinationType.from(stompMsg.frame().destination().get()) != DestinationType.DIRECT) {
					getSession(stompMsg.frame()).sendToBroker(stompMsg);
				}
				break;
			case ACK: {
				final String id = stompMsg.frame().id().get();
				javax.jms.Message message = this.ackMessages.remove(id);
				if (message == null) {
					throw new IllegalStateException("No such message to ACK! [" + id + "]");
				}
				message.acknowledge();
				break;
			}
			case NACK: {
				final String id = stompMsg.frame().id().get();
				javax.jms.Message message = this.ackMessages.remove(id);
				if (message == null) {
					throw new IllegalStateException("No such message to NACK! [" + id + "]");
				}
				// not sure what to do here, but we're
				this.log.warnf("NACK recieved, but no JMS equivalent! [%s]", id);
				break;
			}
			case BEGIN: {
				final String tx = stompMsg.frame().transaction().get();
				if (this.txSessions.containsKey(tx)) {
					throw new IllegalStateException("Transaction already started! [" + tx + "]");
				}
				final Session txSession = this.factory.toSession(this, JMSContext.SESSION_TRANSACTED);
				this.txSessions.put(stompMsg.frame().transaction().get(), txSession);
				break;
			}
			case COMMIT: {
				final String tx = stompMsg.frame().transaction().get();
				final Session txSession = this.txSessions.remove(tx);
				if (txSession == null) {
					throw new IllegalStateException("Transaction session does not exists! [" + tx + "]");
				}
				txSession.commit();
				break;
			}
			case ABORT: {
				final String tx = stompMsg.frame().transaction().get();
				final Session txSession = this.txSessions.remove(tx);
				if (txSession == null) {
					throw new IllegalStateException("Transaction session does not exists! [" + tx + "]");
				}
				txSession.rollback();
				break;
			}
			case SUBSCRIBE: {
				final String subscriptionId = stompMsg.frame().id().get();
				this.subscriptions.compute(subscriptionId, (k, v) -> {
					try {
						if (v != null) {
							throw new IllegalStateException("Subscription already exists! [" + subscriptionId + "]");
						}
						return new Subscription(getSession(stompMsg.frame()), k, stompMsg.frame());
					} catch (JMSException e) {
						throw new IllegalStateException("Unable to subscribe! [" + subscriptionId + "]", e);
					}
				});
				break;
			}
			case UNSUBSCRIBE: {
				final String subscriptionId = stompMsg.frame().id().get();
				final Subscription subscription = this.subscriptions.remove(subscriptionId);
				if (subscription == null)
					throw new IllegalStateException("Subscription does not exist! [" + subscriptionId + "]");
				subscription.close();
				break;
			}
			case ERROR: {
				sendToClient(stompMsg);
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected frame! [" + stompMsg.frame().command());
			}
			sendReceipt(stompMsg.frame());
		} catch (JMSException e) {
			this.log.errorf("Error handling message! [sessionId=%s,command=%s]", this.sessionId, stompMsg.frame().command(),
					e);
		}
	}

	/**
	 * @param msg the disconnect message.
	 */
	public void disconnect(@Nonnull StompMessage msg) {
		close(new CloseReason(CloseCodes.NORMAL_CLOSURE, "Disconnect"));
		sendReceipt(msg.frame());
	}

	/**
	 * @param frame the source frame to send receipt for.
	 */
	private void sendReceipt(@Nonnull Frame frame) {

		frame.getFirst(RECEIPT).ifPresent(id -> sendToClient(Frame.receipt(id).build()));
	}

	/**
	 *
	 * @param msg the input message to acknowledge.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	public void addAckMessage(@Nonnull javax.jms.Message msg) throws JMSException {
		this.ackMessages.put(msg.getJMSMessageID(), msg);
	}

	@Override
	public void close(CloseReason reason) {
		super.close(reason);
		this.heartBeatMonitor.close();
		try {
			if (this.wsSession.isOpen()) {
				this.wsSession.close(reason);
			}
		} catch (IOException e) {
			this.log.warnf(e, "Unable to close!");
		}
	}

	private StompVersion acceptVersion(StompMessage msg) {
		final StompVersion version = StompVersion.stompVersion(wsSession.getNegotiatedSubprotocol());

		final Collection<String> clientSupportedVersion = Arrays
				.asList(msg.frame().getFirst(ACCEPT_VERSION).get().split(","));
		if (!clientSupportedVersion.contains(version.version())) {
			final String errorMsg = String.format("Incompatible subprotocol and 'accept-version'! [subprotocol=%s,accept-version=%s]", wsSession.getNegotiatedSubprotocol(), clientSupportedVersion);
			final Frame.Builder error = Frame.error();
			error.body(TEXT_PLAIN, errorMsg);
			sendToClient(error.build());
			throw new IllegalStateException(errorMsg);
		}
		return version;
	}
}
