/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.validation.constraints.NotNull;

/**
 * Defines a header type.
 *
 * @author Daniel Siviter
 * @since v1.0 [14 Jul 2016]
 */
public interface Header {
	public static final Comparator<Header> HEADER_COMPARATOR = new HeaderComparator();
	public static final Map<CharSequence, Header> HEADERS = new HashMap<>();


	/**
	 * @return the header value.
	 */
	String value();


	// --- Static Methods ---

	/**
	 *
	 * @param value the header value.
	 * @return the header instance.
	 */
	public static Header valueOf(@NotNull CharSequence value) {
		Header header = HEADERS.get(value);

		if (header != null) {
			return header;
		}
		for (Header h : Standard.values()) {
			if (equalsIgnoreCase(h.value(), value)) {
				header = h;
				break;
			}
		}
		if (header != null) {
			HEADERS.put(value, header);
			return header;
		}
		for (Header h : Custom.values()) {
			if (equalsIgnoreCase(h.value(), value)) {
				header = h;
				break;
			}
		}
		if (header != null) {
			HEADERS.put(value, header);
			return header;
		}

		header = new StringHeader(value);
		HEADERS.put(value, header);
		return header;
	}


	// --- Inner Classes ---

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [18 Sep 2017]
	 */
	public enum Standard implements Header {
		CONTENT_TYPE("content-type"),
		CONTENT_LENGTH("content-length"),
		ACCEPT_VERSION("accept-version"),
		VERSION("version"),
		HOST("host"),
		RECEIPT("receipt"),
		RECEIPT_ID("receipt-id"),
		LOGIN("login"),
		PASSCODE("passcode"),
		HEART_BEAT("heart-beat"),
		SESSION("session"),
		SERVER("server"),
		DESTINATION("destination"),
		CORRELATION_ID("correlation-id"),
		REPLY_TO("reply-to"),
		EXPIRATION_TIME("expires"),
		PRIORITY("priority"),
		TYPE("type"),
		PERSISTENT("persistent"),
		MESSAGE_ID("message-id"),
		PRORITY("priority"),
		REDELIVERED("redelivered"),
		TIMESTAMP("timestamp"),
		SUBSCRIPTION("subscription"),
		ID("id"),
		ACK("ack"),
		TRANSACTION("transaction");

		public final String value;

		Standard(String value) {
			this.value = value;
		}

		@Override
		public String value() {
			return this.value;
		}
	}

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [18 Sep 2017]
	 */
	public enum Custom implements Header {
		SELECTOR("selector");

		public final String value;

		Custom(String value) {
			this.value = value;
		}

		@Override
		public String value() {
			return this.value;
		}
	}

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [18 Sep 2017]
	 */
	public static class StringHeader implements Header {
		private final String value;

		StringHeader(CharSequence value) {
			this.value = value.toString().intern();
		}

		@Override
		public String value() {
			return value;
		}

		@Override
		public int hashCode() {
			return Objects.hashCode(this.value.toLowerCase());
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null || getClass() != obj.getClass())
				return false;
			final StringHeader other = (StringHeader) obj;
			return this.value.equalsIgnoreCase(other.value);
		}

		@Override
		public String toString() {
			return this.value;
		}
	}

	/**
	 *
	 */
	public static class HeaderComparator implements Comparator<Header> {
		@Override
		public int compare(Header o1, Header o2) {
			return String.CASE_INSENSITIVE_ORDER.compare(o1.value(), o2.value());
		}
	}
}
