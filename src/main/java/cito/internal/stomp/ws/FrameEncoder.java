/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp.ws;

import static cito.internal.stomp.Encoding.from;
import java.nio.ByteBuffer;

import javax.websocket.EncodeException;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;

import cito.internal.stomp.Frame;

/**
 * Handles decoding/encoding of {@link Frame}s.
 *
 * @author Daniel Siviter
 * @since v1.0 [12 Jul 2016]
 */
public class FrameEncoder {
	private static final int DEFAULT_BUFFER_CAPACITY = 16 * 1024;
	private static final boolean DEFAULT_BUFFER_DIRECT = false;

	protected int bufferCapacity = DEFAULT_BUFFER_CAPACITY;
	protected boolean bufferDirect = DEFAULT_BUFFER_DIRECT;

	public void init(EndpointConfig config) {
		final Integer bufferCapacity = (Integer) config.getUserProperties().get("stomp.bufferCapacity");
		if (bufferCapacity != null) {
			this.bufferCapacity = bufferCapacity.intValue();
		}
		final Boolean bufferDirect = (Boolean) config.getUserProperties().get("stomp.bufferDirect");
		if (bufferCapacity != null) {
			this.bufferDirect = bufferDirect.booleanValue();
		}
	}

	public ByteBuffer encode(Session session, Frame object) throws EncodeException {
		return from(object, this.bufferDirect, this.bufferCapacity);
	}
}
