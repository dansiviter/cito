/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp.ws;

import static cito.internal.stomp.Encoding.from;
import static cito.internal.stomp.Encoding.isFrame;

import java.io.IOException;
import java.nio.ByteBuffer;

import javax.websocket.DecodeException;
import javax.websocket.Session;

import cito.internal.stomp.Frame;

/**
 * Handles decoding/encoding of {@link Frame}s.
 * <p>
 * <strong>Note:</strong> The STOMP specification states "servers MAY place maximum limits" on certain elements. For
 * simplicity sake this is ignored as the WebSocket implementation will have a maximum buffer size. If that's breached
 * then it makes it a moot point of any limits in here. e.g. for Undertow this is 16kb.
 *
 * @author Daniel Siviter
 * @since v1.0 [12 Jul 2016]
 */
public class FrameDecoder {
	public boolean willDecode(Session session, ByteBuffer bytes) {
		return isFrame(bytes);
	}

	public Frame decode(Session session, ByteBuffer bytes) throws DecodeException {
		try {
			return from(bytes);
		} catch (IOException | AssertionError e) {
			throw new DecodeException(bytes, e.getMessage(), e);
		}
	}
}
