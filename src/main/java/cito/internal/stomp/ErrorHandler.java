/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import java.io.IOException;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Specializes;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;

import cito.MimeTypes;
import cito.event.Message;
import cito.internal.Connection;
import cito.internal.Log;
import cito.internal.MessageBuilderFactory;
import cito.internal.MessageBuilderFactory.MessageBuilder;

/**
 * Defines a way to handle errors. Use the {@link Specializes} mechanism to
 * override.
 *
 * @author Daniel Siviter
 * @since v1.0 [1 Sep 2016]
 */
@ApplicationScoped
public class ErrorHandler {
	@Inject
	protected Log log;
	@Inject
	protected Provider<MessageBuilderFactory> factory;

	/**
	 *
	 * @param connection the the connection associated with this error.
	 * @param sessionId  the session identifier.
	 * @param cause      the message that caused the issue.
	 * @param msg        the message. This may be {@code null}.
	 * @param e          the cause exception. This may be {@code null}.
	 */
	public void onError(
			@Nonnull Connection connection,
			@Nonnull String sessionId,
			@Nonnull Message cause,
			String msg,
			Exception e)
	{
		this.log.warnf(e, "Error while processing frame! [sessionId=%s]", sessionId);
		final MessageBuilder<?> builder = this.factory.get().builder().error(Optional.of(cause));
		if (msg == null && e == null) {
			throw new IllegalStateException("Either 'msg' or 'e' must not be null!");
		}
		if (msg == null && e != null) {
			msg = e.getMessage() != null ? e.getMessage() : e.getClass().getSimpleName();
		}
		try {
			builder.body(msg, MimeTypes.TEXT_PLAIN);
			connection.on(builder.build());
			connection.close(new CloseReason(CloseCodes.CLOSED_ABNORMALLY, "Error"));
		} catch (IOException ioe) {
			this.log.errorf(ioe, "Unable to close connection!");
		}
	}
}
