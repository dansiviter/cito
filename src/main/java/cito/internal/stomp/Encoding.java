/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static java.nio.ByteBuffer.allocate;
import static java.nio.ByteBuffer.allocateDirect;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.List;
import java.util.Map.Entry;

/**
 * Performing encoding and decoding of {@link Frame}s.
 *
 * @author Daniel Siviter
 * @since v1.0 [29 Nov 2017]
 */
public enum Encoding { ;
	static final byte COLON = ':';
	public static final byte LF = '\n';
	public static final byte CR = '\r';
	public static final byte NULL = '\u0000';

	/**
	 * @param frame the frame to serialise.
	 * @return the created buffer with the frame serialised into it.
	 */
	public static ByteBuffer from(Frame frame) {
		return from(frame, false, 16 * 1024);
	}

	/**
	 * @param frame the frame to serialise.
	 * @param direct if {@code true} a direct byte buffer will be used.
	 * @param capacity the capasity of the buffer.
	 * @return the created buffer with the frame serialised into it.
	 * @throws BufferOverflowException potentially thrown if there is insufficient space.
	 * @see ByteBuffer#allocate(int)
	 * @see ByteBuffer#allocateDirect(int)
	 */
	public static ByteBuffer from(Frame frame, boolean direct, int capacity) {
		final ByteBuffer buf = direct ? allocateDirect(capacity) : allocate(capacity);
		write(frame, buf);
		buf.flip();
		return buf;
	}

	/**
	 *
	 * @param frame the frame to write to the buffer.
	 * @param buf the buffer to write to.
	 */
	public static void write(Frame frame, ByteBuffer buf) {
		if (frame.isHeartBeat()) {
			buf.put(LF);
			return;
		}

		buf.put(UTF_8.encode(frame.command().name())).put(LF);

		for (Entry<Header, List<String>> e : frame.headers().entrySet()) {
			for (String value : e.getValue()) {
				buf.put(UTF_8.encode(e.getKey().value())).put(COLON).put(UTF_8.encode(value)).put(LF);
			}
		}

		buf.put(LF);
		frame.body().ifPresent(buf::put);
		buf.put(NULL);
	}

	/**
	 * @param buf the buffer to test.
	 * @return {@code true} if this buffer represents a frame.
	 */
	public static boolean isFrame(ByteBuffer buf) {
		boolean nullFound = false;
		for (int i = 1; i < 10; i++) {
			int index = buf.limit() - i;
			if (index < 0) {
				break;
			}
			if (buf.get(index) == NULL) {
				nullFound = true;
				break;
			}
		}

		if (!nullFound) {
			return buf.limit() == 1 && buf.get(0) == LF; // beat
		}

		skipEoL(buf);
		final CharBuffer command = readLine(buf);
		try {
			return Command.valueOf(command.toString()) != null;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

	/**
	 * @param buf the buffer to deserialise.
	 * @return the new frame.
	 * @throws IOException thrown if unable to deserialise.
	 * @throws AssertionError if the {@link Frame.Builder} does not have required values.
	 */
	public static Frame from(ByteBuffer buf) throws IOException, AssertionError {
		skipEoL(buf);

		final CharBuffer command = readLine(buf);
		if (command.length() == 0) {
			return Frame.HEART_BEAT;
		}
		Frame.Builder builder = Frame.builder(Command.valueOf(command.toString()));
		int contentLength = readHeaders(buf, builder);

		if (contentLength > 0) {
			if (contentLength != buf.remaining() - 1) { // ignoring last octet
				throw new IOException("Content-Length doesn't match remaining bytes!");
			}
			buf.limit(buf.limit() - 1);
			final ByteBuffer body = ByteBuffer.allocate(contentLength);
			body.put(buf);
			body.flip();
			builder.body(null, body);
		}

		return builder.build();
	}

	/**
	 * @param buf the buffer to deserialise.
	 * @param builder the frame builder instance.
	 * @return the value for {@code content-length}.
	 * @throws IOException thrown if unable to deserialse.
	 */
	private static int readHeaders(ByteBuffer buf, Frame.Builder builder) throws IOException {
		int contentLength = -1;
		while (true) {
			final CharBuffer cBuf = readLine(buf);
			if (cBuf == null || !cBuf.hasRemaining()) {
				break;
			}
			if (!indexOf(':', cBuf)) {
				throw new IOException("A header must be of the form '<name>:<value>[,<value>]'! [" + cBuf.rewind() + "]");
			} else {
				final CharBuffer valueBuf = cBuf.slice();
				final Header header = Header.valueOf(cBuf.flip().toString());
				final String value = valueBuf.position(valueBuf.position() + 1).toString();
				if (Header.Standard.CONTENT_LENGTH.equals(header)) {
					// this will get set by Frame#body
					contentLength = Integer.valueOf(value);
				} else {
					builder.header(header, value.split(","));
				}
			}
		}
		return contentLength;
	}

	/**
	 * Moves the buffer position to the index of the char buffer.
	 *
	 * @param c the character to find in the buffer.
	 * @param buf the character buffer to search in.
	 * @return {@code true} if the character is found.
	 */
	private static boolean indexOf(char c, CharBuffer buf) {
		while (buf.hasRemaining()) {
			if (buf.get() == c) {
				buf.position(buf.position() - 1);
				return true;
			}
		}
		return false;

	}

	/**
	 * @param buf the byte buffer to test.
	 * @return {@code true} if the buffer is at a EoL.
	 */
	private static boolean isEol(ByteBuffer buf) {
		if (!buf.hasRemaining()) {
			return false;
		}
		if (buf.get(buf.position()) == LF) {
			return true;
		}
		return buf.get(buf.position()) == CR && buf.hasRemaining() && buf.get(buf.position() + 1) == LF;
	}

	/**
	 * @param buf the buffer to touch.
	 * @return {@code true} if a End of Line was encountered.
	 */
	private static boolean skipEoL(ByteBuffer buf) {
		if (isEol(buf)) {
			if (buf.get() == CR) {
				buf.get();
			}
			buf.compact();
			buf.flip();
			return true;
		}
		return false;
	}

	/**
	 * Reads a line in and prepares the buffer for next line.
	 *
	 * @param buf the source buffer.
	 * @return the char buffer representing the line.
	 */
	private static CharBuffer readLine(ByteBuffer buf) {
		while (buf.hasRemaining() && !isEol(buf)) {
			buf.get();
		}
		final ByteBuffer tmp = buf.duplicate();
		tmp.flip();
		try {
			return UTF_8.decode(tmp);
		} finally {
			skipEoL(buf);
		}
	}
}
