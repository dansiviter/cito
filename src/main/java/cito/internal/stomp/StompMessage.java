/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.stomp;

import static cito.internal.stomp.Header.Standard.RECEIPT;
import static cito.internal.stomp.Header.Standard.RECEIPT_ID;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.activation.MimeType;
import javax.annotation.Nonnull;

import cito.event.Message;
import cito.internal.MessageBuilderFactory;
import cito.internal.MessageBuilderFactory.MessageBuilder;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [28 Nov 2019]
 */
public class StompMessage extends Message {
	private final Frame frame;

	private StompMessage(Optional<String> sessionId, @Nonnull Frame frame) {
		super(sessionId);
		this.frame = frame;
	}

	@Override
	public Map<Header, List<String>> headers() {
		return this.frame.headers();
	}

	@Override
	public Optional<Type> type() {
		switch (frame.command()) {
		case CONNECTED:
			return Optional.of(Type.CONNECTED);
		case SEND:
			return Optional.of(Type.SEND);
		case SUBSCRIBE:
			return Optional.of(Type.SUBSCRIBE);
		case UNSUBSCRIBE:
			return Optional.of(Type.UNSUBSCRIBE);
		case DISCONNECT:
			return Optional.of(Type.DISCONNECT);
		case HEARTBEAT:
			return Optional.of(Type.HEARTBEAT);
		default:
			return Optional.empty();
		}
	}

	@Override
	public Optional<String> id() {
		return this.frame.id();
	}

	@Override
	public Optional<ByteBuffer> body() {
		return this.frame.body();
	}

	@Override
	public Optional<MimeType> contentType() {
		return this.frame.contentType();
	}

	@Override
	public Optional<String> destination() {
		return this.frame.destination();
	}

	public Frame frame() {
		return frame;
	}

	/**
	 *
	 * @param factory the message factory.
	 * @param subprotocol the subprotocol.
	 * @return the new builder instance.
	 */
	public static Builder builder(@Nonnull MessageBuilderFactory factory, @Nonnull String subprotocol) {
		return new Builder(factory, subprotocol);
	}

	private static Command from(Type type) {
		switch (type) {
		case CONNECTED:
			return Command.CONNECTED;
		case DISCONNECT:
			return Command.DISCONNECT;
		case HEARTBEAT:
			return Command.HEARTBEAT;
		case SEND:
			return Command.SEND;
		case SUBSCRIBE:
			return Command.SUBSCRIBE;
		case UNSUBSCRIBE:
			return Command.UNSUBSCRIBE;
		default:
			throw new IllegalArgumentException("Unknown type! [" + type + "]");
		}
	}

	// --- Inner Classes ---

	/**
	 *
	 */
	public static class Builder extends MessageBuilder<StompMessage> {
		private Optional<ByteBuffer> raw = Optional.empty();

		Builder(@Nonnull MessageBuilderFactory factory, @Nonnull String subprotocol) {
			super(factory, subprotocol);
		}

		@Override
		public Builder raw(ByteBuffer buf) throws IOException {
			this.raw = Optional.of(buf);
			return this;
		}

		@Override
		public Builder error(Optional<Message> cause) {
			cause.ifPresent(c -> c.getFirst(RECEIPT).ifPresent(v -> header(RECEIPT_ID, v)));
			return this;
		}

		@Override
		public StompMessage build() {
			if (this.raw.isPresent()) {
				if (!this.headers.isEmpty() || this.body.isPresent() || this.type.isPresent()) {
					throw new IllegalStateException("#raw(...) can only be used with #sessionId(...) and #build()!");
				}
				try {
					return new StompMessage(this.sessionId, Encoding.from(raw.get()));
				} catch (IOException e) {
					throw new IllegalStateException(e);
				}
			}

			if (!this.type.isPresent()) {
				throw new IllegalStateException("Type must be set!");
			}

			final Frame.Builder builder = Frame.builder(from(this.type.get()));
			builder.headers(this.headers);
			this.body.ifPresent(builder::body);
			return new StompMessage(this.sessionId, builder.build());
		}
	}
}
