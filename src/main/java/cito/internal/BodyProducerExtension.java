/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.activation.MimeType;
import javax.annotation.Nonnull;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.Extension;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.enterprise.inject.spi.ProcessInjectionPoint;

import cito.annotation.Body;
import cito.event.Message;
import cito.ext.Serialiser;
import cito.internal.io.ByteBufferInputStream;

/**
 * An extension to process {@link Body} annotations for injection of deserialised values.
 *
 * @author Daniel Siviter
 * @since v1.0 [3 May 2017]
 */
public class BodyProducerExtension implements Extension {
	private final List<ProcessInjectionPoint<?, ?>> found = new ArrayList<>();

	/**
	 *
	 * @param pip injection point event.
	 */
	public void findBody(@Observes ProcessInjectionPoint<?, ?> pip) {
		if (pip.getInjectionPoint().getAnnotated().isAnnotationPresent(Body.class)) {
			found.add(pip);
		}
	}

	/**
	 *
	 * @param abd after bean discovery event.
	 * @param beanManager instance of the bean manager.
	 */
	public void addBeans(@Observes AfterBeanDiscovery abd, @Nonnull BeanManager beanManager) {
		this.found.forEach(pip -> createBean(abd, pip, beanManager));
		this.found.clear();
	}

	/**
	 *
	 * @param abd the bean discovery event.
	 * @param pip the injection point.
	 * @param beanManager instance of the bean manager.
	 * @return the created bean instance.
	 */
	private void createBean(@Nonnull AfterBeanDiscovery abd, @Nonnull ProcessInjectionPoint<?, ?> pip, @Nonnull BeanManager beanManager) {
		final InjectionPoint ip = pip.getInjectionPoint();
		final Type type = ip.getType();
		final Class<?> rawType = getRawType(type);
		abd.addBean()
				.name(ip.getMember().getName())
				.beanClass(rawType)
				.types(type, Object.class)
				.qualifiers(ip.getQualifiers())
				.createWith(cc -> create(cc, type, beanManager));
	}

	/**
	 *
	 * @param <T> the expected type.
	 * @param cc the creational context.
	 * @param type the bean type.
	 * @param beanManager bean manager.
	 * @return the created bean.
	 */
	private <T> T create(@Nonnull CreationalContext<?> cc, @Nonnull Type type, @Nonnull BeanManager beanManager) {
		final Instance<Object> instance = beanManager.createInstance();
		final Message  msg = instance.select(Message.class).get();
		try (InputStream is = new ByteBufferInputStream(msg.body().get())) {
			final MimeType contentType = msg.contentType().orElse(null);
			final Serialiser serialiser = instance.select(Serialiser.class).get();
			return serialiser.readFrom(type, contentType, is);
		} catch (IOException e) {
			throw new IllegalStateException("Unable to serialise!", e);
		}
	}

	/**
	 *
	 * @param type the input type.
	 * @return the found raw type.
	 * @throws IllegalStateException if unable to find raw type.
	 */
	private static Class<?> getRawType(@Nonnull Type type) {
		if (type instanceof Class) {
			return (Class<?>) type;
		}
		if (type instanceof ParameterizedType && ((ParameterizedType) type).getRawType() instanceof Class<?>) {
			return (Class<?>) ((ParameterizedType) type).getRawType();
		}
		throw new IllegalStateException("Unable to decipher bean type! [" + type + "]");
	}
}
