/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import java.io.IOException;

import javax.annotation.Nonnull;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.Session;

import cito.event.Message;

/**
 * Defines a STOMP connection.
 *
 * @author Daniel Siviter
 * @since v1.0 [25 Jul 2016]
 */
public interface Connection {
	/**
	 * Called on openining of a websocket connection.
	 *
	 * @param session the websocket session.
	 * @param config the endpoint configuration.
	 */
	default void open(Session session, EndpointConfig config) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @param msg the message event.
	 */
	void on(@Nonnull Message msg);

	/**
	 * @return the session identifier.
	 */
	String getSessionId();

	/**
	 *
	 * @param reason the reason for closure.
	 * @throws IOException thrown when issue in closing occurs.
	 */
	void close(@Nonnull CloseReason reason) throws IOException;

	/**
	 * Called when a message is received from the client.
	 *
	 * @param msg the message.
	 */
	default void fromClient(@Nonnull Message msg) {
		throw new UnsupportedOperationException();
	}
}
