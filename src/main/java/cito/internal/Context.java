/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.internal.Messages.*;
import static java.util.stream.Collectors.toList;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.Nonnull;

/**
 * A utility to hold contextual information.
 *
 * @author Daniel Siviter
 * @since v1.0 [24 Nov 2018]
 */
public class Context {
	private static final ThreadLocal<Deque<Context>> CONTEXT = ThreadLocal.withInitial(ArrayDeque::new);
	private static final Scope NOOP_SCOPE = () -> { };

	private final Optional<Context> parent;
	private final Map<String, Object> register = new ConcurrentHashMap<>();

	/**
	 * @param parent the parent scope.
	 */
	private Context(@Nonnull Optional<Context> parent) {
		this.parent = parent;
	}

	/**
	 * @return the parent scope.
	 */
	public Optional<Context> parent() {
		return this.parent;
	}

	/**
	 *
	 * @param <T> the expected type.
	 * @param key the key.
	 * @return the value for the key.
	 */
	@SuppressWarnings("unchecked")
	public <T> Optional<T> get(@Nonnull String key) {
		final T value = (T) this.register.get(key);
		if (value != null) {
			return Optional.of(value);
		}
		return (Optional<T>) parent.map(p -> p.get(key).orElse(null));
	}

	/**
	 *
	 * @param <T> the expected type.
	 * @param key the key.
	 * @return the value for the key.
	 */
	public <T> Optional<T> get(@Nonnull Class<? super T> key) {
		return get(toKey(key));
	}

	/**
	 *
	 * @param <T>   the expected type.
	 * @param key   the key.
	 * @param value the value for the key.
	 */
	public <T> void put(@Nonnull String key, @Nonnull T value) {
		if (get(key).isPresent()) {
			throw messages().contextValueAlreadySet(key);
		}
		this.register.put(key, value);
	}

	/**
	 *
	 * @param <T>   the expected type.
	 * @param key   the key.
	 * @param value the value for the key.
	 */
	public <T> void put(@Nonnull Class<? super T> key, @Nonnull T value) {
		put(toKey(key), value);
	}

	/**
	 *
	 * @param <T> the expected type.
	 * @param key the key.
	 * @return the current value.
	 */
	@SuppressWarnings("unchecked")
	public <T> T remove(@Nonnull String key) {
		return (T) this.register.remove(key);
	}

	/**
	 *
	 * @param <T> the expected type.
	 * @param key the key.
	 * @return the current value.
	 */
	public <T> T remove(@Nonnull Class<? super T> key) {
		return remove(toKey(key));
	}

	/**
	 *
	 * @param <T>   the expected type.
	 * @param key   the key.
	 * @param value the value for the key.
	 * @return a scope instance.
	 */
	public <T> Scope putScoped(@Nonnull String key, @Nonnull T value) {
		put(key, value);
		return () -> {
			remove(key);
		};
	}

	/**
	 *
	 * @param <T>   the expected type.
	 * @param key   the key.
	 * @param value the value for the key.
	 * @return a scope instance.
	 */
	public <T> Scope putScoped(@Nonnull Class<? super T> key, @Nonnull T value) {
		return putScoped(toKey(key), value);
	}

	// --- Static Methods ---

	/**
	 * @return a new context with no parent.
	 */
	public static Context create() {
		return create(Optional.empty());
	}

	/**
	 * @param parent the parent context.
	 * @return a new context with the given parent.
	 */
	public static Context create(@Nonnull Context parent) {
		return create(Optional.of(parent));
	}

	/**
	 * @param parent the parent context.
	 * @return a new context with the given parent.
	 */
	public static Context create(Optional<Context> parent) {
		return new Context(parent);
	}

	/**
	 * @return the current context.
	 */
	public static Optional<Context> context() {
		return Optional.ofNullable(CONTEXT.get().peek());
	}

	/**
	 *
	 * @param <T>   the expected type.
	 * @param key   the key.
	 * @param value the value for the key.
	 * @return a scope instance.
	 */
	public static <T> Scope putScopedContextual(@Nonnull String key, @Nonnull T value) {
		final Optional<Scope> scope = context().map(c -> c.putScoped(key, value));
		return scope.orElse(NOOP_SCOPE);
	}

	/**
	 *
	 * @param <T>   the expected type.
	 * @param key   the key.
	 * @param value the value for the key.
	 * @return a scope instance.
	 */
	public static <T> Scope putScopedContextual(@Nonnull Class<? super T> key, @Nonnull T value) {
		return putScopedContextual(toKey(key), value);
	}

	/**
	 *
	 * @param <T> the expected type.
	 * @param key the key.
	 * @return the value for the key.
	 */
	@SuppressWarnings("unchecked")
	public static <T> Optional<T> getContextual(@Nonnull String key) {
		return (Optional<T>) context().map(c -> c.get(key).orElse(null));
	}

	/**
	 *
	 * @param <T> the expected type.
	 * @param key the key.
	 * @return the value for the key.
	 */
	public static <T> Optional<T> getContextual(@Nonnull Class<? super T> key) {
		return getContextual(toKey(key));
	}

	/**
	 *
	 * @param context the context to set.
	 * @return the scope for the context.
	 */
	public static Scope scope(@Nonnull Context context) {
		return scope(Optional.of(context));
	}

	/**
	 *
	 * @param context the context to set.
	 * @return the scope for the context.
	 */
	public static Scope scope(Optional<Context> context) {
		return context.map(c -> {
			CONTEXT.get().push(c);
			return (Scope) () -> CONTEXT.get().pop();
		}).orElseGet(() -> NOOP_SCOPE);
	}

	/**
	 *
	 * @param context  the context to use.
	 * @param runnable the runnable.
	 */
	public static void inContext(Optional<Context> context, @Nonnull Runnable runnable) {
		try (Scope scope = scope(context)) {
			runnable.run();
		}
	}

	/**
	 *
	 * @param context  the context to use.
	 * @param runnable the runnable.
	 */
	public static void inContext(@Nonnull Context context, @Nonnull Runnable runnable) {
		try (Scope scope = scope(context)) {
			runnable.run();
		}
	}

	/**
	 *
	 * @param <T>      the expected return type.
	 * @param context  the context to use.
	 * @param callable the callable.
	 * @return the response of the callable.
	 * @throws Exception potentially thrown by the callable.
	 */
	public static <T> T inContext(Optional<Context> context, @Nonnull Callable<T> callable) throws Exception {
		try (Scope scope = scope(context)) {
			return callable.call();
		}
	}

	/**
	 *
	 * @param <T>      the expected return type.
	 * @param context  the context to use.
	 * @param callable the callable.
	 * @return the response of the callable.
	 * @throws Exception potentially thrown by the callable.
	 */
	public static <T> T inContext(@Nonnull Context context, @Nonnull Callable<T> callable) throws Exception {
		try (Scope scope = scope(context)) {
			return callable.call();
		}
	}

	/**
	 *
	 * @param runnable the runnable to wrap using current context, if any.
	 * @return the wrapped runnable.
	 */
	public static Runnable wrap(@Nonnull Runnable runnable) {
		return wrap(context(), runnable);
	}

	/**
	 *
	 * @param context  the context to use.
	 * @param runnable the runnable to wrap.
	 * @return the wrapped runnable.
	 */
	public static Runnable wrap(Optional<Context> context, Runnable runnable) {
		return context.isPresent() ? () -> inContext(context, runnable) : runnable;
	}

	/**
	 *
	 * @param <T>      the expected return type.
	 * @param callable the callable to wrap using current context, if any.
	 * @return the wrapped callable.
	 */
	public static <T> Callable<T> wrap(Callable<T> callable) {
		return wrap(context(), callable);
	}

	/**
	 *
	 * @param <T>      the expected return type.
	 * @param context  the context to use.
	 * @param callable the callable to wrap.
	 * @return the wrapped callable.
	 */
	public static <T> Callable<T> wrap(Optional<Context> context, Callable<T> callable) {
		return context.isPresent() ? () -> inContext(context, callable) : callable;
	}

	/**
	 *
	 * @param executor the executor to wrap.
	 * @return the new executor.
	 */
	public static ExecutorService wrap(ExecutorService executor) {
		return new ContextExecutorService(executor);
	}

	/**
	 *
	 * @param executor the executor to wrap.
	 * @return the new executor.
	 */
	public static Executor wrap(Executor executor) {
		return new ContextExecutor(executor);
	}

	/**
	 *
	 * @param executor the executor to wrap.
	 * @return the new executor.
	 */
	public static ScheduledExecutorService wrap(ScheduledExecutorService executor) {
		return new ContextScheduledExecutorService(executor);
	}

	private static String toKey(Class<?> cls) {
		return cls.getName();
	}

	// --- Inner Classes ---

	/**
	 *
	 */
	@FunctionalInterface
	public static interface Scope extends AutoCloseable {
		@Override
		void close();
	}

	/**
	 *
	 */
	private static class ContextExecutor implements Executor {
		private final Executor delegate;

		private ContextExecutor(Executor delegate) {
			this.delegate = delegate;
		}

		protected Executor delegate() {
			return this.delegate;
		}

		@Override
		public void execute(Runnable command) {
			delegate().execute(wrap(command));
		}
	}

	/**
	 *
	 */
	private static class ContextExecutorService extends ContextExecutor implements ExecutorService {
		private ContextExecutorService(ExecutorService delegate) {
			super(delegate);
		}

		@Override
		protected ExecutorService delegate() {
			return (ExecutorService) super.delegate();
		}

		@Override
		public void shutdown() {
			delegate().shutdown();
		}

		@Override
		public List<Runnable> shutdownNow() {
			return delegate().shutdownNow();
		}

		@Override
		public boolean isShutdown() {
			return delegate().isShutdown();
		}

		@Override
		public boolean isTerminated() {
			return delegate().isTerminated();
		}

		@Override
		public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
			return delegate().awaitTermination(timeout, unit);
		}

		@Override
		public <T> Future<T> submit(Callable<T> task) {
			return delegate().submit(Context.wrap(task));
		}

		@Override
		public <T> Future<T> submit(Runnable task, T result) {
			return delegate().submit(Context.wrap(task), result);
		}

		@Override
		public Future<?> submit(Runnable task) {
			return delegate().submit(Context.wrap(task));
		}

		@Override
		public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
			return delegate().invokeAll(wrap(tasks));
		}

		@Override
		public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
				throws InterruptedException {
			return delegate().invokeAll(wrap(tasks), timeout, unit);
		}

		@Override
		public <T> T invokeAny(Collection<? extends Callable<T>> tasks)
				throws InterruptedException, ExecutionException {
			return delegate().invokeAny(wrap(tasks));
		}

		@Override
		public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit)
				throws InterruptedException, ExecutionException, TimeoutException {
			return delegate().invokeAny(wrap(tasks), timeout, unit);
		}

		private <T> Collection<? extends Callable<T>> wrap(Collection<? extends Callable<T>> tasks) {
			return tasks.stream().map(Context::wrap).collect(toList());
		}
	}

	/**
	 *
	 */
	private static class ContextScheduledExecutorService extends ContextExecutorService
			implements ScheduledExecutorService {
		private ContextScheduledExecutorService(ScheduledExecutorService delegate) {
			super(delegate);
		}

		@Override
		protected ScheduledExecutorService delegate() {
			return (ScheduledExecutorService) super.delegate();
		}

		@Override
		public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
			return delegate().schedule(Context.wrap(command), delay, unit);
		}

		@Override
		public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
			return delegate().schedule(Context.wrap(callable), delay, unit);
		}

		@Override
		public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
			return delegate().scheduleAtFixedRate(wrap(command), initialDelay, period, unit);
		}

		@Override
		public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay,
				TimeUnit unit) {
			return delegate().scheduleWithFixedDelay(wrap(command), initialDelay, delay, unit);
		}
	}
}
