/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static java.util.Collections.emptySet;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nonnull;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterBeanDiscovery;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.BeforeBeanDiscovery;
import javax.enterprise.inject.spi.ObserverMethod;
import javax.enterprise.inject.spi.ProcessObserverMethod;
import javax.websocket.Session;

import org.tomitribe.microscoped.core.ScopeContext;

import cito.annotation.OnAdded;
import cito.annotation.OnConnected;
import cito.annotation.OnDisconnect;
import cito.annotation.OnRemoved;
import cito.annotation.OnSend;
import cito.annotation.OnSubscribe;
import cito.annotation.OnUnsubscribe;
import cito.annotation.WebSocketScope;
import cito.event.DestinationChanged;
import cito.event.Message;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [12 Jul 2016]
 */
public class MessageObserverExtension implements javax.enterprise.inject.spi.Extension {
	private final Map<Class<? extends Annotation>, Set<ObserverMethod<Message>>> messageObservers = new ConcurrentHashMap<>();
	private final Map<Class<? extends Annotation>, Set<ObserverMethod<DestinationChanged>>> destinationObservers = new ConcurrentHashMap<>();

	/**
	 * @param <A> the annotation type.
	 * @param cls the annotation class.
	 * @param method the observer method.
	 */
	private <A extends Annotation> void registerMessageObserver(Class<A> cls, ObserverMethod<Message> method) {
		Set<ObserverMethod<Message>> annotations = this.messageObservers.get(cls);
		if (annotations == null) {
			annotations = new HashSet<>();
			this.messageObservers.put(cls, annotations);
		}
		annotations.add(method);
	}

	/**
	 * @param e the observer method event.
	 */
	public void registerMessageEvent(@Observes ProcessObserverMethod<Message, ?> e) {
		final ObserverMethod<Message> method = e.getObserverMethod();
		for (Annotation a : method.getObservedQualifiers()) {
			if (a instanceof OnConnected) {
				registerMessageObserver(OnConnected.class, method);
				e.veto();
			} else if (a instanceof OnSend) {
				registerMessageObserver(OnSend.class, method);
				e.veto();
			} else if (a instanceof OnSubscribe) {
				registerMessageObserver(OnSubscribe.class, method);
				e.veto();
			} else if (a instanceof OnUnsubscribe) {
				registerMessageObserver(OnUnsubscribe.class, method);
				e.veto();
			} else if (a instanceof OnDisconnect) {
				registerMessageObserver(OnDisconnect.class, method);
				e.veto();
			}
		}
	}

	/**
	 * @param qualifier qualifier annotation type.
	 * @return a set of matching observer methods.
	 */
	public Set<ObserverMethod<Message>> getMessageObservers(@Nonnull Class<? extends Annotation> qualifier) {
		final Set<ObserverMethod<Message>> observers = this.messageObservers.get(qualifier);
		return observers == null ? Collections.emptySet() : observers;
	}

	/**
	 *
	 * @param cls the annotation type.
	 * @param method the method to register.
	 */
	private void registerDestinationObserver(
			@Nonnull Class<? extends Annotation> cls,
			@Nonnull ObserverMethod<DestinationChanged> method)
	{
		final Set<ObserverMethod<DestinationChanged>> annotations =
				this.destinationObservers.computeIfAbsent(cls, k -> new HashSet<>());
		annotations.add(method);
	}

	/**
	 * @param e the event.
	 */
	public void registerDestinationEvent(@Observes ProcessObserverMethod<DestinationChanged, ?> e) {
		final ObserverMethod<DestinationChanged> method = e.getObserverMethod();
		for (Annotation a : method.getObservedQualifiers()) {
			if (a instanceof OnAdded) {
				registerDestinationObserver(OnAdded.class, method);
			}
			if (a instanceof OnRemoved) {
				registerDestinationObserver(OnRemoved.class, method);
			}
		}
	}

	/**
	 * @param qualifier qualifier annotation type.
	 * @return a set of matching observer methods.
	 */
	public Set<ObserverMethod<DestinationChanged>> getDestinationObservers(
			@Nonnull Class<? extends Annotation> qualifier)
	{
		final Set<ObserverMethod<DestinationChanged>> observers = this.destinationObservers.get(qualifier);
		return observers == null ? emptySet() : observers;
	}

	/**
	 * @param event the event.
	 */
	public void addScope(@Observes BeforeBeanDiscovery event) {
		event.addScope(WebSocketScope.class, true, false);
	}

	/**
	 * @param event the event.
	 * @param beanManager the bean manager instance.
	 */
	public void registerContexts(@Observes AfterBeanDiscovery event, BeanManager beanManager) {
		event.addContext(new ScopeContext<Session>(WebSocketScope.class));
	}
}
