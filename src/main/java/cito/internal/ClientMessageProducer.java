/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import java.util.Optional;

import javax.annotation.Nonnull;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

import cito.event.Message;
import cito.internal.Context.Scope;

/**
 * Holds and produces only the message sent from the client.
 *
 * @author Daniel Siviter
 * @since v1.0 [25 Jan 2017]
 */
@ApplicationScoped
public class ClientMessageProducer {
	private static final String KEY = "clientMessage";

	/**
	 * @return the thread-local message instance or {@code null} if none.
	 */
	@Produces @Dependent
	public static Message get() {
		final Optional<Message> msg = Context.getContextual(KEY);
		return msg.orElse(null);
	}

	/**
	 * @param msg the thread-local message to set.
	 * @return a closable to reset the thread-local.
	 * @throws IllegalStateException if the thread-local already contains a value.
	 */
	public static Scope set(@Nonnull Message msg) {
		return Context.putScopedContextual(KEY, msg);
	}
}
