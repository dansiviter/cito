/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.internal.Messages.messages;
import static cito.internal.Context.context;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.websocket.Session;

import org.tomitribe.microscoped.core.ScopeContext;

import cito.annotation.WebSocketScope;
import cito.internal.Context;
import cito.internal.Log;
import cito.internal.QuietClosable;

/**
 * Holds a WebSocket {@link Session} within the CDI runtime.
 *
 * @author Daniel Siviter
 * @since v1.0 [17 Aug 2016]
 */
@ApplicationScoped
public class WebSocketSessionProvider {
	@Inject
	private Log log;
	@Inject
	private BeanManager beanManager;

	/**
	 *
	 * @param session the session to set on the thread local context.
	 * @return the closable to reset the instance.
	 */
	public QuietClosable set(Session session) {
		this.log.debugf("Setting session. [%s]", session.getId());
		final ScopeContext<Session> scopeCtx = webSocketContext(this.beanManager);
		final Session previous = scopeCtx.enter(session);
		final Context context = context().get();
		if (context.get(Session.class).isPresent()) {
			throw messages().sessionAlreadySet(context.get(Session.class).get().getId(), session.getId());
		}
		context.put(Session.class, session);
		return () -> {
			context.remove(Session.class);
			scopeCtx.exit(previous);
		};
	}

	/**
	 * @param context the current context.
	 * @return the current WebSocket Session.
	 */
	@Produces @WebSocketScope
	public Session get(Context context) {
		return context.get(Session.class).get();
	}

	/**
	 * @param beanManager the bean manager instance.
	 * @return the WebSocket {@link ScopeContext}.
	 */
	@SuppressWarnings("unchecked")
	public static ScopeContext<Session> webSocketContext(BeanManager beanManager) {
		return (ScopeContext<Session>) beanManager.getContext(WebSocketScope.class);
	}
}
