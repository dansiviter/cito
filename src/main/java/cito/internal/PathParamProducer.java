/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.internal.PathParser.pathParser;

import javax.annotation.Nonnull;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import cito.annotation.PathParam;
import cito.event.DestinationChanged;
import cito.event.Message;
import cito.internal.Context.Scope;
import cito.internal.PathParser.Result;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [23 Nov 2016]
 */
@ApplicationScoped
public class PathParamProducer {
	/**
	 * @param context the context instance.
	 * @return the current parser value within the thread local context.
	 */
	@Produces
	@Dependent
	public static PathParser get(Context context) {
		return context.get(PathParser.class).orElse(null);
	}

	/**
	 *
	 * @param path the path to set as thread local context value.
	 * @return a closable to reset the value.
	 */
	public static Scope set(@Nonnull String path) {
		return set(pathParser(path));
	}

	/**
	 *
	 * @param path the path to set as thread local context value.
	 * @return a closable to reset the value.
	 */
	public static Scope set(@Nonnull PathParser path) {
		return Context.putScopedContextual(PathParser.class, path);
	}

	/**
	 * Returns the path param value.
	 *
	 * @param ip     the injection point.
	 * @param parser the parser instances.
	 * @param msg    the message. If this is {@code null} then a destination changed
	 *               instance must exist.
	 * @param dc     the destination changed event.
	 * @return the param value or {@code null} if not found.
	 */
	@Produces
	@Dependent
	@PathParam("nonbinding")
	public static String getValue(
			@Nonnull InjectionPoint ip,
			@Nonnull PathParser parser,
			Message msg,
			DestinationChanged dc)
	{
		final String destination;
		if (msg != null) {
			destination = msg.destination().get();
		} else if (dc != null) {
			destination = dc.getDestination();
		} else {
			throw new IllegalStateException("Neither MessageEvent or DestinationEvent is resolvable!");
		}

		final PathParam param = ip.getAnnotated().getAnnotation(PathParam.class);
		final Result r = parser.parse(destination);
		if (r.isSuccess()) {
			return r.get(param.value());
		}
		return null;
	}
}
