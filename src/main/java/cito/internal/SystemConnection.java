/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.jms.JMSContext;
import javax.jms.JMSException;

import cito.event.Message;
import cito.internal.stomp.Session;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [2 Aug 2016]
 */
@ApplicationScoped
public class SystemConnection extends AbstractConnection {
	public static final String SESSION_ID = "$Y$T3M";

	private Session session;

	@Override
	public String getSessionId() {
		return SESSION_ID;
	}

	/**
	 *
	 */
	@PostConstruct
	public void init() {
		try {
			createDelegate(null, null);
		} catch (JMSException e) {
			throw new IllegalStateException("Unable to create system connection!", e);
		}
	}

	/**
	 *
	 * @return
	 * @throws JMSException
	 */
	private Session getSession() throws JMSException {
		if (this.session == null) {
			this.session = this.factory.toSession(this, JMSContext.AUTO_ACKNOWLEDGE);
		}
		return this.session;
	}

	@Override
	public void on(Message msg) {
		final String sessionId = msg.sessionId().orElse(null);
		if (!getSessionId().equals(sessionId) && sessionId != null) {
			throw new IllegalArgumentException("Session identifier mismatch! [expected=" + getSessionId() + " OR null,actual=" + msg.sessionId() + "]");
		}

		this.log.debugf("Message event. [%s]", sessionId);

		try {
			getSession().sendToBroker(msg);
		} catch (JMSException e) {
			this.log.errorf("Error handling message! [sessionId=%s]", getSessionId(), e);
		}
	}
}
