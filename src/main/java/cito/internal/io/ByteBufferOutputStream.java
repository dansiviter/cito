/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.io;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import javax.annotation.Nonnull;

/**
 * Simple wrapper to use a {@link ByteBuffer} as an {@link OutputStream}.
 *
 * @author Daniel Siviter
 * @since v1.0 [24 Feb 2018]
 */
public class ByteBufferOutputStream extends OutputStream {
	private final ByteBuffer buf;

	public ByteBufferOutputStream(@Nonnull ByteBuffer buf) {
		this.buf = buf;
	}

	@Override
	public void write(int b) throws IOException {
		this.buf.put((byte)b);
	}
}
