/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.annotation.Nonnull;
import javax.enterprise.util.AnnotationLiteral;
import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;

import cito.annotation.OnSubscribe;

/**
 * {@link Repeatable} for {@link OnSubscribe}.
 *
 * @author Daniel Siviter
 * @since v1.0 [23 Nov 2019]
 */
@Qualifier
@Target({ FIELD, PARAMETER, METHOD, TYPE })
@Retention(RUNTIME)
@Repeatable(Subprotocols.class)
public @interface Subprotocol {
	@Nonbinding
	String value() default "**";

	// --- Inner Classes ---

	/**
	 * Literal for {@link Subprotocol}.
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [23 Nov 2019]
	 */
	@SuppressWarnings("all")
	public static final class Literal extends AnnotationLiteral<Subprotocol> implements Subprotocol {
		private static final long serialVersionUID = 1L;

		private final String value;

		private Literal(String value) {
			this.value = value;
		}

		@Override
		public String value() {
			return this.value;
		}

		public static Subprotocol subprotocol(@Nonnull String value) {
			return new Literal(value);
		}
	}
}
