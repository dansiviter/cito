/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.Nonnull;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [13 Nov 2019]
 */
@ApplicationScoped
public class ConnectionRegistry {
	@Inject
	private Log log;

	private final ConcurrentMap<String, Connection> connections = new ConcurrentHashMap<>();

	/**
	 * @param connection the session instance to register.
	 */
	public void register(@Nonnull Connection connection) {
		final String sessionId = connection.getSessionId();
		this.log.debugf("Registering connection. [%s]", sessionId);
		final Connection oldConnection = this.connections.put(sessionId, connection);
		if (oldConnection != null) {
			throw new IllegalArgumentException("Session already registered! [" + sessionId + "]");
		}
	}

	/**
	 * @param connection the session instance to unregister.
	 */
	public void unregister(@Nonnull Connection connection) {
		final String sessionId = connection.getSessionId();
		this.log.debugf("Un-registering session. [%s]", sessionId);
		final Connection oldConnection = this.connections.remove(sessionId);
		if (oldConnection == null) {
			throw new IllegalArgumentException("Session not registered! [" + sessionId + "]");
		}
	}

	/**
	 * @param id the session identifier.
	 * @return the session.
	 */
	public Optional<? extends Connection> get(@Nonnull String id) {
		return Optional.ofNullable(this.connections.get(id));
	}

	public SystemConnection systemConnection() {
		return (SystemConnection) get(SystemConnection.SESSION_ID)
				.orElseThrow(() -> new IllegalStateException("Unable to find system connection!"));
	}
}
