/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import javax.annotation.Nonnull;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;

import cito.event.DestinationChanged;
import cito.internal.Context.Scope;

/**
 * Acts as both a interface to detect if {@link DestinationChanged}s will be produced by the broker and to hold/produce
 * {@link DestinationChanged} within the CDI runtime.
 *
 * @author Daniel Siviter
 * @since v1.0 [25 Jan 2017]
 */
@ApplicationScoped
public class DestinationChangedProducer {
	@Produces @Dependent
	public static DestinationChanged get(Context context) {
		return context.get(DestinationChanged.class).orElse(null);
	}

	/**
	 * @param e then event hold in the context.
	 * @return a closable which will clean up the context.
	 */
	public static Scope set(@Nonnull DestinationChanged e) {
		return Context.putScopedContextual(DestinationChanged.class, e);
	}
}
