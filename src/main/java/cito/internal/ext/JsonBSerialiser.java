/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.ext;

import static cito.MimeTypes.APPLICATION_JSON;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;

import javax.activation.MimeType;
import javax.annotation.PreDestroy;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import cito.annotation.WebSocketScope;
import cito.ext.BodyReader;
import cito.ext.BodyWriter;
import cito.internal.Log;

/**
 * {@link BodyWriter} and {@link BodyReader} for {@code application/json} type using Json-B.
 *
 * @author Daniel Siviter
 * @since v1.0 [1 May 2017]
 */
@WebSocketScope
public class JsonBSerialiser implements BodyWriter<Object>, BodyReader<Object> {
	@Inject
	private Log log;
	@Inject
	private Instance<Jsonb> jsonbInstance;

	private volatile Jsonb jsonb;

	@Override
	public boolean isReadable(Type type, MimeType mediaType) {
		return mediaType.match(APPLICATION_JSON);
	}

	@Override
	public Object readFrom(Type type, MimeType mediaType, InputStream is) throws IOException {
		return getJsonb().fromJson(is, type);
	}

	@Override
	public boolean isWriteable(Type type, MimeType mediaType) {
		return mediaType.match(APPLICATION_JSON);
	}

	@Override
	public void writeTo(Object t, Type type, MimeType mediaType, OutputStream os) throws IOException {
		getJsonb().toJson(t, type, os);
	}

	/**
	 * @return returns the JSONB instance.
	 */
	private Jsonb getJsonb() {
		if (this.jsonbInstance.isUnsatisfied()) {
			if (this.jsonb == null) {
				this.jsonb = JsonbBuilder.create();
			}
			return this.jsonb;
		}
		return this.jsonbInstance.get();
	}

	@PreDestroy
	public void destroy() {
		if (this.jsonb != null) {
			try {
				this.jsonb.close();
			} catch (Exception e) {
				this.log.warn("Unable to close Jsonb!", e);
			}
		}
	}
}
