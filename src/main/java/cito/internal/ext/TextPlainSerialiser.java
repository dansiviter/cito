/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal.ext;

import static cito.ImmutableMimeType.charset;
import static cito.MimeTypes.TEXT_PLAIN;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

import javax.activation.MimeType;

import cito.annotation.WebSocketScope;
import cito.ext.BodyReader;
import cito.ext.BodyWriter;

/**
 * {@link BodyWriter} and {@link BodyReader} for {@code text/plain} type.
 *
 * @author Daniel Siviter
 * @since v1.0 [25 Apr 2017]
 */
@WebSocketScope
public class TextPlainSerialiser implements BodyWriter<Object>, BodyReader<Object> {
	@Override
	public boolean isReadable(Type type, MimeType mimeType) {
		return mimeType.match(TEXT_PLAIN) && (type == String.class
				|| (type instanceof Class<?> && Reader.class.isAssignableFrom((Class<?>) type)));
	}

	@Override
	public Object readFrom(Type type, MimeType mimeType, InputStream is) throws IOException {
		if (type == String.class) {
			final Charset charset = charset(mimeType);
			final StringBuilder buf = new StringBuilder();
			try (Reader reader = new InputStreamReader(is, charset)) {
				int read;
				final char[] cbuf = new char[16 * 1_024];
				while ((read = reader.read(cbuf)) != -1) {
					buf.append(cbuf, 0, read);
				}
				return buf.toString();
			}
		} else if (type instanceof Class<?> && Reader.class.isAssignableFrom((Class<?>) type)) {
			return new InputStreamReader(is);
		}
		throw new IllegalArgumentException("Unsupported type!");
	}

	@Override
	public boolean isWriteable(Type type, MimeType mimeType) {
		return mimeType.match(TEXT_PLAIN) && (
				type == String.class ||
				(type instanceof Class<?> && Reader.class.isAssignableFrom((Class<?>) type)));
	}

	@Override
	public void writeTo(Object t, Type type, MimeType mimeType, OutputStream os)
			throws IOException
	{
		if (type == String.class) {
			os.write(charset(mimeType).encode(t.toString()).array());
		} else if (type instanceof Class<?> && Reader.class.isAssignableFrom((Class<?>) type)) {
			final Reader reader = (Reader) t;
			int c;
			while ((c = reader.read()) != -1) {
				os.write(c);
			}
		} else {
			throw new IllegalArgumentException("Unable to write!");
		}
	}
}
