/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.MimeTypes.TEXT_PLAIN;
import static cito.internal.stomp.Header.Standard.CONTENT_LENGTH;
import static cito.internal.stomp.Header.Standard.CONTENT_TYPE;
import static cito.internal.stomp.Header.Standard.DESTINATION;
import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Supplier;

import javax.activation.MimeType;
import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.websocket.Session;

import cito.annotation.WebSocketScope;
import cito.event.Message;
import cito.event.Message.Type;
import cito.ext.Serialiser;
import cito.internal.io.ByteBufferOutputStream;
import cito.internal.stomp.Header;
import cito.internal.stomp.StompMessage;
import cito.internal.stomp.StompVersion;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [28 Nov 2019]
 */
@WebSocketScope
public class MessageBuilderFactory {
	private final Supplier<ByteBuffer> bufferSupplier = () -> ByteBuffer.allocate(4 * 1_024);

	@Inject
	private Session session;
	@Inject
	private Serialiser serialiser;

	/**
	 * @return a new builder based on the websocket negotiated sub-protocol.
	 */
	public MessageBuilder<?> builder() {
		return builder(session.getNegotiatedSubprotocol());
	}

	/**
	 *
	 * @param subprotocol the websocket sub-protocol.
	 * @return a new builder for the sub-protocol.
	 */
	public MessageBuilder<?> builder(@Nonnull String subprotocol) {
		switch (requireNonNull(subprotocol, "Subprotocol expected!")) {
		case StompVersion.V1_0_SUBPROTOCOL:
		case StompVersion.V1_1_SUBPROTOCOL:
		case StompVersion.V1_2_SUBPROTOCOL:
			return StompMessage.builder(this, subprotocol);
		default:
			throw new IllegalStateException("Unknown subprotocol! [" + subprotocol + "]");
		}
	}

	/**
	 * Serialises the payload.
	 *
	 * @param obj         the object to serialise.
	 * @param contentType the type to serialise from.
	 * @return the object serialised to a {@link ByteBuffer}.
	 * @throws IOException if unable to serialise the payload.
	 */
	private ByteBuffer toByteBuffer(@Nonnull Object obj, MimeType contentType) throws IOException {
		if (obj instanceof ByteBuffer) {
			return (ByteBuffer) obj;
		}
		final ByteBuffer buf = this.bufferSupplier.get();
		serialise(obj, contentType, buf);
		buf.flip();
		return buf;
	}

	/**
	 * Serialises the payload.
	 *
	 * @param obj         the object to serialise.
	 * @param contentType the type to serialise from.
	 * @param buf         the buffer to serialise to.
	 * @throws IOException if unable to serialise the payload.
	 */
	private void serialise(@Nonnull Object obj, MimeType contentType, ByteBuffer buf) throws IOException {
		try (OutputStream os = new ByteBufferOutputStream(buf)) {
			this.serialiser.writeTo(obj, obj.getClass(), contentType != null ? contentType : TEXT_PLAIN, os);
		}
	}

	// --- Inner Classes ---

	/**
	 *
	 */
	public abstract static class MessageBuilder<M extends Message> {
		protected final Map<Header, List<String>> headers = new LinkedHashMap<>();
		protected final MessageBuilderFactory factory;
		protected final String subprotocol;

		protected Optional<Type> type = Optional.empty();
		protected Optional<String> sessionId = Optional.empty();
		protected Optional<ByteBuffer> body = Optional.empty();

		protected MessageBuilder(@Nonnull MessageBuilderFactory factory, @Nonnull String subprotocol) {
			this.factory = requireNonNull(factory, "No factory!");
			this.subprotocol = requireNonNull(subprotocol, "No subprotocol!");
		}

		/**
		 * @param type the type of message.
		 * @return this builder instance.
		 */
		public MessageBuilder<M> type(Type type) {
			this.type = Optional.of(type);
			return this;
		}

		/**
		 *
		 * @param sessionId the websocket session identifier.
		 * @return this builder instance.
		 */
		public MessageBuilder<M> sessionId(@Nonnull String sessionId) {
			this.sessionId = Optional.of(sessionId);
			return this;
		}

		/**
		 *
		 * @param buf the raw bytes.
		 * @return this builder instance.
		 * @throws IOException if unable to
		 */
		public abstract MessageBuilder<M> raw(@Nonnull ByteBuffer buf) throws IOException;

		/**
		 *
		 * @param body        the body object.
		 * @param contentType the content type or {@code null}.
		 * @return this builder instance.
		 * @throws IOException if unable to serialse the body.
		 */
		public MessageBuilder<M> body(@Nonnull Object body, MimeType contentType) throws IOException {
			try {
				return body(factory.toByteBuffer(body, contentType));
			} finally {
				if (contentType != null) {
					header(CONTENT_TYPE, contentType.toString());
				}
			}
		}

		/**
		 *
		 * @param body the body as bytes.
		 * @return this builder instance.
		 */
		public MessageBuilder<M> body(@Nonnull ByteBuffer body) {
			this.body = Optional.of(body);
			header(CONTENT_LENGTH, Integer.toString(body.limit()));
			return this;
		}

		/**
		 * @param header the header to add.
		 * @param values the values for the header.
		 * @return this builder instance.
		 */
		public MessageBuilder<M> header(@Nonnull Header header, @Nonnull String... values) {
			return header(header, Arrays.asList(values));
		}

		/**
		 * @param header the header to add.
		 * @param values the values for the header.
		 * @return this builder instance.
		 */
		public MessageBuilder<M> header(@Nonnull Header header, @Nonnull List<CharSequence> values) {
			if (values.isEmpty()) {
				throw new IllegalArgumentException("'values' cannot be null or empty!");
			}

			final StringJoiner joiner = new StringJoiner(",");
			for (CharSequence v : values) {
				joiner.add(v);
			}

			final List<String> valueList = this.headers.compute(header, (k, v) -> {
				return v == null ? new ArrayList<>() : v;
			});
			valueList.add(joiner.toString());
			return this;
		}

		/**
		 * @param headers the headers to copy.
		 * @return this builder instance
		 */
		public MessageBuilder<M> headers(@Nonnull Map<Header, String> headers) {
			headers.forEach(this::header);
			return this;
		}

		/**
		 * @param destination the destination.
		 * @return this builder instance
		 */
		public MessageBuilder<M> destination(@Nonnull String destination) {
			return header(DESTINATION, destination);
		}

		/**
		 *
		 * @param cause the message that caused the error.
		 * @return this builder instance
		 */
		public abstract MessageBuilder<M> error(Optional<Message> cause);

		/**
		 * @return the new message instance.
		 */
		public abstract M build();
	}
}
