/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.internal.Messages.messages;
import static java.util.Collections.singletonMap;

import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.websocket.CloseReason;

import cito.internal.stomp.ErrorHandler;
import cito.internal.stomp.Factory;

/**
 *
 * @author Daniel Siviter
 * @since v1.0 [2 Aug 2016]
 */
public abstract class AbstractConnection implements cito.internal.Connection {
	@Inject
	protected Log log;
	@Inject
	protected BeanManager beanManager;
	@Inject
	protected ConnectionFactory connectionFactory;
	@Inject
	protected ErrorHandler errorHandler;
	@Inject
	protected Factory factory;

	protected JMSContext delegate;

	/**
	 * Creates a JMS connection to delegate to.
	 *
	 * @param login the login.
	 * @param password the password.
	 * @throws JMSException thrown if there is a JMS issue.
	 */
	protected void createDelegate(String login, String password) throws JMSException {
		if (this.delegate != null) {
			throw messages().alreadyConnected();
		}
		if (login != null) {
			this.delegate = this.connectionFactory.createContext(login, password);
		} else {
			this.delegate = this.connectionFactory.createContext();
		}
		final String sessionId = getSessionId();
		this.log.infof("Starting JMS connection... [%s]", sessionId);
		this.delegate.setClientID(sessionId);
		this.delegate.start();
	}

	public JMSContext getDelegate() {
		return delegate;
	}

	@Override
	public void close(CloseReason reason) {
		this.log.infof("Closing connection. [sessionId=%s,code=%s,reason=%s]",
				getSessionId(), reason.getCloseCode().getCode(), reason.getReasonPhrase());
		if (this.delegate != null) {
			this.delegate.close();
		}
	}

	@Override
	public String toString() {
		return super.toString() + singletonMap("sessionId", getSessionId());
	}
}
