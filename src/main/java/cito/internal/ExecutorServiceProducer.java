/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static java.lang.Runtime.getRuntime;
import static java.util.concurrent.Executors.newScheduledThreadPool;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 */
@ApplicationScoped
public class ExecutorServiceProducer {
	private static final String SCHEDULED_EXECUTOR = "java:comp/DefaultManagedScheduledExecutorService";
	@Inject
	private Log log;

	private boolean shutdownScheduledExecutor;

	@Produces
	@ApplicationScoped
	public ScheduledExecutorService scheduledExecutorService() {
		return Context.wrap(jndi(ScheduledExecutorService.class, SCHEDULED_EXECUTOR).orElseGet(() -> {
			try {
				return newScheduledThreadPool(getRuntime().availableProcessors());
			} finally {
				this.shutdownScheduledExecutor = true;
			}
		}));
	}

	private <E extends ExecutorService> Optional<E> jndi(Class<E> cls, String name) {
		try {
			return Optional.of(cls.cast(new InitialContext().lookup(name)));
		} catch (NamingException e) {
			this.log.infof("Unable to extract. Constructing own. [%s]", name);
		}
		return Optional.empty();
	}

	public void destroy(@Disposes ScheduledExecutorService executor) {
		if (this.shutdownScheduledExecutor) {
			Util.shutdownAndWait(executor, 5, TimeUnit.SECONDS);
		}
	}
}
