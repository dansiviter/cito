/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.internal.LogProducer.log;
import static java.util.Objects.requireNonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * General utility methods. Yea, I know someone will think this is an anti
 * pattern, but meh!
 *
 * @author Daniel Siviter
 * @since v1.0 [9 Aug 2016]
 */
public enum Util { ;
	private static final Log LOG = log(Util.class);

	/**
	 * @param <T> the type required.
	 * @param a   the input array. If {@code null} then the returned value will be
	 *            {@code null}.
	 * @return the first value within the input array.
	 */
	@SafeVarargs
	public static <T> T getFirst(T... a) {
		return a == null || a.length == 0 ? null : a[0];
	}

	/**
	 * @param <T> the type required.
	 * @param c   the input collection. If {@code null} then the returned value will
	 *            be {@code null}.
	 * @return the first value within the input collection.
	 */
	public static <T> T getFirst(Collection<T> c) {
		return c == null || c.isEmpty() ? null : c.iterator().next();
	}

	/**
	 * @param s character sequence to check.
	 * @return {@code true} if the given sequence is NOT {@code null} and is the
	 *         empty string.
	 */
	public static boolean isEmpty(CharSequence s) {
		return s != null && s.length() == 0;
	}

	/**
	 * @param s character sequence to check.
	 * @return {@code true} if the given sequence is {@code null} or is the empty
	 *         string.
	 */
	public static boolean isNullOrEmpty(CharSequence s) {
		return s == null || s.length() == 0;
	}

	/**
	 * @param <C> the collection type.
	 * @param c   the input collection to test.
	 * @return the input collection.
	 * @throws IllegalArgumentException if the collection is empty.
	 */
	public static <C extends Collection<?>> C requireNonEmpty(C c) {
		if (requireNonNull(c).isEmpty()) {
			throw new IllegalArgumentException("Collection is empty!");
		}
		return c;
	}

	/**
	 * @param <A>         the annotation type wanted.
	 * @param type        the annotation type wanted.
	 * @param annotations the collection of annotations to check.
	 * @return an array of the found annotation instances.
	 */
	@SuppressWarnings("unchecked")
	public static <A extends Annotation> A[] getAnnotations(Class<A> type,
			Collection<? extends Annotation> annotations) {
		final Collection<A> found = new ArrayList<>();
		for (Annotation a : annotations) {
			if (type.isAssignableFrom(type)) {
				found.add(type.cast(a));
			}
		}
		return found.toArray((A[]) Array.newInstance(type, 0));
	}

	/**
	 * Utility using the JavaDoc recommended pattern to shutdown an executor.
	 *
	 * @param executor the executor to shutdown.
	 * @param timeout the timeout.
	 * @param unit the timeout unit.
	 */
	public static void shutdownAndWait(ExecutorService executor, long timeout, TimeUnit unit) {
		if (executor.isTerminated()) {
			return;
		}
		executor.shutdown();
		try {
			if (!executor.awaitTermination(timeout, unit)) {
				executor.shutdownNow(); // Cancel currently executing tasks
				if (!executor.awaitTermination(timeout, unit)) {
					LOG.warnf("Executor did not terminate cleanly! [timeout=%d,unit=%s]", timeout, unit);
				}
			}
		} catch (InterruptedException ie) {
			executor.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}
}
