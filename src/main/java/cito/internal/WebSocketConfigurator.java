/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static javax.websocket.server.HandshakeRequest.SEC_WEBSOCKET_PROTOCOL;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import javax.websocket.server.ServerEndpointConfig.Configurator;

import cito.security.SecurityContext;

/**
 * Links the WebSocket endpoint to the security mechanism.
 *
 * @author Daniel Siviter
 * @since v1.0 [29 Sep 2016]
 */
public class WebSocketConfigurator extends Configurator {
	private static final ThreadLocal<SecurityContext> SECURITY_CONTEXT = new ThreadLocal<SecurityContext>();
	/** Set of supported protocols in order of preference */
	private static final String[] SUPPORTED_SUBPROTOCOLS = { "v12.stomp", "v11.stomp", "v10.stomp" };

	@Override
	public void modifyHandshake(ServerEndpointConfig sec, HandshakeRequest request, HandshakeResponse response) {
		setSubProtocol(request, response);
		SECURITY_CONTEXT.set(new SecurityContext(request));
	}

	public static SecurityContext removeSecurityContext() {
		try {
			return SECURITY_CONTEXT.get();
		} finally {
			SECURITY_CONTEXT.remove();
		}
	}

	/**
	 * The underlying WebSocket implementation may not select the correct protocol as it could be in any order. We
	 * ensure we order the values based on newest.
	 *
	 * @param req
	 * @param res
	 */
	private static void setSubProtocol(@Nonnull HandshakeRequest req, @Nonnull HandshakeResponse res) {
		final String subProtocols = getSubProtocol(req);
		if (subProtocols != null) {
			res.getHeaders().put(SEC_WEBSOCKET_PROTOCOL, asList(subProtocols));
		}
	}

	/**
	 * Returns the newest supported protocol.
	 *
	 * @param req the input request.
	 * @return the newest supported sub-protocol, or {@code null}.
	 */
	private static String getSubProtocol(@Nonnull HandshakeRequest req) {
		final Set<String> subProtocols = splitSubProtocols(req);
		for (String subProtocol : SUPPORTED_SUBPROTOCOLS) {
			if (subProtocols.contains(subProtocol)) {
				return subProtocol;
			}
		}
		return null;
	}

	/**
	 * Extracts the first {@code Sec-WebSocket-Protocol} value and splits into a {@link Set}.
	 *
	 * @param req the input request.
	 * @return the set of sub-protocols.
	 */
	private static Set<String> splitSubProtocols(@Nonnull HandshakeRequest req) {
		final List<String> subprotocols = req.getHeaders().getOrDefault(SEC_WEBSOCKET_PROTOCOL, emptyList());
		return !subprotocols.isEmpty() ? new HashSet<>(asList(subprotocols.get(0).split(", "))) : emptySet();
	}
}
