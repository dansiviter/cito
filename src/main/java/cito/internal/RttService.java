/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.internal.Util.shutdownAndWait;
import static java.lang.Long.BYTES;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.concurrent.ScheduledExecutorService;

import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.websocket.PongMessage;
import javax.websocket.Session;

import cito.internal.Log;

/**
 * Uses WebSocket ping/pong to calculate Round Trip Time (RTT).
 *
 * @author Daniel Siviter
 * @since v1.0 [30 Jan 2018]
 *
 */
@ApplicationScoped
public class RttService {
	private static final Duration RTT_DELAY = Duration.ofMinutes(1);

	@Inject
	private Log log;
	@Inject
	private ScheduledExecutorService executor;

	/**
	 * @param session the session to start for.
	 */
	public void start(Session session) {
		this.executor.submit(() -> ping(session));
	}

	/**
	 * @param session the session to pinged.
	 */
	private void ping(Session session) {
		if (!session.isOpen()) {
			return;
		}
		try {
			final long now = System.currentTimeMillis();
			final ByteBuffer buf = (ByteBuffer) ByteBuffer.allocate(BYTES).putLong(now).flip();
			session.getBasicRemote().sendPing(buf);
			this.log.pingSent(session.getId());
		} catch (IOException e) {
			log.warn("Problems with ping/pong!", e);
		}
	}

	/**
	 * @param session the session that ponged.
	 * @param msg     the pong message.
	 */
	public void pong(Session session, PongMessage msg) {
		final long now = System.currentTimeMillis();
		final ByteBuffer buf = msg.getApplicationData();
		final long rtt = now - buf.getLong();
		this.log.pongReceived(rtt, session.getId());
		session.getUserProperties().put("RTT", rtt);
		this.executor.schedule(() -> ping(session), RTT_DELAY.getSeconds(), SECONDS);
	}

	@PreDestroy
	public void destroy() {
		shutdownAndWait(this.executor, 1, SECONDS);
	}
}
