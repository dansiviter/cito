/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.MimeTypes.TEXT_PLAIN;
import static cito.internal.Context.scope;
import static cito.internal.WebSocketSessionProvider.webSocketContext;
import static cito.internal.annotation.Subprotocol.Literal.subprotocol;
import static java.lang.String.format;
import static javax.enterprise.event.NotificationOptions.ofExecutor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;

import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler.Whole;
import javax.websocket.PongMessage;
import javax.websocket.Session;

import org.apache.commons.lang3.RandomStringUtils;

import cito.event.Message;
import cito.internal.Context.Scope;
import cito.internal.stomp.Encoding;
import cito.internal.stomp.Frame;
import cito.security.SecurityContext;

/**
 * Cito websocket endpoint. This generally initialised using the {@link javax.websocket.server.ServerContainer}:
 * <pre>
 *   ServerContainer container = ...
 *   container.addEndpoint(CitoEndpoint.config("/cito"));
 * </pre>
 * In a servlet container this can be done from a {@code ServletContextListener}:
 * <pre>
 * 	 &#064;WebListener
 * 	 public class MyListener implements ServletContextListener {
 *     public void contextInitialized(ServletContextEvent servletContextEvent) {
 *  	 ServletContext ctx = servletContextEvent.getServletContext();
 *       ServerContainer container (ServerContainer) ctx.getAttribute("javax.websocket.server.ServerContainer");
 *       container.addEndpoint(CitoEndpoint.config("/cito"));
 *     }
 *   }
 * </pre>
 *
 * @author Daniel Siviter
 * @since v1.0 [15 Jul 2016]
 */
public class CitoEndpoint extends Endpoint {
	private final Context context = Context.create();

	@Inject
	protected Log log;
	@Inject
	private BeanManager beanManager;
	@Inject
	private WebSocketSessionProvider webSocketProvider;
	@Inject @Any
	private Instance<Connection> connectionInstance;
	@Inject
	private Event<Message> messageEvent;
	@Inject
	private ExecutorService executor;
	@Inject
	private RttService rttService;
	@Inject
	private MessageBuilderFactory messageBuilderFactory;

	private final SecurityContext securityCtx;
	private Connection connection;

	public CitoEndpoint() {
		this.securityCtx = WebSocketConfigurator.removeSecurityContext();
	}

	@Override
	public void onOpen(Session session, EndpointConfig config) {
		this.log.infof("WebSocket connection opened. [id=%s,principal=%s]", session.getId(),
				session.getUserPrincipal());
		try (Scope scope = scope(this.context)) {
			SecurityContextProducer.set(session, this.securityCtx);
			try (QuietClosable c = this.webSocketProvider.set(session)) {
				this.connection = this.connectionInstance.select(subprotocol(session.getNegotiatedSubprotocol())).get();
				this.connection.open(session, config);
				session.addMessageHandler(ByteBuffer.class, new Whole<ByteBuffer>() {
					@Override
					public void onMessage(ByteBuffer message) {
						on(session, message);
					}
				});
				session.addMessageHandler(PongMessage.class, new Whole<PongMessage>() {
					@Override
					public void onMessage(PongMessage message) {
						pong(session, message);
					}
				});
				this.rttService.start(session);
			}
		}
	}

	public void pong(Session session, PongMessage msg) {
		this.log.debugf("Received pong from client. [id=%s,principal=%s]", session.getId(), session.getUserPrincipal());
		try (Scope scope = scope(this.context)) {
			try (QuietClosable c = this.webSocketProvider.set(session)) {
				this.rttService.pong(session, msg);
			}
		}
	}

	public void on(Session session, ByteBuffer buf) {
		final String sessionId = session.getId();
		this.log.debugf("Received message from client. [id=%s,principal=%s]", sessionId, session.getUserPrincipal());
		try (Scope scope = scope(this.context)) {
			try (QuietClosable c = this.webSocketProvider.set(session)) {
				final Message msg = this.messageBuilderFactory.builder().raw(buf).sessionId(sessionId).build();
				try (Scope msgScope = ClientMessageProducer.set(msg)) {
					this.connection.fromClient(msg);
					this.messageEvent.fire(msg);
					this.messageEvent.fireAsync(msg, ofExecutor(this.executor));
				}
			} catch (IOException e) {
				this.log.errorf(e, "Unable to decode frame! [id=%s,principal=%s]", session.getId(), session.getUserPrincipal());
			}
		}
	}

	@Override
	public void onError(Session session, Throwable cause) {
		final String errorId = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
		this.log.warnf(cause, "WebSocket error. [id=%s,principal=%s,errorId=%s]", session.getId(), session.getUserPrincipal(), errorId);
		try (Scope scope = scope(this.context)) {
			try (QuietClosable c = this.webSocketProvider.set(session)) {
				final Frame errorFrame = Frame.error().body(TEXT_PLAIN, format("%s [errorId=%s]", cause.getMessage(), errorId)).build();
				session.getBasicRemote().sendBinary(Encoding.from(errorFrame, false, 64 * 1024));
			} catch (IOException e) {
				this.log.errorf(e, "Unable to send error frame! [id=%s,principal=%s]", session.getId(), session.getUserPrincipal());
			}
			try {
				session.close(new CloseReason(CloseCodes.CLOSED_ABNORMALLY, format("See server log. [errorId=%s]", errorId)));
			} catch (IOException e) {
				this.log.errorf(e, "Unable to close session! [id=%s,principal=%s]", session.getId(), session.getUserPrincipal());
			}
		}
	}

	@Override
	public void onClose(Session session, CloseReason reason) {
		this.log.infof("WebSocket connection closed. [id=%s,principal=%s,code=%s,reason=%s]", session.getId(), session.getUserPrincipal(), reason.getCloseCode(), reason.getReasonPhrase());

		try (Scope scope = scope(this.context);
				QuietClosable c = this.webSocketProvider.set(session))
		{
			if (this.connection != null) {
				this.connection.close(reason);
			}
		} catch (IOException e) {
			this.log.warnf(e, "Error closing session! [%s]", session.getId());
		}
		webSocketContext(this.beanManager).destroy(session);
	}
}
