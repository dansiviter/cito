/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.internal;

import static cito.internal.Util.getAnnotations;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.function.Function;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.ObservesAsync;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.ObserverMethod;
import javax.inject.Inject;

import cito.DestinationType;
import cito.annotation.OnConnected;
import cito.annotation.OnDisconnect;
import cito.annotation.OnSend;
import cito.annotation.OnSubscribe;
import cito.annotation.OnUnsubscribe;
import cito.event.Message;
import cito.internal.Glob;
import cito.internal.MessageObserverExtension;
import cito.internal.PathParamProducer;
import cito.internal.Context.Scope;

/**
 * Fires off events related to destinations.
 *
 * @author Daniel Siviter
 * @since v1.0 [27 Jul 2016]
 */
@ApplicationScoped
public class EventProducer {
	private final Map<String, String> idDestinationMap = new WeakHashMap<>();

	@Inject
	private BeanManager manager;

	/**
	 * @param msg the message event to process.
	 */
	public void messageAsync(@ObservesAsync Message msg) {
		message(msg);
	}

	/**
	 * @param msg the message event to process.
	 */
	public void message(@Observes Message msg) {
		if (!msg.type().isPresent()) {
			return;
		}

		final MessageObserverExtension extension = this.manager.getExtension(MessageObserverExtension.class);

		switch (msg.type().get()) {
		case CONNECTED: { // on client thread as it's response to CONNECT
			extension.getMessageObservers(OnConnected.class).forEach(om -> om.notify(msg));
			break;
		}
		case SEND: {
			final String destination = msg.destination().get();
			final boolean consumed = notify(OnSend.class, extension.getMessageObservers(OnSend.class), destination,
					OnSend::value, msg);
			if (!consumed && DestinationType.from(destination) == DestinationType.DIRECT) {
				throw new IllegalStateException("Non-JMS destination not consumed! [destination=" + destination
						+ ",sessionId=" + msg.sessionId() + "]");
			}
			break;
		}
		case SUBSCRIBE: {
			final String id = msg.id().get();
			final String destination = msg.destination().get();
			idDestinationMap.put(id, destination);
			notify(OnSubscribe.class, extension.getMessageObservers(OnSubscribe.class), destination, OnSubscribe::value,
					msg);
			break;
		}
		case UNSUBSCRIBE: {
			final String id = msg.id().get();
			final String destination = this.idDestinationMap.remove(id);
			notify(OnUnsubscribe.class, extension.getMessageObservers(OnUnsubscribe.class), destination,
					OnUnsubscribe::value, msg);
			break;
		}
		case DISCONNECT: {
			extension.getMessageObservers(OnDisconnect.class).forEach(om -> om.notify(msg));
			break;
		}
		default:
			break;
		}
	}

	// --- Static Methods ---

	/**
	 *
	 * @param annotation      the annotation class.
	 * @param observerMethods the observers to notify.
	 * @param destination     the destination.
	 * @param msg             the message event.
	 * @return {@code true} if one or more notifiers consumed the event.
	 */
	private static <A extends Annotation> boolean notify(
			Class<A> annotation,
			Set<ObserverMethod<Message>> observerMethods,
			String destination,
			Function<A, String> extractor,
			Message msg)
	{
		boolean consumed = false;
		for (ObserverMethod<Message> om : observerMethods) {
			for (A a : getAnnotations(annotation, om.getObservedQualifiers())) {
				final String value = extractor.apply(a);
				if (!Glob.from(value).matches(destination)) {
					continue;
				}
				try (Scope scope = PathParamProducer.set(value)) {
					om.notify(msg);
					consumed = true;
				}
			}
		}
		return consumed;
	}
}
