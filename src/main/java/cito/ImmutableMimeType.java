/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito;

import static java.nio.charset.Charset.forName;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.nio.charset.Charset;
import java.util.Enumeration;

import javax.activation.MimeType;
import javax.activation.MimeTypeParameterList;
import javax.activation.MimeTypeParseException;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import java8.util.Objects;

/**
 *
 */
@Immutable
public class ImmutableMimeType extends MimeType {
	public static final String CHARSET_PARAMETER = "charset";
	public static final String WILDCARD = "*";

	private final ImmutableMimeTypeParameterList parameterList = new ImmutableMimeTypeParameterList();

	private ImmutableMimeType(String raw) throws MimeTypeParseException {
		super(raw);
	}

	@Override
	public void setParameter(String name, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MimeTypeParameterList getParameters() {
		return parameterList;
	}

	private MimeTypeParameterList getParametersDelegate() {
		return super.getParameters();
	}

	@Override
	public void setPrimaryType(String primary) {
		throw new UnsupportedOperationException("This is immutable!");
	}

	@Override
	public void setSubType(String sub) {
		throw new UnsupportedOperationException("This is immutable!");
	}

	/**
	 * @return a builder instance using this mime-type as a base.
	 */
	public Builder toBuilder() {
		return builder(this);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 31 * hash + (getPrimaryType() == null ? 0 : getPrimaryType().hashCode());
		hash = 31 * hash + (getSubType() == null ? 0 : getSubType().hashCode());
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof MimeType)) {
			return false;
		}
		final MimeType other = (MimeType) obj;
		return match(other) && equals(getParameters(), other.getParameters());
	}

	private static boolean equals(MimeTypeParameterList params0, MimeTypeParameterList params1) {
		final Enumeration<?> names = params0.getNames();
		while (names.hasMoreElements()) {
			final String name = names.nextElement().toString();
			if (!Objects.equals(params0.get(name), params1.get(name))) {
				return false;
			}
		}
		return true;
	}

	// --- Static Methods ---

	/**
	 * Creates an immutable {@link MimeType} builder from the input string.
	 *
	 * @param raw the input string.
	 * @return a new builder instance.
	 */
	public static Builder builder(@Nonnull CharSequence raw) {
		return new Builder(raw);
	}

	/**
	 * Creates an immutable {@link MimeType} builder from the primary and sub types.
	 *
	 * @param primary the primary type.
	 * @param sub     the sub-type.
	 * @return a new builder instance.
	 */
	public static Builder builder(@Nonnull String primary, @Nonnull String sub) {
		return new Builder(primary, sub);
	}

	/**
	 * Creates an immutable {@link MimeType} builder from the input MIME type.
	 *
	 * @param mimeType the input mime type.
	 * @return a new builder instance.
	 */
	public static Builder builder(@Nonnull MimeType mimeType) {
		return builder(mimeType.toString());
	}

	/**
	 * Creates an immutable {@link MimeType} from the input string.
	 *
	 * @param raw the input string.
	 * @return the created mime type.
	 */
	public static ImmutableMimeType mimeType(@Nonnull CharSequence raw) {
		return builder(raw).build();
	}

	/**
	 * Creates an immutable {@link MimeType} from the primary and sub types.
	 *
	 * @param primary the primary type.
	 * @param sub     the sub-type.
	 * @return the created mime type.
	 */
	public static ImmutableMimeType mimeType(@Nonnull String primary, @Nonnull String sub) {
		return builder(primary, sub).build();
	}

	/**
	 * Extracts the charset from the input {@link MimeType}.
	 *
	 * @param mimeType the input MIME type to check.
	 * @return the found charset or UTF-8 if not found.
	 */
	public static Charset charset(@Nonnull MimeType mimeType) {
		final String charset = mimeType.getParameter(CHARSET_PARAMETER);
		if (charset != null && !charset.isEmpty()) {
			return forName(mimeType.getParameters().get(CHARSET_PARAMETER));
		}
		return UTF_8;
	}


	// --- Inner Classes ---

	/**
	 *
	 */
	private class ImmutableMimeTypeParameterList extends MimeTypeParameterList {
		@Override
		public int size() {
			return getParametersDelegate().size();
		}

		@Override
		public boolean isEmpty() {
			return getParametersDelegate().isEmpty();
		}

		@Override
		public String get(String name) {
			return getParametersDelegate().get(name);
		}

		@Override
		public void set(String name, String value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void remove(String name) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Enumeration<?> getNames() {
			return getParametersDelegate().getNames();
		}
	}

	/**
	 *
	 */
	public static class Builder {
		private final MimeType delegate;

		Builder(CharSequence raw) {
			try {
				this.delegate = new MimeType(raw.toString());
			} catch (MimeTypeParseException e) {
				throw new IllegalArgumentException(e);
			}
		}

		Builder(String primary, String sub) {
			try {
				this.delegate = new MimeType(primary, sub);
			} catch (MimeTypeParseException e) {
				throw new IllegalArgumentException(e);
			}
		}

		public Builder param(String name, String value) {
			this.delegate.setParameter(name, value);
			return this;
		}

		public Builder charset(Charset charset) {
			return param(CHARSET_PARAMETER, charset.name());
		}

		public Builder charset(String charset) {
			return param(CHARSET_PARAMETER, charset);
		}

		public ImmutableMimeType build() {
			try {
				return new ImmutableMimeType(this.delegate.toString());
			} catch (MimeTypeParseException e) {
				throw new IllegalArgumentException(e);
			}
		}
	}
}
