/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito;

import static java.util.Collections.emptyMap;

import java.io.IOException;
import java.util.Map;

import javax.activation.MimeType;
import javax.annotation.Nonnull;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import cito.event.Message;
import cito.internal.ConnectionRegistry;
import cito.internal.Log;
import cito.internal.MessageBuilderFactory;
import cito.internal.stomp.Header;

/**
 * Server messaging support. This can be used in two ways: {@link Inject}ed or {@code extend} it.
 * <p>
 * To inject, use:
 * <pre>
 * 	&#064;Inject
 * 	private MessagingSupport support;
 * </pre>
 *
 * @author Daniel Siviter
 * @since v1.0 [27 Jul 2016]
 */
@Dependent
public class MessagingSupport {
	@Inject
	private Log log;
	@Inject
	private ConnectionRegistry registry;
	@Inject
	private MessageBuilderFactory messageBuilderFactory;

	/**
	 * Broadcast to all users and all sessions subscribed to the {@code destination}.
	 *
	 * @param destination the broadcast destination.
	 * @param payload the send payload.
	 * @throws IOException if unable to serialise the payload.
	 */
	public void broadcast(
			@Nonnull String destination,
			@Nonnull Object payload)
	throws IOException
	{
		broadcast(destination, payload, (MimeType) null);
	}

	/**
	 * Broadcast to all users and all sessions subscribed to the {@code destination}.
	 *
	 * @param destination the broadcast destination.
	 * @param payload the send payload.
	 * @param type the type to convert the data to. If {@code null} then {@code text/plain} will be used.
	 * @throws IOException if unable to serialise the payload.
	 */
	public void broadcast(
			@Nonnull String destination,
			@Nonnull Object payload,
			MimeType type)
	throws IOException
	{
		broadcast(destination, payload, type, emptyMap());
	}

	/**
	 * Broadcast to all users and all sessions subscribed to the {@code destination}.
	 *
	 * @param destination the broadcast destination.
	 * @param payload the send payload.
	 * @param headers the message headers.
	 * @throws IOException if unable to serialise the payload.
	 */
	public void broadcast(
			@Nonnull String destination,
			@Nonnull Object payload,
			@Nonnull Map<Header, String> headers)
	throws IOException
	{
		broadcast(destination, payload, null, headers);
	}

	/**
	 * Broadcast to all users and all sessions subscribed to the {@code destination}.
	 *
	 * @param destination the broadcast destination.
	 * @param payload the send payload.
	 * @param contentType the type to convert the data to. If {@code null} then {@code text/plain} will be used.
	 * @param headers the message headers.
	 * @throws IOException if unable to serialise the payload.
	 */
	public void broadcast(
			@Nonnull String destination,
			@Nonnull Object payload,
			MimeType contentType,
			@Nonnull Map<Header, String> headers)
	throws IOException
	{
		this.log.debugf("Broadcasting... [destination=%s]", destination);
		final Message message = this.messageBuilderFactory.builder()
				.body(payload, contentType)
				.destination(destination)
				.headers(headers)
				.build();
		this.registry.systemConnection().on(message);
	}

	/**
	 * Send to a specific user session.
	 *
	 * @param sessionId the user session identifier to send to.
	 * @param destination the send to destination.
	 * @param payload the send payload.
	 * @param type the type to convert the data to. If {@code null} then {@code text/plain} will be used.
	 * @throws IOException if unable to serialise the payload.
	 */
	public void sendTo(
			@Nonnull String sessionId,
			@Nonnull String destination,
			@Nonnull Object payload,
			MimeType type)
					throws IOException
	{
		sendTo(sessionId, destination, payload, type, emptyMap());
	}

	/**
	 * Send to a specific user session.
	 *
	 * @param sessionId the user session identifier to send to.
	 * @param destination the send to destination.
	 * @param payload the send payload.
	 * @throws IOException if unable to serialise the payload.
	 */
	public void sendTo(
			@Nonnull String sessionId,
			@Nonnull String destination,
			@Nonnull Object payload)
					throws IOException
	{
		sendTo(sessionId, destination, payload, null, emptyMap());
	}

	/**
	 * Send to a specific user session.
	 *
	 * @param sessionId the user session identifier to send to.
	 * @param destination the send to destination.
	 * @param payload the send payload.
	 * @param headers the message headers.
	 * @throws IOException if unable to serialise the payload.
	 */
	public void sendTo(
			@Nonnull String sessionId,
			@Nonnull String destination,
			@Nonnull Object payload,
			@Nonnull Map<Header, String> headers)
					throws IOException
	{
		sendTo(sessionId, destination, payload, null, headers);
	}

	/**
	 * Send to a specific user session.
	 *
	 * @param sessionId the user session identifier to send to.
	 * @param destination the send to destination.
	 * @param payload the send payload.
	 * @param type the type to convert the data to. If {@code null} then {@code text/plain} will be used.
	 * @param headers the message headers.
	 * @throws IOException if unable to serialise the payload.
	 */
	public void sendTo(
			@Nonnull String sessionId,
			@Nonnull String destination,
			@Nonnull Object payload,
			MimeType type,
			@Nonnull Map<Header, String> headers)
	throws IOException
	{
		this.log.debugf("Sending... [sessionId=%s,destination=%s]", sessionId, destination);
		final Message message = this.messageBuilderFactory.builder()
				.sessionId(sessionId)
				.destination(destination)
				.headers(headers)
				.build();
		this.registry.get(sessionId).ifPresent(c -> c.on(message));
	}
}
