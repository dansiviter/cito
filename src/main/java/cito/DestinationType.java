/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito;

import javax.annotation.Nonnull;

/**
 * Defines a destination type.
 * 
 * @author Daniel Siviter
 * @since v1.0 [3 Jan 2018]
 */
public enum DestinationType {
	/** JMS Topic destination */
	TOPIC,
	/** JMS Queue destination */
	QUEUE,
	/** Destination type that's only handled in the app layer **/
	DIRECT;

	/**
	 * @param destination the input destination.
	 * @return the destination type.
	 */
	public static DestinationType from(@Nonnull String destination) {
		final String type = destination.substring(0, destination.indexOf('/', 1) + 1).toLowerCase();
		switch (type) {
		case "queue/":
			return QUEUE;
		case "topic/":
			return TOPIC;
		default:
			return DIRECT;
		}
	}
}
