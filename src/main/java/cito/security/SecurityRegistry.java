/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.security;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import cito.security.Builder.Limitation;
import cito.event.Message;

/**
 * Registry should always allow {@code null} destinations.
 *
 * @author Daniel Siviter
 * @since v1.0 [30 Aug 2016]
 */
@ApplicationScoped
public class SecurityRegistry {
	private final Set<Limitation> limitations = new LinkedHashSet<>();

	@Inject @Any
	private Instance<SecurityCustomiser> customisers;

	@PostConstruct
	public void init() {
		final Set<SecurityCustomiser> configurers = new TreeSet<>(Comparator.comparing(SecurityRegistry::getPriority));
		this.customisers.forEach(configurers::add);
		configurers.forEach(c -> {
			customise(c);
			this.customisers.destroy(c);
		});
	}

	/**
	 * @param limitation the limitation to register.
	 */
	public synchronized void register(Limitation limitation) {
		this.limitations.add(limitation);
	}

	/**
	 * @param msg the frame to test.
	 * @return a list of limitations matching the frame.
	 */
	public synchronized List<Limitation> getMatching(Message msg) {
		return this.limitations.stream().filter(e -> e.test(msg)).collect(Collectors.toList());
	}

	/**
	 * @param msg the frame to test.
	 * @param ctx the current security context.
	 * @return {@code true} if the frame is permitted to proceed.
	 */
	public boolean isPermitted(Message msg, SecurityContext ctx) {
		for (Limitation limitation : getMatching(msg)) {
			if (!limitation.permitted(ctx)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return returns a new builder instance.
	 */
	public Builder builder() {
		return builder(this);
	}

	/**
	 * @param customiser functional way to customise.
	 */
	public void customise(SecurityCustomiser customiser) {
		customiser.customise(this);
	}


	// --- Static Methods ---

	/**
	 * @param registry the registry instance.
	 * @return a new builder instance.
	 */
	public static Builder builder(SecurityRegistry registry) {
		return new Builder(registry);
	}

	/**
	 * Returns the {@link Priority#value()} if available or 5000 if not.
	 *
	 * @param customiser the customiser instance.
	 * @return the priority value.
	 */
	private static int getPriority(SecurityCustomiser customiser) {
		final Priority priority = customiser.getClass().getAnnotation(Priority.class);
		return priority != null ? priority.value() : 5000;
	}
}
