/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.security;

import static cito.internal.Util.requireNonEmpty;
import static java.util.Collections.unmodifiableList;

import java.lang.annotation.Annotation;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;

import cito.annotation.DenyAllLiteral;
import cito.annotation.PermitAllLiteral;
import cito.annotation.RolesAllowedLiteral;
import cito.event.Message;
import cito.internal.Glob;
import cito.internal.stomp.Command;
import cito.internal.stomp.StompMessage;

/**
 * A builder to assist with creating security restrictions.
 *
 * @author Daniel Siviter
 * @since v1.0 [19 Oct 2016]
 */
public class Builder {
	private final SecurityRegistry registry;
	private final List<MessageMatcher> frameMatchers = new ArrayList<>();
	private final List<SecurityMatcher> securityMatchers = new ArrayList<>();

	Builder(SecurityRegistry registry) {
		this.registry = registry;
	}

	/**
	 * @param matcher the frame matcher.
	 * @return this builder instance.
	 */
	public Builder matches(MessageMatcher matcher) {
		this.frameMatchers.add(matcher);
		return this;
	}

	/**
	 *
	 * @param commands the commands to match against.
	 * @return this builder instance.
	 */
	public Builder matches(Command... commands) {
		return matches(new CommandMatcher(commands));
	}

	/**
	 *
	 * @param destinations the destinations to match against.
	 * @return this builder instance.
	 */
	public Builder matches(String... destinations) {
		return matches(new DestinationsMatcher(destinations));
	}

	/**
	 *
	 * @param matcher the secutiry matcher.
	 * @return this builder instance.
	 */
	public Builder matches(SecurityMatcher matcher) {
		this.securityMatchers.add(matcher);
		return this;
	}

	/**
	 * Matches against null destinations.
	 *
	 * @return this builder instance.
	 */
	public Builder nullDestination() {
		return matches(new NullDestinationMatcher());
	}

	/**
	 * @param roles the roles to match against.
	 * @return this builder instance.
	 */
	public Builder roles(String... roles) {
		return matches(new SecurityAnnotationMatcher(new RolesAllowedLiteral(roles)));
	}

	/**
	 * Permits all.
	 *
	 * @return this builder instance.
	 */
	public Builder permitAll() {
		return matches(new SecurityAnnotationMatcher(PermitAllLiteral.permitAll()));
	}

	/**
	 * Denies all.
	 *
	 * @return this builder instance.
	 */
	public Builder denyAll() {
		return matches(new SecurityAnnotationMatcher(DenyAllLiteral.denyAll()));
	}

	/**
	 * Ensures a {@link Principal} exists.
	 *
	 * @return this builder instance.
	 */
	public Builder principleExists() {
		return matches(new PrincipalMatcher());
	}

	/**
	 * Builds the limitation from the builder.
	 *
	 * @return this builder instance.
	 */
	public Limitation build() {
		final Limitation limitation = new Limitation(this.frameMatchers, this.securityMatchers);
		this.registry.register(limitation);
		return limitation;
	}


	// --- Inner Classes ---

	/**
	 * A matcher for {@link Command}s.
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [18 Oct 2016]
	 */
	public static class CommandMatcher implements MessageMatcher {
		private final Set<Command> commands;

		public CommandMatcher(Command... commands) {
			this.commands = EnumSet.copyOf(Arrays.asList(commands));
		}

		@Override
		public boolean test(Message msg) {
			final StompMessage stompMsg = (StompMessage) msg;
			return this.commands.contains(stompMsg.frame().command());
		}
	}

	/**
	 * Simple interface to define a destination {@link MessageMatcher} type.
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [18 Oct 2016]
	 */
	public static abstract class DestinationMatcher implements MessageMatcher {
		@Override
		public final boolean test(Message msg) {
			return matches(msg.destination().orElse(null));
		}

		protected abstract boolean matches(String destination);
	}

	/**
	 * A {@link DestinationMatcher} that uses the exact destination.
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [18 Oct 2016]
	 */
	public static class ExactDestinationMatcher extends DestinationMatcher {
		private final String destination;

		public ExactDestinationMatcher(String destination) {
			this.destination = destination;
		}

		@Override
		public boolean matches(String destination) {
			return Objects.equals(destination, this.destination);
		}
	}

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [27 Oct 2016]
	 */
	public static class NullDestinationMatcher extends ExactDestinationMatcher {
		public NullDestinationMatcher() {
			super(null);
		}
	}

	/**
	 * A {@link DestinationMatcher} that uses {@link Glob} patterns.
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [18 Oct 2016]
	 */
	public static class GlobDestinationMatcher extends DestinationMatcher {
		private final Glob glob;

		public GlobDestinationMatcher(String destination) {
			this.glob = Glob.from(destination);
			if (!this.glob.hasWildcard()) {
				throw new IllegalArgumentException("Destination no a Glob pattern! [" + destination + "]");
			}
		}

		@Override
		public boolean matches(String destination) {
			return destination != null && this.glob.matches(destination);
		}
	}

	/**
	 * Defines a group of destinations to test. If this is a {@link Glob} type then {@link GlobDestinationMatcher},
	 * otherwise the simpler {@link ExactDestinationMatcher} is used for performance.
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [18 Oct 2016]
	 */
	public static class DestinationsMatcher implements MessageMatcher {
		private final DestinationMatcher[] matchers;

		public DestinationsMatcher(String... destinations) {
			if (destinations.length == 0) {
				throw new IllegalArgumentException("Must have one destination!");
			}
			this.matchers = new DestinationMatcher[destinations.length];

			for (int i = 0; i < destinations.length; i++) {
				if (destinations[i].contains("*")) {
					this.matchers[i] = new GlobDestinationMatcher(destinations[i]);
				} else {
					this.matchers[i] = new ExactDestinationMatcher(destinations[i]);
				}
			}
		}

		@Override
		public boolean test(Message msg) {
			for (DestinationMatcher matcher : this.matchers) {
				if (!matcher.test(msg)) {
					return false;
				}
			}
			return true;
		}
	}

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [5 Oct 2016]
	 */
	public static class PrincipalMatcher implements SecurityMatcher {
		@Override
		public boolean permitted(SecurityContext securityCtx) {
			return securityCtx.getUserPrincipal() != null;
		}
	}

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [30 Aug 2016]
	 */
	public static class SecurityAnnotationMatcher implements SecurityMatcher {
		private final Annotation annotation;

		public SecurityAnnotationMatcher(Annotation annotation) {
			if (annotation.annotationType() != PermitAll.class &&
					annotation.annotationType() != DenyAll.class &&
					annotation.annotationType() != RolesAllowed.class)
			{
				throw new IllegalArgumentException("Not supported! [" + annotation + "]");
			}
			this.annotation = annotation;
		}

		@Override
		public boolean permitted(SecurityContext securityCtx) {
			if (this.annotation.annotationType() == DenyAll.class) {
				return false;
			}

			if (this.annotation.annotationType() == PermitAll.class) {
				return true;
			}

			if (this.annotation.annotationType() == RolesAllowed.class) {
				final String[] roles = ((RolesAllowed) this.annotation).value();

				for (String role : roles) {
					if (securityCtx.isUserInRole(role)) {
						return true;
					}
				}
			}
			return false;
		}
	}

	/**
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [25 Oct 2016]
	 */
	public static class Limitation implements MessageMatcher, SecurityMatcher {
		private static final AtomicInteger ID = new AtomicInteger();

		private final int id;
		private final List<MessageMatcher> frameMatchers;
		private final List<SecurityMatcher> securityMatchers;

		public Limitation(List<MessageMatcher> frameMatchers, List<SecurityMatcher> securityMatchers) {
			this.id = ID.incrementAndGet();
			this.frameMatchers = unmodifiableList(new ArrayList<>(requireNonEmpty(frameMatchers)));
			this.securityMatchers = unmodifiableList(new ArrayList<>(requireNonEmpty(securityMatchers)));
		}

		public int getId() {
			return id;
		}

		@Override
		public boolean test(Message msg) {
			for (MessageMatcher matcher : this.frameMatchers) {
				if (!matcher.test(msg)) {
					return false;
				}
			}
			return true;
		}

		@Override
		public boolean permitted(SecurityContext ctx) {
			for (SecurityMatcher matcher : this.securityMatchers) {
				if (!matcher.permitted(ctx)) {
					return false;
				}
			}
			return true;
		}
	}
}
