/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.enterprise.util.AnnotationLiteral;
import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;

/**
 * Inject an expanded path parameter into the method:
 * <pre>
 * 	public void on(
 * 			&#064;Observes &#064;OnSend("{param}.world}") MessageEvent e,
 * 			&#064;PathParam("param") String param)
 * 	{
 * 		// do something
 * 	}
 * </pre>
 *
 * @author Daniel Siviter
 * @since v1.0 [23 Nov 2016]
 */
@Qualifier
@Target({ PARAMETER, METHOD })
@Retention(RUNTIME)
public @interface PathParam {
	@Nonbinding
	String value();


	// --- Inner Classes ---

	/**
	 * Literal for {@link PathParam}.
	 *
	 * @author Daniel Siviter
	 * @since v1.0 [19 Sep 2018]
	 */
	@SuppressWarnings("all")
	public static final class Literal extends AnnotationLiteral<PathParam> implements PathParam {
		private static final long serialVersionUID = 1L;

		private final String value;

		private Literal(String value) {
			this.value = value != null ? value : "**";
		}

		@Override
		public String value() {
			return this.value;
		}

		public static PathParam pathParam(String value) {
			return new Literal(value);
		}
	}
}
