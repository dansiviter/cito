package cito.annotation;

import javax.annotation.security.DenyAll;
import javax.enterprise.util.AnnotationLiteral;

/**
 * Literal for {@link DenyAll}
 *
 * @author Daniel Siviter
 * @since v1.0 [19 Sep 2018]
 */
@SuppressWarnings("all")
public class DenyAllLiteral extends AnnotationLiteral<DenyAll> implements DenyAll {
	private static final DenyAll INSTANCE = new DenyAllLiteral();

	public static DenyAll denyAll() {
		return INSTANCE;
	}
}
