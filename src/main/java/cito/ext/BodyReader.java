/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.ext;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;

import javax.activation.MimeType;
import javax.annotation.Nonnull;

/**
 * Performs deserialisation of a stream to an object.
 * 
 * @author Daniel Siviter
 * @since v1.0 [24 Aug 2016]
 * @param <T> the type that can be written.
 * @see Serialiser
 */
public interface BodyReader<T> {
	/**
	 * @param type the type wanted.
	 * @param mimeType the media type.
	 * @return {@code true} if this reader can deserialise the given type.
	 */
	boolean isReadable(@Nonnull Type type, @Nonnull MimeType mimeType);

	/**
	 * 
	 * @param type the type wanted.
	 * @param mimeType the media type.
	 * @param is the input stream
	 * @return the new type.
	 * @throws IOException thrown if there was an issue deserialising.
	 */
	T readFrom(@Nonnull Type type, @Nonnull MimeType mimeType, @Nonnull InputStream is) throws IOException;
}
