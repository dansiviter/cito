/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.ext;

import static cito.internal.Messages.messages;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;

import javax.activation.MimeType;
import javax.annotation.Nonnull;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

/**
 * Performs [de]serialisation of beans.
 *
 * @author Daniel Siviter
 * @since v1.0 [24 Aug 2016]
 * @see BodyReader
 * @see BodyWriter
 */
@ApplicationScoped
public class Serialiser {
	@Inject
	private Instance<BodyReader<?>> readers;
	@Inject
	private Instance<BodyWriter<?>> writers;

	/**
	 * @param <T>      the type to deserialise.
	 * @param type     the type to deserialise to.
	 * @param mimeType the content type.
	 * @param is       the input stream to serialise.
	 * @return the new instance.
	 * @throws IOException thrown if an error occurs while deserialising.
	 */
	@SuppressWarnings("unchecked")
	public <T> T readFrom(@Nonnull Type type, @Nonnull MimeType mimeType, @Nonnull InputStream is) throws IOException {
		for (BodyReader<?> reader : this.readers) {
			if (reader.isReadable(type, mimeType)) {
				return (T) reader.readFrom(type, mimeType, is);
			}
		}
		throw messages().noReaderFound(type, mimeType);
	}

	/**
	 * @param <T>      the type to serialise.
	 * @param t        the instance to serialise.
	 * @param type     the type to serialise from.
	 * @param mimeType the content type.
	 * @param os       the output stream.
	 * @throws IOException thrown if an error occurs while serialising.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> void writeTo(@Nonnull T t, @Nonnull Type type, @Nonnull MimeType mimeType, @Nonnull OutputStream os)
			throws IOException {
		for (BodyWriter<?> writer : this.writers) {
			if (writer.isWriteable(type, mimeType)) {
				((BodyWriter) writer).writeTo(t, type, mimeType, os);
				return;
			}
		}
		throw messages().noWriterFound(type, mimeType);
	}
}
